// @flow
export type Config = {
  environment: string,
  jwtSecret: string,
  siteUrl: string,
  serverBackend: string
}

