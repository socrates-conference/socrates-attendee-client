// @flow

import {ReactWrapper} from 'enzyme';

expect.extend({
  toBeRequired(received: ReactWrapper) {
    let message = () => '', pass = true;
    if (!received || !(received.prop('required') || received.hasClass('required'))) {
      const elementName = received.prop('name') || received.prop('htmlFor') || received.prop('id');
      message = () => `Expected ${received.name()} ${elementName} to be required.`;
      pass = false;
    }
    return {message, pass};
  }
});

type CustomMatchersType = {not: CustomMatchersType, toBeRequired: (received: ReactWrapper) => void};
export type SoCraTesMatchersType = CustomMatchersType & JestExpectType & JestPromiseType & EnzymeMatchersType;

// $FlowFixMe
const expectExtended = expr => (expect(expr): SoCraTesMatchersType);
export default expectExtended;