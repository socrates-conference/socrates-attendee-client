// @flow

import * as React from 'react';
import * as PropTypes from 'prop-types';
import './App.css';
import {
  connect,
  Provider
} from 'react-redux';
import {
  Redirect,
  Route,
  Router,
  Switch
} from 'react-router-dom';
import ScrollToTop from './view/common/ScrollToTop';
import Navigation from './view/common/navigation/Navigation';
import Footer from './view/common/Footer';
import LoginContainer from './view/authentication/LoginContainer';
import WebsiteMonitoring from './WebsiteMonitoring';
import Profile from './view/profile/Profile';
import SignedInRoute from './view/SignedInRoute';
import ForgotPasswordContainer from './view/authentication/forgotPassword/ForgotPasswordContainer';
import ConfirmRegistration from './view/confirmRegistration/ConfirmRegistration';
import FourOFour from './view/common/FourOFour';
import AcceptInvitationComponent from './view/handleInvitation/handleInvitationComponent';
import RoomMarketplaceContainer from './view/roomMarketplace/RoomMarketplaceContainer';

export type WithLayoutProps = {
  children: React.Node,
}

function WithLayout(props: WithLayoutProps) {
  return (
    <div>
      <Navigation/>
      {props.children}
      <Footer/>
    </div>
  );
}

WithLayout.propTypes = {
  children: PropTypes.node.isRequired
};

export type AppProps = {
  store: any,
  history: any,
}

export class App extends React.Component<AppProps> {
  static propTypes = {
    history: PropTypes.any,
    store: PropTypes.any
  };

  static defaultProps = {};
  render() {
    return (<Provider store={this.props.store}>
      <Router history={this.props.history}>
        <WebsiteMonitoring>
          <ScrollToTop>
            <Switch>
              <Redirect exact from="/" to="/profile"/>
              <SignedInRoute exact path="/profile">
                <WithLayout>
                  <Profile/>
                </WithLayout>
              </SignedInRoute>
              <SignedInRoute exact path="/confirm-registration/:confirmationKey">
                <WithLayout>
                  <ConfirmRegistration/>
                </WithLayout>
              </SignedInRoute>
              <Route exact path="/login">
                <WithLayout>
                  <LoginContainer/>
                </WithLayout>
              </Route>
              <SignedInRoute exact path="/invitations/:action/:key">
                <WithLayout>
                  <AcceptInvitationComponent/>
                </WithLayout>
              </SignedInRoute>
              <SignedInRoute exact path="/room-marketplace">
                <WithLayout>
                  <RoomMarketplaceContainer/>
                </WithLayout>
              </SignedInRoute>
              <Route exact path="/forgot-password">
                <WithLayout>
                  <ForgotPasswordContainer/>
                </WithLayout>
              </Route>
              <Route>
                <WithLayout>
                  <FourOFour/>
                </WithLayout>
              </Route>
            </Switch>
          </ScrollToTop>
        </WebsiteMonitoring>
      </Router>
    </Provider>);
  }
}

const mapStateToProps = (state) => {
  return {...state};
};

export default connect(mapStateToProps)(App);
