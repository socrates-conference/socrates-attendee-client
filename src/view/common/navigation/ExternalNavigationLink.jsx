//@flow
import React from 'react';
type Props = {
  url: string,
  title: string,
  iconClass: string
}

export function ExternalNavigationLink({url, title, iconClass}: Props) {
  // noinspection CheckTagEmptyBody
  return (<li className="nav-item">
    <a
      className="nav-link"
      href={url}
      title={title}>
      <span className={iconClass}></span>
      <span className="d-md-none d-lg-none d-xl-inline"> {title}</span>
    </a>
  </li>);
}