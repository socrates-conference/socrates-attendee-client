// @flow

import React from 'react';
import brandLogo from '../../../assets/img/socrates_no_text_40.png';
import config from '../../../config';
import '../Header.css';

export default function NavigationBrand() {
  const homeUrl = `${config.siteUrl}/home`;
  return (
    <a className="navbar-brand" href={homeUrl} title="Home">
      <span>
        <img src={brandLogo} alt="logo"/>
      </span>
      <span> SoCraTes 2019</span>
    </a>
  );
}