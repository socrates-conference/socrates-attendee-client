// @flow
import React from 'react';
import {connect} from 'react-redux';
import NavigationBrand from './NavigationBrand';
import NavigationToggler from './NavigationToggler';
import DropdownItem from './DropdownItem';
import DropdownToggler from './DropdownToggler';
import {withRouter} from 'react-router';
import type {AuthenticationState} from '../../authentication/authenticationReducer';
import {logout} from '../../authentication/authenticationCommand';
import config from '../../../config';
import {ExternalNavigationLink} from './ExternalNavigationLink';
import ExternalNavigationApplicationLink from './ExternalNavigationApplicationLink';

type Props = {
  authentication: AuthenticationState,
  logout: () => void
}

// eslint-disable-next-line no-unused-vars
function Navigation(props: Props) {
  const isLoggedIn = props.authentication.token.trim() !== '';
  const collapsiblePanelId = 'navbarSupportedContent';
  const userDropdown = 'navbarDropdownMenuLink';
  const formatUrl = `${config.siteUrl}/format`;
  const locationUrl = `${config.siteUrl}/location`;
  const valuesUrl = `${config.siteUrl}/values`;
  const accessibilityUrl = `${config.siteUrl}/accessibility`;
  const faqUrl = `${config.siteUrl}/faq`;
  const historyUrl = `${config.siteUrl}/history`;
  const imprintUrl = `${config.siteUrl}/imprint`;
  const privacyPolicyUrl = `${config.siteUrl}/privacy-policy`;
  return (
    <div id="navigation">
      <nav className="navbar navbar-expand-md navbar-dark bg-dark">
        <NavigationBrand/>
        <NavigationToggler target={collapsiblePanelId}/>
        <div className="collapse navbar-collapse" id={collapsiblePanelId}>
          <ul className="navbar-nav mr-auto">
            <ExternalNavigationLink url={formatUrl} title="Format" iconClass="fas fa-calendar-check"/>
            <ExternalNavigationLink url={locationUrl} title="Location" iconClass="fas fa-globe"/>
            <ExternalNavigationLink url={valuesUrl} title="Values" iconClass="fas fa-gift"/>
            <ExternalNavigationLink url={accessibilityUrl} title="Accessibility" iconClass="fab fa-accessible-icon"/>
            <ExternalNavigationLink url={faqUrl} title="FAQ" iconClass="fas fa-question-circle"/>
            <ExternalNavigationLink url={historyUrl} title="History" iconClass="fas fa-history"/>
            <ExternalNavigationLink url={imprintUrl} title="Imprint" iconClass="fas fa-paragraph"/>
            <ExternalNavigationLink url={privacyPolicyUrl} title="Privacy policy" iconClass="fas fa-user-secret"/>
            <ExternalNavigationApplicationLink visible={!isLoggedIn}/>
          </ul>
          <div className="navbar-nav navbar-item mr-3 dropdown">
            <DropdownToggler
              target={userDropdown}
              url="#navigation"
              title={props.authentication.userName}
              iconClass="fas fa-user"
            />
            <div className="dropdown-menu bg-dark dropdown-menu-right" aria-labelledby={userDropdown}>
              <DropdownItem visible={!isLoggedIn} url="/login" title="Login" iconClass="fas fa-sign-in-alt"/>
              <DropdownItem visible={isLoggedIn} url="/profile" title="Profile" iconClass="fas fa-users"/>
              <DropdownItem visible={isLoggedIn} url="#navigation" title="Logout" iconClass="fas fa-sign-out-alt"
                onClick={props.logout}/>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {authentication: {...state.authentication}};
};

const mapDispatchToProps = {
  logout
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navigation));
