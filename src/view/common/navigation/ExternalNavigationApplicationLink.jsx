// @flow
import React from 'react';
import config from '../../../config';

type Props={visible: boolean};
export default function ExternalNavigationApplicationLink({visible}: Props) {
  return (
    <li className={'nav-item' + (visible ? ' d-block' : ' d-none')}>
      <a className="btn btn-primary pull-right btn-application"
        href={`${config.siteUrl}/application`}>Apply for a ticket</a>
    </li>
  );
}
