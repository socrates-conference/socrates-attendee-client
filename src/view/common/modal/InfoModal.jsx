// @flow

import React from 'react';
import type {ModalButton} from './Modal';
import Modal from './Modal';
import * as PropTypes from 'prop-types';

export type InfoModalInfo = {
  title: string,
  text: string,
}

type Props = {
  closeModal: () => void,
  info: InfoModalInfo,
  show: boolean,
}

export default function InfoModal(props: Props) {

  const _ok = () => {
    props.closeModal();
  };

  const buttons: Array<ModalButton> = [
    {isSubmit: true, onClick: _ok, text: 'Ok'}
  ];

  return (
    <Modal show={props.show} buttons={buttons}>
      <div className="modal-header alert alert-success">
        <h3>
          <strong>{props.info.title}</strong>
        </h3>
      </div>
      <div className="modal-body">
        <div><strong>{props.info.text}</strong></div>
      </div>
    </Modal>
  );
}
InfoModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
  info: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired
};

