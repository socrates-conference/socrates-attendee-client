import React from 'react';
import {mount} from 'enzyme';
import AreYouSureModal from './AreYouSureModal';
import {spy} from 'sinon';
import {ModalResult} from './Modal';

describe('(Component) AreYouSureModal', () => {
  let wrapper;
  let closeSpy;
  describe('renders', () => {
    beforeEach(() => {
      closeSpy = spy();
      wrapper = mount(
        <AreYouSureModal
          editData={{}}
          show={true}
          closeModal={closeSpy}
          info={{commandId: 'test', title: 'Test title', text: 'Test text'}}
        />);
    });

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });
    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
    it('closes on ok', () => {
      wrapper.find('#modal-button-Ok').simulate('submit');
      expect(closeSpy.withArgs({}, ModalResult.Success, 'test').calledOnce).toBe(true);
    });
    it('closes on cancel', () => {
      wrapper.find('#modal-button-Cancel').simulate('click');
      expect(closeSpy.withArgs({}, ModalResult.Cancel, 'test').calledOnce).toBe(true);
    });
  });
});
