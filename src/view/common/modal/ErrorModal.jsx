// @flow

import React from 'react';
import type {ModalButton} from './Modal';
import Modal from './Modal';
import * as PropTypes from 'prop-types';

export type ErrorModalInfo = {
  title: string,
  text: string,
  message: string
}

type Props = {
  closeModal: () => void,
  info: ErrorModalInfo,
  show: boolean,
}

export default function ErrorModal(props: Props) {

  const _ok = () => {
    props.closeModal();
  };

  const buttons: Array<ModalButton> = [
    {isSubmit: true, onClick: _ok, text: 'Ok'}
  ];

  return (
    <Modal show={props.show} buttons={buttons}>
      <div className="modal-header alert alert-danger">
        <h3>
          <strong>{props.info.title}</strong>
        </h3>
      </div>
      <div className="modal-body">
        <div><strong>{props.info.text}</strong></div>
        <div>message: {props.info.message}</div>
      </div>
    </Modal>
  );
}
ErrorModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
  info: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired
};

