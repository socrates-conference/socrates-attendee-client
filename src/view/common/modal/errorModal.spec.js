import React from 'react';
import {mount} from 'enzyme';
import {spy} from 'sinon';
import ErrorModal from './ErrorModal';

describe('(Component) ErrorModal', () => {
  let wrapper;
  let closeSpy;
  describe('renders', () => {
    beforeEach(() => {
      closeSpy = spy();
      wrapper = mount(
        <ErrorModal
          show={true}
          closeModal={closeSpy}
          info={{commandId: 'test', title: 'Test title', text: 'Test text'}}
        />);
    });

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });
    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
    it('closes on ok', () => {
      wrapper.find('#modal-button-Ok').simulate('submit');
      expect(closeSpy.calledOnce).toBe(true);
    });
  });
});
