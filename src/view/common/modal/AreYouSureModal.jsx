// @flow

import React from 'react';
import type {ModalButton} from './Modal';
import Modal, {ModalResult} from './Modal';
import * as PropTypes from 'prop-types';

export type AreYouSureModalInfo = {
  commandId: string,
  title: string,
  text: string
}

type Props = {
  closeModal: (Object, string, string) => void,
  editData: Object,
  info: AreYouSureModalInfo,
  show: boolean,
}

export default function AreYouSureModal(props: Props) {

  const _ok = () => {
    props.closeModal(props.editData, ModalResult.Success, props.info.commandId);
  };

  const _cancel = () => {
    props.closeModal(props.editData, ModalResult.Cancel, props.info.commandId);
  };

  const buttons: Array<ModalButton> = [
    {isSubmit: true, onClick: _ok, text: 'Ok'},
    {isSubmit: false, onClick: _cancel, text: 'Cancel'}
  ];

  return (
    <Modal show={props.show} buttons={buttons}>
      <div className="modal-header">
        <h3 className="mb-2 mx-auto">
          <strong>{props.info.title}</strong>
        </h3>
      </div>
      <div className="mb-2 mx-auto alert alert-info">
        <small>{props.info.text}</small>
      </div>
      <div className="form-row">
        <h3>Are you sure?</h3>
      </div>
    </Modal>
  );
}
AreYouSureModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
  editData: PropTypes.object.isRequired,
  info: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired
};

