// @flow

import React from 'react';
import './footer.css';
import config from '../../config';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

export default function Footer() {
  const twitterUrl = 'https://twitter.com/intent/tweet?text=SoCraTes+2019+will+take+place+from+22+to+25+August.' +
    '+More+at&amp;url=http%3A%2F%2Fsocrates-conference.de%2F&amp;hashtags=SoCraTes2019&amp;via=SoCraTes_Conf';

  return (
    <div id="footer" className="container">
      <footer>
        <div className="row">
          <hr/>
          <div className="col-sm-12">
            <div className="btn-group float-right">
              <a className="btn btn-info" href={`${config.siteUrl}/imprint`}>
                <FontAwesomeIcon icon="info" />
                <span className="d-none d-xl-inline"> Legal Notice</span>
              </a>
              <a className="btn btn-info" href="mailto:info@socrates-conference.de" title="E-Mail">
                <FontAwesomeIcon icon="envelope" />
                <span className="d-none d-xl-inline"> E-mail</span>
              </a>
              <a className="btn btn-info" href={twitterUrl} title="Tweet">
                <FontAwesomeIcon icon={['fab', 'twitter']} />
                <span className="d-none d-xl-inline"> Tweet</span>
              </a>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}
