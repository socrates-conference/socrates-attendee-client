import React from 'react';
import {shallow} from 'enzyme';
import TableBody from './TableBody';

describe('(Component) TableBody', () => {
  const columns = [{header: 'Name', field: 'name', isSortable: true, isFilterable: true}];
  const data = [{name: 'Name1'}, {name: 'Name2'}];
  const actions = [{
    callback: () => {
    }, icon: {}, buttonClass: '', text: ''
  }];
  const wrapper = shallow(
    <TableBody actions={actions} data={data} columns={columns}/>
  );

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
