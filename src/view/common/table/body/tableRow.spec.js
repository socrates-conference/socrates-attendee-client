import React from 'react';
import {shallow} from 'enzyme';
import TableRow from './TableRow';

describe('(Component) TableRow', () => {
  const columns = [{header: 'Name', field: 'name', isSortable: true, isFilterable: true}];
  const content = {name: 'Name1'};
  const actions = [{
    callback: () => {
    }, icon: {}, buttonClass: '', text: ''
  }];
  const wrapper = shallow(
    <TableRow actions={actions} content={content} columns={columns}/>
  );

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
