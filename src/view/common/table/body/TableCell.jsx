// @flow

import React from 'react';
import type {Node} from 'react';

export type TableCellProps = {
  content: string,
  displayClass?: string,
  format?: (any) => string,
  cellTemplate?: (any) => Node
};


export default function TableCell(props: TableCellProps) {
  const renderContent = () => {
    if(props.cellTemplate) {
      return props.cellTemplate(props.content);
    } else if (props.format) {
      return props.format(props.content);
    } else {
      return props.content;
    }
  };
  return (
    <td className={props.displayClass}>
      {renderContent()}
    </td>
  );
}
