// @flow

import React from 'react';
import TableCell from './TableCell';
import RowActionCell from '../actions/RowActionCell';
import type {ColumnDefinition, Action} from '../TableTypes';

export type TableRowProps = {
  columns: Array<ColumnDefinition>,
  content: {},
  rowActions: Array<Action>
};

export default function TableRow(props: TableRowProps) {
  const hasActions = props.rowActions && props.rowActions.length > 0;
  return (
    <tr>
      {
        props.columns.map((column, index) =>
          column.visible ? <TableCell
            key={'cell-' + column.header + '-' + index}
            content={props.content[column.field]}
            format={column.format}
            displayClass={column.displayClass}
            cellTemplate = {column.cellTemplate}
          /> : null)
      }
      {hasActions ? <RowActionCell rowActions={props.rowActions} data={props.content}/> : null}
    </tr>
  );
}

