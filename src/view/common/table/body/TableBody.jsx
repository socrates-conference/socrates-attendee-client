// @flow

import React from 'react';
import TableRow from './TableRow';
import type {ColumnDefinition, Action} from '../TableTypes';

export type TableBodyProps = {
  columns: Array<ColumnDefinition>,
  data: Array<Object>,
  rowActions: Array<Action>
};

export default function TableBody(props: TableBodyProps) {
  return (
    <tbody>
      {
        props.data.map((row, index) =>
          <TableRow key={'row' + index} content={row} columns={props.columns} rowActions={props.rowActions}/>)
      }
    </tbody>
  );
}

