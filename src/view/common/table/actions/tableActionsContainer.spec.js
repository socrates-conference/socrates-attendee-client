import React from 'react';
import {shallow} from 'enzyme';
import TableActionsContainer from './TableActionsContainer';

describe('(Component) TableActionsContainer', () => {
  const wrapper = shallow(
    <TableActionsContainer actions={[{
      execute: () => {}, canExecute: () => true, icon: {}, buttonClass: '', text: ''
    }]} allListItems={[]}/>
  );

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
