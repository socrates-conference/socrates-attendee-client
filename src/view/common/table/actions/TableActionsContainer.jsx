// @flow

import React from 'react';
import type {Action} from '../TableTypes';
import PropTypes from 'prop-types';
import {executeActionIfAllowed} from './tableActionContainerHelper';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

export type TableActionsContainerProps = {
  actions: Array<Action>,
  allListItems: Array<Object>
}

export default function TableActionsContainer(props: TableActionsContainerProps) {
  const visibleActions =
    (props.actions && props.actions.length > 0) ? props.actions.filter(item => item.visible) : [];

  return (
    <div className="row">
      {
        visibleActions.map((action, index) => {
          const buttonClass = 'btn btn-sm ' + action.buttonClass;
          return (
            <div key={`globalButton-${index}`} className="col-sm-4 col-md-3 col-lg-2 mb-2">
              <button
                className={buttonClass}
                onClick={() => executeActionIfAllowed(action, props.allListItems)}
                disabled={!action.canExecute(props.allListItems)}>
                <span>{action.text}&nbsp;</span>
                <FontAwesomeIcon icon={action.icon} />
              </button>
            </div>
          );
        })
      }
    </div>
  );
}

TableActionsContainer.propTypes = {
  actions: PropTypes.array.isRequired,
  allListItems: PropTypes.array.isRequired
};