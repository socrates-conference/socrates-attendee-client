import React from 'react';
import {shallow} from 'enzyme';
import RowActionCell from './RowActionCell';

describe('(Component) RowActionCell', () => {
  const wrapper = shallow(
    <RowActionCell rowActions={[{
      execute: () => {
      }, icon: {}, buttonClass: '', text: ''
    }]} data={{}}/>
  );

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
