import React from 'react';
import {shallow} from 'enzyme';
import RowActionButton from './RowActionButton';

describe('(Component) RowActionButton', () => {
  describe('renders', () => {
    const wrapper = shallow(
      <RowActionButton action={{
        execute: () => {
        }, canExecute: () => true, icon: {}, buttonClass: '', text: ''
      }} data={{id: 1}}/>
    );

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });

  it('executes the command if button is clicked and can be executed', () => {
    const mockCallback = jest.fn();
    const data = {id: 1};
    const wrapper = shallow(
      <RowActionButton
        action={{execute: mockCallback, canExecute: () => true, icon: {}, buttonClass: '', text: ''}}
        data={data}/>
    );
    wrapper.find('button').simulate('click');
    expect(mockCallback.mock.calls.length).toBe(1);
    expect(mockCallback.mock.calls[0][0]).toEqual(data);
  });
});
