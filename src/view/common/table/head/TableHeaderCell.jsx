// @flow

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import type {SortOptions} from '../TableTypes';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

type TableHeaderCellState = {
  currentSortDirection: string
}

export type TableHeaderCellProps = {
  display: string,
  displayClass: string,
  sortOptions: SortOptions,
  onClick: (string) => void
}

export default class TableHeaderCell extends Component<TableHeaderCellProps, TableHeaderCellState> {
  static propTypes = {
    display: PropTypes.string.isRequired,
    displayClass: PropTypes.string,
    onClick: PropTypes.func,
    sortOptions: PropTypes.object
  };

  static defaultProps = {
    displayClass: '',
    sortOptions: {isSortable: false, isSortColumn: false, onSort: () => {}},
    onClick: () => {}
  };

  state: TableHeaderCellState = {
    currentSortDirection: 'none'
  };

  _onClick = () => {
    if (this.props.sortOptions.isSortable) {
      let nextDirection: string = 'none';
      if (this.state.currentSortDirection === 'none') {
        nextDirection = 'asc';
      } else if (this.state.currentSortDirection === 'asc') {
        nextDirection = 'desc';
      } else if (this.state.currentSortDirection === 'desc') {
        nextDirection = 'asc';
      }
      this.setState({currentSortDirection: nextDirection});
      this.props.onClick(this.props.display);
      this.props.sortOptions.onSort(this.props.display, nextDirection);
    }
  };

  _shouldShowIconFor = (direction: string): boolean => {
    return this.props.sortOptions.isSortColumn && this.state.currentSortDirection === direction;
  };
  render = () => {
    const showAscendantIcon = this._shouldShowIconFor('asc');
    const showDescendantIcon = this._shouldShowIconFor('desc');
    return (
      <th
        className={this.props.displayClass}
        onClick={this._onClick}>
        <div>
          {this.props.display}&nbsp;
          <sup>
            <span className={showAscendantIcon ? '' : 'd-none'}>
              <FontAwesomeIcon icon="sort-up" />
            </span>
            <span className={showDescendantIcon ? '' : 'd-none'}>
              <FontAwesomeIcon icon="sort-down" />
            </span>
          </sup>
        </div>
      </th>
    );
  };
}

