// @flow

import React from 'react';
import * as PropTypes from 'prop-types';
import TableHeaderRow from './TableHeaderRow';
import TableFilterRow from './TableFilterRow';
import type {ColumnDefinition, TableFilterItem} from '../TableTypes';

export type TableHeadProps = {
  columns: Array<ColumnDefinition>,
  hasActions: boolean,
  onSort: (string, string) => void,
  onFilter: (Array<TableFilterItem>) => void
}

export default function TableHead(props: TableHeadProps) {
  TableHead.propTypes = {
    columns: PropTypes.array.isRequired,
    hasActions: PropTypes.bool.isRequired,
    onFilter: PropTypes.func.isRequired,
    onSort: PropTypes.func.isRequired
  };

  const hasFilterableColumns = props.columns.some(column => column.isFilterable);
  return (
    <thead className="thead-dark">
      <TableHeaderRow columns={props.columns} onSort={props.onSort} hasActions={props.hasActions}/>
      {hasFilterableColumns
        ? <TableFilterRow columns={props.columns} onFilter={props.onFilter} hasActions={props.hasActions}/>
        : null }
    </thead>
  );
}

