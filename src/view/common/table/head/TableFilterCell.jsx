// @flow

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';

export type TableFilterCellProps = {
  columnName: string,
  displayClass: string,
  onChange: (string, string) => void
}

type TableFilterCellState = {
  value: string
}
export default class TableFilterCell extends Component<TableFilterCellProps, TableFilterCellState> {
  static propTypes = {
    columnName: PropTypes.string.isRequired,
    displayClass: PropTypes.string,
    onChange: PropTypes.func
  };

  static defaultProps = {
    displayClass: '',
    onChange: () => {
    }
  };

  state: TableFilterCellState = {
    value: ''
  };

  _onChange = (e: SyntheticEvent<HTMLInputElement>) => {
    this.setState({value: e.currentTarget.value});
    this.props.onChange(this.props.columnName, e.currentTarget.value);
  };

  render = () => {
    return (
      <th className={this.props.displayClass}>
        <div>
          <input
            type="text" className="form-control form-control-sm"
            onChange={this._onChange} value={this.state.value}
          />
        </div>
      </th>
    );
  };
}

