// @flow

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import type {ColumnDefinition, TableFilterItem} from '../TableTypes';
import TableFilterCell from './TableFilterCell';

type TableFilterRowState = {
  filter: Array<TableFilterItem>,
  timeoutId: number,
}

type TableFilterRowProps = {
  columns: Array<ColumnDefinition>,
  hasActions: boolean,
  onFilter: (Array<TableFilterItem>) => void

}

export default class TableFilterRow extends Component<TableFilterRowProps, TableFilterRowState> {
  static propTypes = {
    columns: PropTypes.array.isRequired,
    hasActions: PropTypes.bool.isRequired,
    onFilter: PropTypes.func
  };

  state: TableFilterRowState = {
    filter: [],
    intervalId: 0,
    timeoutId: 0,
    filterChanged: false
  };

  componentWillUnmount = () => {
    if (this.state.timeoutId > 0) {
      clearInterval(this.state.timeoutId);
    }
  };

  _filter = () => {
    this.props.onFilter(this.state.filter);
  };
  _onChange = (columnName: string, value: string) => {
    let updateState = false;
    const newFilter = this.state.filter.slice(0);
    const index = this.state.filter.findIndex((i) => i.columnName === columnName);
    if (index >= 0) {
      newFilter.splice(index, 1);
      updateState = true;
    }
    if (value.trim().length > 0) {
      newFilter.push({columnName, value: value.trim()});
      updateState = true;
    }
    if (updateState) {
      this._resetTimer(newFilter);
    }
  };

  _resetTimer = (filter: Array<TableFilterItem>) => {
    if (this.state.timeoutId > 0) {
      clearTimeout(this.state.timeoutId);
    }

    const timeoutId = setTimeout(this._filter, 500);
    this.setState({filter, timeoutId});
  };

  render = () => {
    return (
      <tr>
        {this.props.columns.map((column, index) =>
          column.visible ?
            column.isFilterable ?
              <TableFilterCell
                key={'filter' + index}
                columnName={column.header}
                displayClass={column.displayClass}
                onChange={this._onChange}
              /> : <th key={'filter' + index}/> : null
        )}
        {this.props.hasActions ? <th key="filterAction"/> : null}
      </tr>
    );
  };
}

