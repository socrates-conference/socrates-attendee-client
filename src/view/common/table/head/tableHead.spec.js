import React from 'react';
import {shallow} from 'enzyme';
import TableHead from './TableHead';

describe('(Component) RowActionCell', () => {
  const columns = [
    {header: 'Name', field: 'name', isSortable: true, isFilterable: true},
    {header: 'Adr', field: 'address', isSortable: true, isFilterable: true}
  ];
  const wrapper = shallow(
    <TableHead
      columns={columns}
      hasActions={true}
      onSort={() => {}}
      onFilter={() => {}}
    />
  );

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
