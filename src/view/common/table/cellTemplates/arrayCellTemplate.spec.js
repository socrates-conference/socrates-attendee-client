import React from 'react';
import {shallow} from 'enzyme';
import ArrayCellTemplate from './ArrayCellTemplate';

describe('(Component) ArrayCellTemplate', () => {
  const wrapper = shallow(
    <ArrayCellTemplate values={[1, 2, 3]}/>
  );

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
