// @flow

import React from 'react';

export default function EmailCellTemplate (props: {value: string}) {
  return (
    <div>
      <a href={'mailto://' + props.value}>
        {props.value}
      </a>
    </div>
  );
}
