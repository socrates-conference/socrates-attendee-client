// @flow

import React from 'react';
import './arrayCellTemplate.css';

export default function ArrayCellTemplate (props: {values: Array<Object>}) {
  return (
    <div>
      {
        props.values.map((item, index) =>
          <div key={'arrayItem-' + index.toString()} className="room-type-bubble d-md-inline-flex">
            {item}
          </div>
        )
      }
    </div>
  );
}