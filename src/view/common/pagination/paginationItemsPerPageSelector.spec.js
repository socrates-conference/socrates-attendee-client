import React from 'react';
import {shallow} from 'enzyme';
import PaginationItemsPerPageSelector from './PaginationItemsPerPageSelector';

describe('(Component) PaginationItemsPerPageSelector', () => {
  let mockLengthChanged;
  let wrapper;
  beforeEach(() => {
    mockLengthChanged = jest.fn();
    wrapper = shallow(
      <PaginationItemsPerPageSelector
        currentItemsPerPage={10}
        itemsPerPageChanged={mockLengthChanged}
        availableItemsPerPageSelections={[10, 25, 50]}/>
    );
  });

  afterEach(() => {
    mockLengthChanged.mockReset();
  });


  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('click changes page length', () => {
    wrapper.find('#paginationLengthButton').simulate('click');
    wrapper.find('#length-25').simulate('click');
    expect(mockLengthChanged.mock.calls.length).toBe(1);
    expect(mockLengthChanged.mock.calls[0][0]).toBe(25);
  });
});
