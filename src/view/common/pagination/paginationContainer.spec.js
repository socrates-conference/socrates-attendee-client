import React from 'react';
import {shallow} from 'enzyme';
import PaginationContainer from './PaginationContainer';

describe('(Component) PaginationContainer', () => {
  const wrapper = shallow(
    <PaginationContainer
      currentPage={1}
      currentItemsPerPage={10}
      lastPage={15}
      moveToPage={() => {}}
      itemsPerPageChanged={() => {}}
      availableItemsPerPageSelections={[10, 25, 50]}

    />
  );

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
