// @flow

import SagaTester from 'redux-saga-tester';
import rootSaga from './rootSaga';
import {login, logout} from '../authentication/authenticationCommand';
import * as sinon from 'sinon';
import * as api from '../requests/api';
import rootReducer from '../reducers/rootReducer';
import RoutingCommand, {routeTo} from '../commands/routingCommand';
import * as axios from 'axios/index';

describe('rootSaga', () => {
  let sagaTester: SagaTester;
  describe('authentication', () => {
    const INITIAL_STATE = {
      placeHolder: {},
      authentication: {
        token: '',
        userName: 'Guest',
        isAdministrator: false,
        hasFinished: false
      }
    };
    beforeEach(() => {
      sagaTester = new SagaTester({INITIAL_STATE, reducers: rootReducer});
      sagaTester.start(rootSaga);
    });

    afterEach(() => {
      sagaTester.reset();
    });

    it('reacts to logout dispatching LOGOUT_SUCCESS ', async () => {
      axios.defaults.headers.common['Authorization'] = 'Bearer token';
      const newAuthState = {...sagaTester.getState().authentication, token: 'token', userName: 'TheName'};
      sagaTester.updateState({...INITIAL_STATE, authentication: newAuthState});

      sagaTester.dispatch(logout());
      await sagaTester.waitFor('AuthenticationEvent/LOGOUT_SUCCESS');

      expect(sagaTester.getState().authentication.token).toBe('');
      expect(sagaTester.getState().authentication.userName).toBe('Guest');
      expect(axios.defaults.headers.common['Authorization']).toBeUndefined();
    });

    it('reacts to routeTo putting the new destination to the router', () => {
      sagaTester.dispatch(routeTo({pathname: '/profile'}));
      const pushAction = sagaTester.getLatestCalledAction();
      expect(pushAction.payload.method).toBe('push');
      expect(pushAction.payload.args[0]).toEqual({pathname: '/profile'});
    });

    describe('reacts to login command', () => {
      const loginStub = sinon.stub(api, 'login');
      const getRouteToCommands = (actions) => actions.filter(action => action.type === RoutingCommand.ROUTE_TO);
      afterEach(() => {
        loginStub.reset();
      });

      it('emitting login started, login success and route to when login succeed', () => {
        const theToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFsZXhAZW1haWwuZGUiLCJuY' +
          'W1lIjoiQWxleCIsImlzQWRtaW5pc3RyYXRvciI6MCwiaWF0IjoxNTIxNTY3MjUyfQ.QJRC7fzx3s4Khhd_jXRtrbaB-' +
          'p3wx9lEjP-muNU_2-0';
        delete axios.defaults.headers.common['Authorization'];
        loginStub.returns(theToken);
        sagaTester.dispatch(login('somebody@email.de', 'thePassword', {pathname: '/home'}));
        const actions = sagaTester.getCalledActions();
        expect(loginStub.getCalls().length).toBe(1);
        expect(getRouteToCommands(actions)).toHaveLength(1);
        expect(sagaTester.getState().authentication.token).toBe(theToken);
        expect(sagaTester.getState().authentication.userName).toBe('Alex');
        expect(sagaTester.getState().authentication.hasFinished).toBe(true);
        expect(axios.defaults.headers.common['Authorization']).toBeDefined();
        expect(axios.defaults.headers.common['Authorization'].indexOf('Bearer')).toBe(0);
      });
      it('emitting login started and login error when login fails', () => {
        delete axios.defaults.headers.common['Authorization'];
        loginStub.returns('');
        sagaTester.dispatch(login('somebody@email.de', 'thePassword', {pathname: '/'}));
        expect(loginStub.getCalls().length).toBe(1);
        expect(sagaTester.getState().authentication.token).toBe('');
        expect(sagaTester.getState().authentication.userName).toBe('Guest');
        expect(sagaTester.getState().authentication.hasFinished).toBe(true);
        expect(axios.defaults.headers.common['Authorization']).toBeUndefined();
      });
      it('emitting login started and login error when token cannot be verified', () => {
        delete axios.defaults.headers.common['Authorization'];
        loginStub.returns('non verifiable');
        sagaTester.dispatch(login('somebody@email.de', 'thePassword', {pathname: '/'}));
        expect(loginStub.getCalls().length).toBe(1);
        expect(sagaTester.getState().authentication.token).toBe('');
        expect(sagaTester.getState().authentication.userName).toBe('Guest');
        expect(sagaTester.getState().authentication.hasFinished).toBe(true);
        expect(axios.defaults.headers.common['Authorization']).toBeUndefined();
      });
    });

  });
});