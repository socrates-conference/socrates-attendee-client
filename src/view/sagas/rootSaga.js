// @flow
import {
  all,
  put,
  takeEvery
} from 'redux-saga/effects';
import RoutingCommand from '../commands/routingCommand';
import {push} from 'react-router-redux';
import AuthenticationCommand from '../authentication/authenticationCommand';
import {
  loginSaga, forgotPasswordSaga,
  logoutSaga, updatePasswordSaga
} from '../authentication/authenticationSaga';
import ProfileCommand from '../profile/profileCommand';
import {
  confirmParticipantSaga,
  findParticipantSaga,
  updateParticipantSaga,
  changeProfileSaga
} from '../profile/profileSaga';
import ConfirmRegistrationCommand from '../confirmRegistration/confirmRegistrationCommand';
import {updateConfirmationKeySaga} from '../confirmRegistration/confirmRegistrationSaga';
import CancelParticipationCommand from '../cancelParticipation/cancelParticipationCommand';
import {
  cancelParticipationSaga, abortCancelParticipationSaga,
  requestCancelParticipationSaga, finishCancelParticipationSage
} from '../cancelParticipation/cancelParticipationSaga';
import {
  handleInvitationSaga,
  requestAutomaticRoommateSaga,
  requestKnownRoommateSaga, updateRoomSharingSaga
} from '../roomSharing/roomSharingSaga';
import RoomSharingCommand from '../roomSharing/roomSharingCommand';
import RoomMarketplaceCommand from '../roomMarketplace/roomMarketplaceCommand';
import {
  getPotentialRoommatesSaga,
  handleMarketplaceActionSaga,
  handleMarketplaceCancelActionSaga, sendInvitationSaga
} from '../roomMarketplace/roomMarketplaceSaga';

function* routingToSaga(action: any) {
  yield put(push(action.location));
}

function* rootSaga(): any {
  yield all([
    takeEvery(RoutingCommand.ROUTE_TO, routingToSaga),
    takeEvery(AuthenticationCommand.LOGIN, loginSaga),
    takeEvery(AuthenticationCommand.LOGOUT, logoutSaga),
    takeEvery(AuthenticationCommand.UPDATE_PASSWORD, updatePasswordSaga),
    takeEvery(AuthenticationCommand.GENERATE_PASSWORD, forgotPasswordSaga),
    takeEvery(AuthenticationCommand.LOGOUT, logoutSaga),
    takeEvery(ProfileCommand.FIND_PARTICIPANT, findParticipantSaga),
    takeEvery(ProfileCommand.CONFIRM_PARTICIPANT, confirmParticipantSaga),
    takeEvery(ProfileCommand.UPDATE_PARTICIPANT, updateParticipantSaga),
    takeEvery(ConfirmRegistrationCommand.UPDATE_KEY, updateConfirmationKeySaga),
    takeEvery(CancelParticipationCommand.CANCEL_PARTICIPATION, cancelParticipationSaga),
    takeEvery(CancelParticipationCommand.REQUEST_CANCEL_PARTICIPATION, requestCancelParticipationSaga),
    takeEvery(CancelParticipationCommand.ABORT_CANCEL_PARTICIPATION, abortCancelParticipationSaga),
    takeEvery(CancelParticipationCommand.FINISH_CANCEL_PARTICIPATION, finishCancelParticipationSage),
    takeEvery(ProfileCommand.CHANGE_PROFILE, changeProfileSaga),
    takeEvery(RoomSharingCommand.REQUEST_KNOWN_ROOMMATE, requestKnownRoommateSaga),
    takeEvery(RoomSharingCommand.REQUEST_AUTOMATIC_ROOMMATE, requestAutomaticRoommateSaga),
    takeEvery(RoomSharingCommand.HANDLE_INVITATION, handleInvitationSaga),
    takeEvery(RoomSharingCommand.UPDATE_ROOM_SHARING, updateRoomSharingSaga),
    takeEvery(RoomMarketplaceCommand.GET_POTENTIAL_ROOMMATES, getPotentialRoommatesSaga),
    takeEvery(RoomMarketplaceCommand.HANDLE_ACTION, handleMarketplaceActionSaga),
    takeEvery(RoomMarketplaceCommand.CLOSE_MODAL, handleMarketplaceCancelActionSaga),
    takeEvery(RoomMarketplaceCommand.SEND_INVITATION, sendInvitationSaga)
  ]);
}

export default rootSaga;
