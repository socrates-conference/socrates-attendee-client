// @flow

import authenticationReducer from '../authentication/authenticationReducer';
import profileReducer from '../profile/profileReducer';
import confirmRegistrationReducer from '../confirmRegistration/confirmRegistrationReducer';
import cancelParticipationReducer from '../cancelParticipation/cancelParticipationReducer';
import roomSharingReducer from '../roomSharing/roomSharingReducer';
import {roomMarketplaceReducer} from '../roomMarketplace/roomMarketplaceReducer';

const reducers = {
  authentication: authenticationReducer,
  profile: profileReducer,
  confirmRegistration: confirmRegistrationReducer,
  cancelParticipation: cancelParticipationReducer,
  roomSharing: roomSharingReducer,
  roomMarketplace: roomMarketplaceReducer
};
export default reducers;
