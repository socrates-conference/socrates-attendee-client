// @flow

import axios from 'axios';
import {
  login
} from './api';

jest.mock('axios');

describe('api', () => {
  describe('login', () => {
    let promise;
    beforeEach(() => {
      axios.post.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.post.mockReset();
    });
    it('returns the token string, as received from the server on calling login', (done) => {
      promise = Promise.resolve({data: {token: 'TheToken'}});
      login('someToken').then((result) => {
        expect(result).toEqual('TheToken');
        done();
      });
    });
    it('login returns empty string if server request fails', (done) => {
      promise = Promise.reject(new Error('error'));
      login('someToken').catch((result) => {
        expect(result.message).toBe('error');
        done();
      });
    });
  });
});
