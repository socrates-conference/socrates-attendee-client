// @flow

import client from './client';
import config from '../../config';
import {getToken} from '../services/jwtStorage';
import {setAuthorizationHeader} from './clientConfig';
import type {
  Participant,
  PriceCalculation
} from '../profile/profileReducer';
import type {InvitableRequest, Invitation, InvitationRequest} from '../roomSharing/roomSharingReducer';
import type {PotentialRoommate} from '../roomMarketplace/roomMarketplaceReducer';

const token = getToken();
if (token) {
  setAuthorizationHeader(token);
}

export const login = (requestToken: string): Promise<string> => {
  return client.post(`${config.siteUrl}${config.serverBackend}/login`, {requestToken})
    .then(response => response.data.token);
};

export const updatePassword = (requestToken: string): Promise<string> => {
  return client.post(`${config.siteUrl}${config.serverBackend}/update-password`, {requestToken})
    .then(response => response.data.token);
};
export const revokePassword = (requestToken: string): Promise<boolean> => {
  return client.post(`${config.siteUrl}${config.serverBackend}/revoke-password`, {requestToken})
    .then(response => response.status === 200);
};

export const generatePassword = (requestToken: string): Promise<boolean> => {
  return client.post(`${config.siteUrl}${config.serverBackend}/generate-password`, {requestToken})
    .then(response => response.status === 200);
};

export const calculatePrice = (personId: number, arrival: string, departure: string,
  email: string): Promise<?PriceCalculation> => {
  return client.get(`${config.siteUrl}${config.serverBackend}/participants/calculate-price/${personId}`,
    {params: {arrival, departure, email}}).then(response => response.data);
};

export const findParticipant = (email: string): Promise<?Participant> => {
  return client.get(`${config.siteUrl}${config.serverBackend}/participants`, {params: {email}})
    .then(response => participantFromResponse(response));
};

export const confirmParticipant = (participant: Participant): Promise<Participant> => {
  return client.put(`${config.siteUrl}${config.serverBackend}/participants`, participant)
    .then(response => participantFromResponse(response));
};

export const updateParticipant = (participant: Participant): Promise<Participant> => {
  return client.post(`${config.siteUrl}${config.serverBackend}/participants`, participant)
    .then(response => participantFromResponse(response));
};

export const cancelParticipation = (participantId: number): Promise<boolean> => {
  return client.delete(`${config.siteUrl}${config.serverBackend}/participants/${participantId}`)
    .then(response => response.status === 200);
};

export const readEmailForConfirmationKey = (confirmationKey: string): Promise<?string> => {
  return client.get(`${config.siteUrl}${config.serverBackend}/registrations/by-key/${confirmationKey}`)
    .then(response => response.data.email);
};
export const deleteConfirmationKey = (confirmationKey: string): Promise<boolean> => {
  return client.delete(`${config.siteUrl}${config.serverBackend}/registrations/by-key/${confirmationKey}`)
    .then(response => response.status === 200);
};

const participantFromResponse = response => {
  return response.data ? ({...response.data, participantId: response.data.id}) : null;
};

const invitationFromResponse = response => {
  return response.data ? ({...response.data}) : null;
};

export const addInvitation = (invitation: InvitationRequest): Promise<Invitation> => {
  return client.post(`${config.siteUrl}${config.serverBackend}/invitations`, invitation)
    .then(response => invitationFromResponse(response));
};
export const addInvitable = (invitable: InvitableRequest): Promise<boolean> => {
  return client.post(`${config.siteUrl}${config.serverBackend}/invitables`, invitable)
    .then(response => response.data);
};

export const readInvitationByKey = (key: string): Promise<Invitation> => {
  return client.get(`${config.siteUrl}${config.serverBackend}/invitations/byKey/${key}`)
    .then(response => invitationFromResponse(response));
};
export const findSentInvitation = (personId: string): Promise<?Invitation> => {
  return client.get(`${config.siteUrl}${config.serverBackend}/invitations/sentBy/${personId}`)
    .then(response => invitationFromResponse(response));
};
export const findReceivedInvitations = (personId: string): Promise<Invitation[]> => {
  return client.get(`${config.siteUrl}${config.serverBackend}/invitations/receivedBy/${personId}`)
    .then(response => response.data);
};
export const handleInvitation = (action: string, key: string, inviteeEmail: string): Promise<Invitation> => {
  return client.put(`${config.siteUrl}${config.serverBackend}/invitations/${action}`,
    {action, key, inviteeEmail})
    .then(response => invitationFromResponse(response));
};
export const findPotentialRoommates = (email: string, departure: string): Promise<PotentialRoommate[]> => {
  return client.post(`${config.siteUrl}${config.serverBackend}/invitables/byEmail`, {email, departure})
    .then(response => response.data);
};
export const isInMarketplace = (email: string): Promise<boolean> => {
  return client.post(`${config.siteUrl}${config.serverBackend}/invitables/isInMarketplace`, {email})
    .then(response => response.data);
};
