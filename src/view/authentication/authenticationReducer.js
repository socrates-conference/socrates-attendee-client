// @flow

import AuthenticationEvent from './authenticationEvents';
import * as jwt from 'jsonwebtoken';
import config from '../../config';
import {getToken} from '../services/jwtStorage';

export type AuthenticationState = {
  token: string,
  isAdministrator: boolean,
  isOneTimePassword: boolean,
  userName: string,
  email: string,
  hasFinished: boolean,
  oldPassword: string
}

const INITIAL_STATE: AuthenticationState = {
  token: '',
  userName: 'Guest',
  email: '',
  isAdministrator: false,
  isOneTimePassword: false,
  hasFinished: false,
  oldPassword: ''
};

const initialState = (initial: AuthenticationState): AuthenticationState => {
  const token = getToken();
  if (!token) {
    return initial;
  }

  const {
    name: userName,
    isAdministrator,
    email,
    isOneTimePassword
  } = jwt.verify(token, config.jwtSecret);

  return {
    ...initial,
    token,
    userName,
    isAdministrator,
    isOneTimePassword,
    email
  };
};

const authenticationReducer = (state: AuthenticationState = initialState(INITIAL_STATE), action: any) => {
  switch (action.type) {
    case AuthenticationEvent.LOGIN_SUCCESS:
    case AuthenticationEvent.UPDATE_PASSWORD_SUCCESS:
      return {
        ...state,
        hasFinished: true,
        token: action.token,
        isAdministrator: action.data.isAdministrator,
        isOneTimePassword: action.data.isOneTimePassword,
        userName: action.data.name,
        email: action.data.email,
        oldPassword: ''
      };
    case AuthenticationEvent.UPDATE_PASSWORD_NEEDED:
      return {
        ...state,
        hasFinished: false,
        token: '',
        isAdministrator: false,
        isOneTimePassword: true,
        userName: '',
        email: action.data.email,
        oldPassword: action.oldPassword
      };
    case AuthenticationEvent.LOGIN_STARTED:
    case AuthenticationEvent.LOGOUT_SUCCESS:
      return {
        ...state, isAdministrator: false, hasFinished: false, token: '', userName: 'Guest', email: '', oldPassword: ''
      };
    case AuthenticationEvent.LOGIN_ERROR:
      return {
        ...state, isAdministrator: false, hasFinished: true, token: '', userName: 'Guest', email: '', oldPassword: ''
      };
    case AuthenticationEvent.UPDATE_PASSWORD_ERROR:
      return {
        ...state, isAdministrator: false, hasFinished: true, token: '', userName: 'Guest', oldPassword: ''
      };
    default:
      return state;
  }
};

export default authenticationReducer;
