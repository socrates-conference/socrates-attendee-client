// @flow

import {
  call,
  put
} from 'redux-saga/effects';
import {
  generatePasswordError,
  generatePasswordSuccess,
  loginError,
  loginStarted,
  loginSuccess,
  logoutSuccess, updatePasswordError, updatePasswordNeeded, updatePasswordSuccess
} from './authenticationEvents';
import * as api from '../requests/api';
import * as jwt from 'jsonwebtoken';
import {routeTo} from '../commands/routingCommand';
import config from '../../config';
import * as sjcl from 'sjcl';
import * as bcryptjs from 'bcryptjs';

import {setToken, removeToken} from '../services/jwtStorage';
import {setAuthorizationHeader, removeAuthorizationHeader} from '../requests/clientConfig';

export const jsonWebTokenSecret = config.jwtSecret;
export type LoginCommandData = {
  email: string,
  password: string,
  comesFrom: Object
}

export function* loginSaga(action: LoginCommandData): Iterable<any> {
  yield put(loginStarted());
  try {
    const sha256 = sjcl.hash.sha256.hash(action.password);
    const hash = sjcl.codec.hex.fromBits(sha256);
    const payload = {
      email: action.email,
      hash: hash
    };
    const requestToken = yield call(jwt.sign, payload, config.jwtSecret, {expiresIn: 15});
    const token: ?string = yield call(api.login, requestToken);
    if (token && token.trim().length > 0) {
      const userData = yield call(jwt.verify, token, jsonWebTokenSecret);
      if (userData && !userData.isOneTimePassword) {
        setAuthorizationHeader(token);
        setToken(token);
        yield put(loginSuccess(token, userData));
        yield put(routeTo(action.comesFrom));
      }
      if (userData && userData.isOneTimePassword) {
        const revokePayload = {
          email: action.email
        };
        const revokeToken = yield call(jwt.sign, revokePayload, config.jwtSecret, {expiresIn: 15});
        yield call(api.revokePassword, revokeToken);
        yield put(updatePasswordNeeded(userData, action.password));
      }
    } else {
      removeSession();
      yield put(loginError());
    }
  } catch (error) {
    removeSession();
    yield put(loginError());
  }
}

export function* logoutSaga(): Iterable<any> {
  yield call(removeSession);
  yield put(logoutSuccess());
}

const removeSession = () => {
  removeAuthorizationHeader();
  removeToken();
};


export function* updatePasswordSaga(action: Object): Iterable<any> {
  try {
    const oldPasswordSha256 = sjcl.hash.sha256.hash(action.oldPassword);
    const oldPassword = sjcl.codec.hex.fromBits(oldPasswordSha256);
    const oldPasswordHash = bcryptjs.hashSync(oldPassword, 10);
    const newPasswordSha256 = sjcl.hash.sha256.hash(action.newPassword);
    const newPassword = sjcl.codec.hex.fromBits(newPasswordSha256);
    const payload = {
      email: action.email,
      oldPassword: oldPassword,
      oldPasswordHash: oldPasswordHash,
      newPassword: newPassword
    };
    const requestToken = yield call(jwt.sign, payload, config.jwtSecret, {expiresIn: 15});
    const token: ?string = yield call(api.updatePassword, requestToken);
    if (token && token.trim().length > 0) {
      const userData = yield call(jwt.verify, token, jsonWebTokenSecret);
      if (userData) { // only for flow: userData is never not truthy, otherwise there would be an error
        setAuthorizationHeader(token);
        setToken(token);
        yield put(updatePasswordSuccess(token, userData));
      }
      if (userData && !userData.isOneTimePassword) {
        yield put(routeTo(action.comesFrom));
      }
    } else {
      removeSession();
      yield put(updatePasswordError());
    }
  } catch (error) {
    removeSession();
    yield put(updatePasswordError());
  }
}

export function* forgotPasswordSaga(action: Object): Iterable<any> {
  try {
    const hash = bcryptjs.hashSync(action.email, 10);
    const payload = {
      email: action.email,
      emailHash: hash
    };
    const requestToken = yield call(jwt.sign, payload, config.jwtSecret, {expiresIn: 15});
    const success: ?boolean = yield call(api.generatePassword, requestToken);
    if (success) {
      yield put(generatePasswordSuccess());
    } else {
      yield put(generatePasswordError());
    }
  } catch (err) {
    yield put(generatePasswordError());
  }
}
