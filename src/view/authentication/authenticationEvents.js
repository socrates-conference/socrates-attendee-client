// @flow

export default class AuthenticationEvent {

  static get LOGIN_STARTED(): string {
    return 'AuthenticationEvent/LOGIN_STARTED';
  }

  static get LOGIN_SUCCESS(): string {
    return 'AuthenticationEvent/LOGIN_SUCCESS';
  }

  static get LOGIN_ERROR(): string {
    return 'AuthenticationEvent/LOGIN_ERROR';
  }

  static get LOGOUT_SUCCESS(): string {
    return 'AuthenticationEvent/LOGOUT_SUCCESS';
  }

  static get UPDATE_PASSWORD_NEEDED(): string {
    return 'AuthenticationEvent/UPDATE_PASSWORD_NEEDED';
  }

  static get UPDATE_PASSWORD_SUCCESS(): string {
    return 'AuthenticationEvent/UPDATE_PASSWORD_SUCCESS';
  }

  static get UPDATE_PASSWORD_ERROR(): string {
    return 'AuthenticationEvent/UPDATE_PASSWORD_ERROR';
  }

  static get GENERATE_PASSWORD_SUCCESS(): string {
    return 'AuthenticationEvent/GENERATE_PASSWORD_SUCCESS';
  }

  static get GENERATE_PASSWORD_ERROR(): string {
    return 'AuthenticationEvent/GENERATE_PASSWORD_ERROR';
  }
}

export const loginStarted = () => ({type: AuthenticationEvent.LOGIN_STARTED});
export const logoutSuccess = () => ({type: AuthenticationEvent.LOGOUT_SUCCESS});
export const loginSuccess =
  (token: string, data: Object) => (
    {type: AuthenticationEvent.LOGIN_SUCCESS, token, data}
  );
export const loginError = () => ({type: AuthenticationEvent.LOGIN_ERROR});
export const updatePasswordSuccess =
  (token: string, data: Object) => (
    {type: AuthenticationEvent.UPDATE_PASSWORD_SUCCESS, token, data}
  );
export const updatePasswordNeeded =
  (data: Object, oldPassword: string) => ({type: AuthenticationEvent.UPDATE_PASSWORD_NEEDED, data, oldPassword});
export const updatePasswordError = () => ({type: AuthenticationEvent.UPDATE_PASSWORD_ERROR});
export const generatePasswordSuccess = () => ({type: AuthenticationEvent.GENERATE_PASSWORD_SUCCESS});
export const generatePasswordError = () => ({type: AuthenticationEvent.GENERATE_PASSWORD_ERROR});
