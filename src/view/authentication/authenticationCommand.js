//@flow

export default class AuthenticationCommand {
  static get LOGIN(): string {
    return 'AuthenticationCommand/LOGIN';
  }
  static get LOGOUT(): string {
    return 'AuthenticationCommand/LOGOUT';
  }
  static get UPDATE_PASSWORD(): string {
    return 'AuthenticationCommand/UPDATE_PASSWORD';
  }
  static get GENERATE_PASSWORD(): string {
    return 'AuthenticationCommand/GENERATE_PASSWORD';
  }
}

export const login = (email: string, password: string, comesFrom: Object) => ({
  type: AuthenticationCommand.LOGIN,
  email,
  password,
  comesFrom
});

export const logout = () => ({type: AuthenticationCommand.LOGOUT});

export const generatePassword = (email: string) => ({type: AuthenticationCommand.GENERATE_PASSWORD, email});

export const updatePassword = (email: string, oldPassword: string, newPassword: string, comesFrom: Object) => ({
  type: AuthenticationCommand.UPDATE_PASSWORD,
  email,
  oldPassword,
  newPassword,
  comesFrom
});
