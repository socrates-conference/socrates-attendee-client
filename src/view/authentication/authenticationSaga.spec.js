// @flow
import axios from 'axios';
import AuthenticationEvent from './authenticationEvents';
import SagaTester from 'redux-saga-tester';
import rootReducer from '../reducers/rootReducer';
import rootSaga from '../sagas/rootSaga';
import {
  login,
  logout, updatePassword
} from './authenticationCommand';
import * as jwtStorage from '../services/jwtStorage';

const TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAdGVzdC5kZSIsIm5hbWUiOiJ0ZXN0IiwiaXNBZG1pbmlzd' +
  'HJhdG9yIjp0cnVlfQ.34sGwDqBKCYRI66Ewu_v_sTZJfN3_02Rjch3AD3kXBc';
const EXPIRED_TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAdGVzdC5kZSIsIm5hbWUiOiJ0ZXN0IiwiaXNBZ' +
  'G1pbmlzdHJhdG9yIjp0cnVlLCJleHAiOjE1MjUzOTcyNzh9.ljnvovLZCbhFoQrwdW0NjqEkglZPR2JRaEXn7RAGGLA';

jest.mock('axios');

describe('LoginSaga', () => {
  let sagaTester: SagaTester;
  let promise;
  let setTokenSpy;
  let removeTokenSpy;
  const INITIAL_STATE = {
    placeHolder: {},
    authentication: {
      token: '',
      userName: 'Guest',
      isAdministrator: false,
      hasFinished: false
    }
  };

  beforeEach(() => {
    sagaTester = new SagaTester({INITIAL_STATE, reducers: rootReducer});
    sagaTester.start(rootSaga);

    axios.post.mockImplementation(() => promise);
    setTokenSpy = jest.spyOn(jwtStorage, 'setToken');
    removeTokenSpy = jest.spyOn(jwtStorage, 'removeToken');
  });

  afterEach(() => {
    sagaTester.reset();
    axios.post.mockReset();
    setTokenSpy.mockRestore();
    removeTokenSpy.mockRestore();
  });

  it('should dispatch LoginStarted event', async () => {
    sagaTester.dispatch(login('test', 'test@test.de', {pathname: '/profile'}));
    await sagaTester.waitFor(AuthenticationEvent.LOGIN_STARTED);
  });

  describe('when login is successful', () => {
    beforeEach(async () => {
      promise = Promise.resolve({
        status: 200,
        data: {
          token: TOKEN
        }
      });
      sagaTester.dispatch(login('test', 'test@test.de', {pathname: '/profile'}));
      await sagaTester.waitFor(AuthenticationEvent.LOGIN_SUCCESS);
    });

    it('should retrieve jwt token', () => {
      expect(axios.post.mock.calls).toHaveLength(1);
    });

    it('should add jwt token to axios headers', () => {
      expect(axios.defaults.headers.common['Authorization']).toEqual(`Bearer ${TOKEN}`);
    });

    it('should store received jwt token', () => {
      expect(removeTokenSpy).not.toHaveBeenCalled();
      expect(setTokenSpy).toHaveBeenCalledWith(TOKEN);
    });

    it('should dispatch LoginSuccess event', () => {
      expect(sagaTester.getCalledActions()).toContainEqual({
        data: {
          email: 'test@test.de', isAdministrator: true,
          name: 'test'
        },
        token: TOKEN,
        type: 'AuthenticationEvent/LOGIN_SUCCESS'
      });
    });

    it('should reroute to the page where the user came from', async () => {
      expect(sagaTester.getCalledActions())
        .toContainEqual({type: 'RoutingCommand/ROUTE_TO', location: {pathname: '/profile'}});
    });
  });

  describe('when jwt token is expired', () => {
    beforeEach(async () => {
      promise = Promise.resolve({
        status: 200,
        data: {
          token: EXPIRED_TOKEN
        }
      });
      sagaTester.dispatch(login('test', 'test@test.de', {pathname: '/profile'}));
      await sagaTester.waitFor(AuthenticationEvent.LOGIN_ERROR);
    });

    it('should clear axios header', () => {
      expect(axios.defaults.headers.common['Authorization']).toBeUndefined();
    });

    it('should remove jwt token from store', () => {
      expect(removeTokenSpy).toHaveBeenCalled();
      expect(setTokenSpy).not.toHaveBeenCalled();
    });

    it('should dispatch LoginError event', () => {
      expect(sagaTester.getCalledActions()).toContainEqual({type: AuthenticationEvent.LOGIN_ERROR});
    });
  });

  describe('when jwt token is empty', () => {
    beforeEach(async () => {
      promise = Promise.resolve({
        status: 200,
        data: {
          token: ''
        }
      });
      sagaTester.dispatch(login('test', 'test@test.de', {pathname: '/profile'}));
      await sagaTester.waitFor(AuthenticationEvent.LOGIN_ERROR);
    });

    it('should clear axios header', () => {
      expect(axios.defaults.headers.common['Authorization']).toBeUndefined();
    });

    it('should remove jwt token from store', () => {
      expect(removeTokenSpy).toHaveBeenCalled();
      expect(setTokenSpy).not.toHaveBeenCalled();
    });

    it('should dispatch LoginError event', () => {
      expect(sagaTester.getCalledActions()).toContainEqual({type: AuthenticationEvent.LOGIN_ERROR});
    });
  });

  describe('when api is down', () => {
    beforeEach(async () => {
      promise = Promise.reject(new Error());
      sagaTester.dispatch(login('test', 'test@test.de', {pathname: '/profile'}));
      await sagaTester.waitFor(AuthenticationEvent.LOGIN_ERROR);
    });

    it('should clear axios header', () => {
      expect(axios.defaults.headers.common['Authorization']).toBeUndefined();
    });

    it('should remove jwt token from store', () => {
      expect(removeTokenSpy).toHaveBeenCalled();
      expect(setTokenSpy).not.toHaveBeenCalled();
    });

    it('should dispatch LoginError event', () => {
      expect(sagaTester.getCalledActions()).toContainEqual({type: AuthenticationEvent.LOGIN_ERROR});
    });
  });
});

describe('LogoutSaga', () => {
  let sagaTester: SagaTester;
  let setTokenSpy;
  let removeTokenSpy;
  const INITIAL_STATE = {
    placeHolder: {},
    authentication: {
      token: '',
      userName: 'Guest',
      isAdministrator: false,
      hasFinished: false
    }
  };

  beforeEach(async () => {
    setTokenSpy = jest.spyOn(jwtStorage, 'setToken');
    removeTokenSpy = jest.spyOn(jwtStorage, 'removeToken');

    sagaTester = new SagaTester({INITIAL_STATE, reducers: rootReducer});
    sagaTester.start(rootSaga);
    sagaTester.dispatch(logout());
    await sagaTester.waitFor(AuthenticationEvent.LOGOUT_SUCCESS);
  });

  afterEach(() => {
    sagaTester.reset();
    setTokenSpy.mockRestore();
    removeTokenSpy.mockRestore();
  });

  it('should remove jwt token from axios headers', () => {
    expect(axios.defaults.headers.common['Authorization']).toBeUndefined();
  });

  it('should remove jwt token from store', () => {
    expect(removeTokenSpy).toHaveBeenCalled();
    expect(setTokenSpy).not.toHaveBeenCalled();
  });

  it('should dispatch LogoutSuccess event', () => {
    expect(sagaTester.getCalledActions()).toContainEqual({type: AuthenticationEvent.LOGOUT_SUCCESS});
  });
});

describe('updatePasswordSaga', () => {
  let sagaTester: SagaTester;
  let promise;
  const INITIAL_STATE = {
    placeHolder: {},
    authentication: {
      token: '',
      userName: 'Guest',
      email: '',
      isAdministrator: false,
      isOneTimePassword: false,
      hasFinished: false
    }
  };

  beforeEach(() => {
    sagaTester = new SagaTester({INITIAL_STATE, reducers: rootReducer});
    sagaTester.start(rootSaga);

    axios.post.mockImplementation(() => promise);
  });

  afterEach(() => {
    sagaTester.reset();
    axios.post.mockReset();
  });

  describe('when update is successful', () => {
    beforeEach(async () => {
      promise = Promise.resolve({
        status: 200,
        data: {
          token: TOKEN
        }
      });
      sagaTester.dispatch(updatePassword('test@test.de', 'oldPassword', 'newPassword', {pathname: '/somewhere'}));
      await sagaTester.waitFor(AuthenticationEvent.UPDATE_PASSWORD_SUCCESS);
    });

    it('should retrieve auth token', () => {
      expect(axios.post.mock.calls).toHaveLength(1);
    });
    it('should add auth token to axios headers', () => {
      expect(axios.defaults.headers.common['Authorization']).toEqual(`Bearer ${TOKEN}`);
    });
    it('should dispatch UPDATE_PASSWORD_SUCCESS event', () => {
      expect(sagaTester.getCalledActions()).toContainEqual({
        data: {
          email: 'test@test.de', isAdministrator: true,
          name: 'test'
        },
        token: TOKEN,
        type: 'AuthenticationEvent/UPDATE_PASSWORD_SUCCESS'
      });
    });
    it('should reroute to the page where the user came from', async () => {
      expect(sagaTester.getCalledActions())
        .toContainEqual({type: 'RoutingCommand/ROUTE_TO', location: {pathname: '/somewhere'}});
    });
  });

  describe('when token is expired', () => {
    beforeEach(async () => {
      promise = Promise.resolve({
        status: 200,
        data: {
          token: EXPIRED_TOKEN
        }
      });
      sagaTester.dispatch(updatePassword('test@test.de', 'oldPassword', 'newPassword', {pathname: '/somewhere'}));
      await sagaTester.waitFor(AuthenticationEvent.UPDATE_PASSWORD_ERROR);
    });
    it('should clear axios header', () => {
      expect(axios.defaults.headers.common['Authorization']).toBeUndefined();
    });
    it('should dispatch UPDATE_PASSWORD_ERROR event', () => {
      expect(sagaTester.getCalledActions()).toContainEqual({type: AuthenticationEvent.UPDATE_PASSWORD_ERROR});
    });
  });

  describe('when token is empty', () => {
    beforeEach(async () => {
      promise = Promise.resolve({
        status: 200,
        data: {
          token: ''
        }
      });
      sagaTester.dispatch(updatePassword('test@test.de', 'oldPassword', 'newPassword', {pathname: '/somewhere'}));
      await sagaTester.waitFor(AuthenticationEvent.UPDATE_PASSWORD_ERROR);
    });
    it('should clear axios header', () => {
      expect(axios.defaults.headers.common['Authorization']).toBeUndefined();
    });
    it('should dispatch UPDATE_PASSWORD_ERROR event', () => {
      expect(sagaTester.getCalledActions()).toContainEqual({type: AuthenticationEvent.UPDATE_PASSWORD_ERROR});
    });
  });

  describe('when api is down', () => {
    beforeEach(async () => {
      promise = Promise.reject(new Error());
      sagaTester.dispatch(updatePassword('test@test.de', 'oldPassword', 'newPassword', {pathname: '/somewhere'}));
      await sagaTester.waitFor(AuthenticationEvent.UPDATE_PASSWORD_ERROR);
    });
    it('should clear axios header', () => {
      expect(axios.defaults.headers.common['Authorization']).toBeUndefined();
    });
    it('should dispatch UPDATE_PASSWORD_ERROR event', () => {
      expect(sagaTester.getCalledActions()).toContainEqual({type: AuthenticationEvent.UPDATE_PASSWORD_ERROR});
    });
  });

});
