import {shallow} from 'enzyme';
import React from 'react';
import {LoginContainer} from './LoginContainer';

describe('LoginContainer', () => {
  let login;
  describe('shows login form', () => {
    const state = {
      token: '',
      userName: 'Guest',
      email: '',
      isAdministrator: false,
      isOneTimePassword: false,
      hasFinished: false
    };
    beforeEach(() => {
      login = shallow(<LoginContainer state={state}/>);
    });

    it('without crashing', () => {
      expect(login).toHaveLength(1);
    });

    it('correctly', () => {
      expect(login).toMatchSnapshot();
    });
  });
  describe('shows change password form', () => {
    const state = {
      token: '',
      userName: 'Guest',
      email: '',
      isAdministrator: false,
      isOneTimePassword: true,
      hasFinished: true
    };
    beforeEach(() => {
      login = shallow(<LoginContainer state={state}/>);
    });

    it('without crashing', () => {
      expect(login).toHaveLength(1);
    });

    it('correctly', () => {
      expect(login).toMatchSnapshot();
    });
  });
});