import {mount} from 'enzyme';
import React from 'react';
import {LoginFormContainer} from './LoginFormContainer';
import {spy} from 'sinon';

const state = {
  token: '',
  result: {isSuccess: false, isAdministrator: false},
  hasFinished: false
};

describe('LoginFormContainer', () => {
  let login;
  const loginSpy = spy();
  const routeSpy = spy();
  beforeEach(() => {
    login = mount(
      <LoginFormContainer
        from="/" login={loginSpy} state={state} location={{state: {from: '/'}}}
        routeTo={routeSpy}
      />
    );
  });

  afterEach(()=>{
    loginSpy.resetHistory();
    routeSpy.resetHistory();
  });

  it('renders without crashing', () => {
    expect(login).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(login).toMatchSnapshot();
  });

  it('onPasswordChanged updates state', () => {
    login.instance().onPasswordChange('onePassword');
    login.update();
    expect(login.state('password')).toEqual('onePassword');
  });

  it('onEmailChanged updates state', () => {
    login.instance().onEmailChange('newEmail@mail.com');
    login.update();
    expect(login.state('email')).toEqual('newEmail@mail.com');
  });

  it('empty password is invalid', () => {
    login.instance().onPasswordChange('');
    login.update();
    expect(login.state('hasValidPassword')).toBe(false);
  });

  it('non empty password is valid', () => {
    login.instance().onPasswordChange('pwd');
    login.update();
    expect(login.state('hasValidPassword')).toBe(true);
  });
  it('empty email is invalid', () => {
    login.instance().onEmailChange('');
    login.update();
    expect(login.state('hasValidEmail')).toBe(false);
  });

  it('correct formatted email is valid', () => {
    login.instance().onEmailChange('valid@email.de');
    login.update();
    expect(login.state('hasValidEmail')).toBe(true);
  });

  it('malformed email is invalid', () => {
    login.instance().onEmailChange('noemail');
    login.update();
    expect(login.state('hasValidEmail')).toBe(false);
  });
  it('on submit start login is called', () => {
    login.instance().login();
    login.update();
    expect(loginSpy.calledOnce).toBe(true);
  });
});