// @flow

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import {connect} from 'react-redux';
import type {AuthenticationState} from '../authenticationReducer';

import LoginForm from './LoginForm';
import isValidEmailFormat from '../../../utils/isValidEmailFormat';
import {login} from '../authenticationCommand';
import {routeTo} from '../../commands/routingCommand';
import {withRouter} from 'react-router';


export type Props = {
  from: string,
  location: Object,
  login: (string, string, Object) => void,
  routeTo: (Object) => void,
  state: AuthenticationState;
}

type State = {
  email: string,
  password: string,
  hasValidEmail: boolean,
  hasValidPassword: boolean
};


export class LoginFormContainer extends Component<Props, State> {

  static propTypes = {
    from: PropTypes.string,
    location: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired,
    state: PropTypes.any
  };

  state = {
    email: '',
    password: '',
    hasValidEmail: false,
    hasValidPassword: false
  };


  login = () => {
    const redirectGoal = this.getRedirectGoal();
    this.props.login(this.state.email, this.state.password, redirectGoal);
  };

  getRedirectGoal() {
    return this.props.location.state && this.props.location.state.from
      ? {pathname: this.props.location.state.from} : {pathname: '/profile'};
  }

  onEmailChange = (email: string) => {
    const hasValidEmail = email.trim().length > 0 && isValidEmailFormat(email);
    this.setState({...this.state, email, hasValidEmail});
  };

  onPasswordChange = (password: string) => {
    const hasValidPassword = password.trim().length > 0;
    this.setState({...this.state, password, hasValidPassword});
  };

  render = () => {
    const {email, password, hasValidEmail, hasValidPassword} = this.state;
    const showErrorMessage = this.props.state.hasFinished && this.props.state.token.trim().length === 0;
    return (
      <LoginForm
        email={email}
        hasValidEmail={hasValidEmail}
        password={password}
        hasValidPassword={hasValidPassword}
        onEmailChange={this.onEmailChange}
        onPasswordChange={this.onPasswordChange}
        onSubmit={this.login}
        showErrorMessage={showErrorMessage}
        passwordForgotten={this.props.routeTo}
      />
    );
  };
}

const mapStateToProps = (state) => {
  return {state: {...state.authentication}};
};

const mapDispatchToProps = {
  login, routeTo
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginFormContainer));
