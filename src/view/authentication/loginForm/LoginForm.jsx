// @flow

import React from 'react';
import '../authentication.css';

export type Props = {
  email: string,
  hasValidEmail: boolean,
  hasValidPassword: boolean,
  onPasswordChange: (value: string) => void,
  onEmailChange: (value: string) => void,
  onSubmit: () => void,
  password: string,
  passwordForgotten: (Object) => void,
  showErrorMessage: boolean
}

function AuthenticationFailed() {
  return (
    <div className="row card-error">
      <div className="col-12">
        Log in failed. Email or password are incorrect.
      </div>
    </div>
  );
}

export default function LoginForm(props: Props) {
  const klass = (isValid) => `form-control mb-2 mr-sm-2 ${isValid ? 'is-valid' : 'is-invalid'}`;
  const emailClass = klass(props.hasValidEmail);
  const passwordClass = klass(props.hasValidPassword);

  const onEmailChange = (event: SyntheticEvent<HTMLInputElement>) => {
    props.onEmailChange(event.currentTarget.value);
  };

  const onPasswordChange = (event: SyntheticEvent<HTMLInputElement>) => {
    props.onPasswordChange(event.currentTarget.value);
  };

  const onSubmit = (event: Event) => {
    event.preventDefault();
    props.onSubmit();
  };

  const isEnabled = (): boolean => {
    return props.hasValidEmail && props.hasValidPassword;
  };

  return (
    <div className="container">
      <div className="card border-secondary card-login">
        <div className="card-body">
          <b>Hint</b>:<br/>
          The login page allows the participants to access their profile page. This means that as long as you have
          not received a spot for the conference (through sponsoring or the lottery), a log in is not possible.<br/>
          Thank you for your understanding.
        </div>
      </div>
      <form className="form" onSubmit={onSubmit}>
        <div className="card border-secondary card-login">
          <div className="card-header">
            <h2>Login</h2>
          </div>
          <div className="card-body">
            <div className="row">
              <div className="col-sm-12 col-md-3 align-self-center">
                <label htmlFor="auth-email">
                  Email:
                </label>
              </div>
              <div className="col-sm-12 col-md-9">
                <input
                  id="auth-email" name="email" type="email" className={emailClass} placeholder="Your email"
                  required
                  onChange={onEmailChange} value={props.email}/>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-md-3 align-self-center">
                <label htmlFor="auth-password">
                  Password:
                </label>
              </div>
              <div className="col-sm-12 col-md-9">
                <input
                  id="auth-password" name="password" type="password" className={passwordClass} required
                  onChange={onPasswordChange} value={props.password}/>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-md-3">
                <button id="auth-login" className="btn btn-primary mb-2" disabled={!isEnabled()} type="submit">
                  Login
                </button>
              </div>
              <div className="col-sm-12 col-md-3 offset-md-6">
                <button
                  id="auth-forgotten" className="btn btn-link"
                  onClick={() => props.passwordForgotten({pathname: '/forgot-password'})} title="I forgot my password">
                  Forgot Password?
                </button>
              </div>
            </div>
            {props.showErrorMessage ? <AuthenticationFailed/> : null}
          </div>
        </div>
      </form>
    </div>
  );
}
