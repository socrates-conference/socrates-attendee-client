import {mount} from 'enzyme';
import React from 'react';
import ForgotPasswordForm from './ForgotPasswordForm';

describe('ForgotPasswordForm', () => {
  let wrapper;
  describe('renders', () => {
    beforeEach(() => {
      wrapper = mount(
        <ForgotPasswordForm
          email="email@valid.de" hasValidEmail={true} onEmailChange={() => {}}
          onSubmit={() => {}} showSuccessMessage={true}
        />);
    });

    it('without crashing', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });
});