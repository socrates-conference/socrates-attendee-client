// @flow

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import {connect} from 'react-redux';
import type {AuthenticationState} from '../authenticationReducer';
import isValidEmailFormat from '../../../utils/isValidEmailFormat';
import {generatePassword} from '../authenticationCommand';
import ForgotPasswordForm from './ForgotPasswordForm';


export type Props = {
  generatePassword: (string) => void,
  state: AuthenticationState;
}

type State = {
  email: string,
  password: string,
  hasValidEmail: boolean,
  hasValidPassword: boolean,
  showSuccessMessage: boolean
};


export class ForgotPasswordContainer extends Component<Props, State> {

  static propTypes = {
    generatePassword: PropTypes.func.isRequired,
    state: PropTypes.any
  };

  state = {
    email: '',
    password: '',
    hasValidEmail: false,
    hasValidPassword: false,
    showSuccessMessage: false
  };


  submit = (event: Event) => {
    event.preventDefault();
    this.setState({showSuccessMessage: false});
    this.props.generatePassword(this.state.email);
    this.setState({showSuccessMessage: true});
  };

  onEmailChange = (email: string) => {
    const hasValidEmail = email.trim().length > 0 && isValidEmailFormat(email);
    this.setState({...this.state, email, hasValidEmail});
  };

  render = () => {
    const {email, hasValidEmail, showSuccessMessage} = this.state;
    return (
      <div id="login-container">

        <ForgotPasswordForm
          email={email}
          hasValidEmail={hasValidEmail}
          onEmailChange={this.onEmailChange}
          onSubmit={this.submit}
          showSuccessMessage={showSuccessMessage}
        />
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {state: {...state.authentication}};
};

const mapDispatchToProps = {
  generatePassword
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordContainer);
