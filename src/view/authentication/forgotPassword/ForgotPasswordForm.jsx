// @flow

import React from 'react';

export type Props = {
  email: string,
  hasValidEmail: boolean,
  onEmailChange: (value: string) => void,
  onSubmit: (Event) => void,
  showSuccessMessage: boolean
}

function GenerationSucceeded() {
  return (
    <div className="row card-error">
      <div className="col-12">
        You will receive soon the new password per E-Mail.
      </div>
    </div>
  );
}

export default function ForgotPasswordForm(props: Props) {
  const klass = (isValid) => `form-control mb-2 mr-sm-2 ${isValid ? 'is-valid' : 'is-invalid'}`;
  const emailClass = klass(props.hasValidEmail);

  return (
    <div className="container">
      <form className="form" onSubmit={props.onSubmit}>
        <div className="card border-secondary card-login">
          <div className="card-header">
            <h2>I need a new Password</h2>
          </div>
          <div className="card-body">
            <div className="row">
              <div className="col-3 align-self-center">
                <label htmlFor="generate-onetime-email">
                  Email:
                </label>
              </div>
              <div className="col-9">
                <input
                  id="generate-onetime-email" name="email" type="email"
                  className={emailClass} placeholder="Your email"
                  required
                  onChange={(ev: SyntheticEvent<HTMLInputElement>) => props.onEmailChange(ev.currentTarget.value)}
                  value={props.email}/>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <button
                  id="generate-onetime-submit" className="btn btn-primary mb-2" disabled={!props.hasValidEmail}
                  type="submit">
                  Please generate a new one for me
                </button>
              </div>
            </div>
            {props.showSuccessMessage ? <GenerationSucceeded/> : null}
          </div>
        </div>
      </form>
    </div>
  );
}