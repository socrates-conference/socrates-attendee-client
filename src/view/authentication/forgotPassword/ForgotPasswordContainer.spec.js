import {shallow} from 'enzyme';
import React from 'react';
import {ForgotPasswordContainer} from './ForgotPasswordContainer';
import {spy} from 'sinon';

describe('ForgotPasswordContainer', () => {
  let wrapper;
  describe('renders', () => {
    const state = {
      token: '',
      userName: 'Guest',
      email: '',
      isAdministrator: false,
      isOneTimePassword: false,
      hasFinished: false
    };
    beforeEach(() => {
      wrapper = shallow(<ForgotPasswordContainer state={state} generatePassword={() => {}}/>);
    });

    it('without crashing', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });
  describe('after render', () => {
    const generateSpy = spy();
    const state = {
      token: '',
      userName: 'Guest',
      email: '',
      isAdministrator: false,
      isOneTimePassword: true,
      hasFinished: true
    };
    beforeEach(() => {
      wrapper = shallow(<ForgotPasswordContainer state={state} generatePassword={generateSpy}/>);
    });
    afterEach(() => {
      generateSpy.resetHistory();
    });

    it('onEmailChanged updates state', () => {
      wrapper.instance().onEmailChange('newEmail@mail.com');
      wrapper.update();
      expect(wrapper.state('email')).toEqual('newEmail@mail.com');
    });
    it('empty email is invalid', () => {
      wrapper.instance().onEmailChange('');
      wrapper.update();
      expect(wrapper.state('hasValidEmail')).toBe(false);
    });

    it('correct formatted email is valid', () => {
      wrapper.instance().onEmailChange('valid@email.de');
      wrapper.update();
      expect(wrapper.state('hasValidEmail')).toBe(true);
    });

    it('malformed email is invalid', () => {
      wrapper.instance().onEmailChange('noemail');
      wrapper.update();
      expect(wrapper.state('hasValidEmail')).toBe(false);
    });
    it('on submit generate password is called', () => {
      wrapper.instance().submit({preventDefault: () => {}});
      wrapper.update();
      expect(generateSpy.calledOnce).toBe(true);
    });

  });
});