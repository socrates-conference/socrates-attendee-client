// @flow

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {withRouter} from 'react-router';
import OneTimePasswordForm from './OneTimePasswordForm';
import type {AuthenticationState} from '../authenticationReducer';
import {updatePassword} from '../authenticationCommand';


export type Props = {
  from: string,
  location: Object,
  updatePassword: (string, string, string, Object) => void,
  state: AuthenticationState;
}

type State = {
  oldPassword: string,
  hasValidOldPassword: boolean,
  newPassword: string,
  hasValidNewPassword: boolean,
  confirmNewPassword: string,
  hasValidConfirmNewPassword: boolean
};


export class OneTimePasswordContainer extends Component<Props, State> {

  static propTypes = {
    from: PropTypes.string,
    location: PropTypes.object.isRequired,
    state: PropTypes.any,
    updatePassword: PropTypes.func.isRequired
  };

  state = {
    oldPassword: '',
    hasValidOldPassword: false,
    newPassword: '',
    hasValidNewPassword: false,
    confirmNewPassword: '',
    hasValidConfirmNewPassword: false
  };


  updatePassword = () => {
    const redirectGoal = this.getRedirectGoal();
    this.props.updatePassword(this.props.state.email, this.state.oldPassword, this.state.newPassword, redirectGoal);
  };

  getRedirectGoal() {
    return this.props.location.state && this.props.location.state.from
      ? {pathname: this.props.location.state.from} : {pathname: '/profile'};
  }

  onOldPasswordChange = (oldPassword: string) => {
    const hasValidOldPassword = oldPassword.trim().length > 0 && oldPassword === this.props.state.oldPassword;
    this.setState({...this.state, oldPassword, hasValidOldPassword});
  };

  onNewPasswordChange = (newPassword: string) => {
    const hasValidNewPassword = newPassword.trim().length > 0;
    this.setState({...this.state, newPassword, hasValidNewPassword});
  };

  onConfirmNewPasswordChange = (confirmNewPassword: string) => {
    const hasValidConfirmNewPassword =
      confirmNewPassword.trim().length > 0 && confirmNewPassword === this.state.newPassword;
    this.setState({...this.state, confirmNewPassword, hasValidConfirmNewPassword});
  };

  render = () => {
    const {
      oldPassword, hasValidOldPassword, newPassword, hasValidNewPassword,
      confirmNewPassword, hasValidConfirmNewPassword
    } = this.state;
    const showErrorMessage = this.props.state.hasFinished && this.props.state.token.trim().length === 0;
    return (
      <OneTimePasswordForm
        email={this.props.state.email}
        oldPassword={oldPassword} newPassword={newPassword} confirmNewPassword={confirmNewPassword}
        onOldPasswordChange={this.onOldPasswordChange} onNewPasswordChange={this.onNewPasswordChange}
        onConfirmNewPasswordChange={this.onConfirmNewPasswordChange} showErrorMessage={showErrorMessage}
        hasValidOldPassword={hasValidOldPassword} hasValidNewPassword={hasValidNewPassword}
        hasValidConfirmNewPassword={hasValidConfirmNewPassword}
        onSubmit={this.updatePassword}
      />
    );
  };
}

const mapStateToProps = (state) => {
  return {state: {...state.authentication}};
};

const mapDispatchToProps = {
  updatePassword
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OneTimePasswordContainer));
