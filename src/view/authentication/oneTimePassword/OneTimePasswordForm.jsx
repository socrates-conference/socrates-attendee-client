// @flow

import React from 'react';
import PropTypes from 'prop-types';

export type Props = {
  confirmNewPassword: string,
  email: string,
  hasValidConfirmNewPassword: boolean,
  hasValidNewPassword: boolean,
  hasValidOldPassword: boolean,
  newPassword: string,
  oldPassword: string,
  onConfirmNewPasswordChange: (value: string) => void,
  onNewPasswordChange: (value: string) => void,
  onOldPasswordChange: (value: string) => void,
  onSubmit: () => void,
  showErrorMessage: boolean
}

function UpdatePasswordFailed() {
  return (
    <div className="row card-error">
      <div className="col-12">
        Cannot update your password. Maybe you mistyped something?
      </div>
    </div>
  );
}

export default function OneTimePasswordForm(props: Props) {
  const klass = (isValid) => `form-control mb-2 mr-sm-2 ${isValid ? 'is-valid' : 'is-invalid'}`;
  const emailClass = klass(true);
  const oldPasswordClass = klass(props.hasValidOldPassword);
  const newPasswordClass = klass(props.hasValidNewPassword);
  const confirmNewPasswordClass = klass(props.hasValidConfirmNewPassword);

  const onOldPwdChange = (event: SyntheticEvent<HTMLInputElement>) => {
    props.onOldPasswordChange(event.currentTarget.value);
  };
  const onNewPwdChange = (event: SyntheticEvent<HTMLInputElement>) => {
    props.onNewPasswordChange(event.currentTarget.value);
  };
  const onConfirmNewPwdChange = (event: SyntheticEvent<HTMLInputElement>) => {
    props.onConfirmNewPasswordChange(event.currentTarget.value);
  };

  const onSubmit = (event: Event) => {
    event.preventDefault();
    props.onSubmit();
  };

  const isEnabled = (): boolean => {
    return props.hasValidOldPassword && props.hasValidNewPassword && props.hasValidConfirmNewPassword;
  };

  return (
    <div className="container">
      <form className="form" onSubmit={onSubmit}>
        <div className="card border-secondary card-login">
          <div className="card-header">
            <h2>Change Your Password</h2>
          </div>
          <div className="card-body">
            <div className="row">
              <div className="col-sm-12 col-md-3  align-self-center">
                <label htmlFor="auth-email">
                  Email:
                </label>
              </div>
              <div className="col-sm-12 col-md-9 ">
                <input
                  id="one-time-email" name="email" type="email" className={emailClass} placeholder="Your email"
                  required readOnly
                  value={props.email}/>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-md-3 align-self-center">
                <label htmlFor="one-time-old-password">
                  Old password:
                </label>
              </div>
              <div className="col-sm-12 col-md-9">
                <input
                  id="one-time-old-password" name="old-password" type="password" className={oldPasswordClass} required
                  onChange={onOldPwdChange} value={props.oldPassword}/>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-md-3 align-self-center">
                <label htmlFor="one-time-new-password">
                  New password:
                </label>
              </div>
              <div className="col-sm-12 col-md-9">
                <input
                  id="one-time-new-password" name="new-password" type="password" className={newPasswordClass} required
                  onChange={onNewPwdChange} value={props.newPassword}/>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-md-3 align-self-center">
                <label htmlFor="one-time-confirm-password">
                  Confirm password:
                </label>
              </div>
              <div className="col-sm-12 col-md-9">
                <input
                  id="one-time-confirm-password" name="confirm-password" type="password"
                  className={confirmNewPasswordClass} required
                  onChange={onConfirmNewPwdChange} value={props.confirmNewPassword}/>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <button id="one-time-update" className="btn btn-primary mb-2" disabled={!isEnabled()} type="submit">
                  Update password
                </button>
              </div>
            </div>
            {props.showErrorMessage ? <UpdatePasswordFailed/> : null}
          </div>
        </div>
      </form>
    </div>
  );
}

OneTimePasswordForm.propTypes = {
  confirmNewPassword: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  hasValidConfirmNewPassword: PropTypes.bool.isRequired,
  hasValidNewPassword: PropTypes.bool.isRequired,
  hasValidOldPassword: PropTypes.bool.isRequired,
  newPassword: PropTypes.string.isRequired,
  oldPassword: PropTypes.string.isRequired,
  onConfirmNewPasswordChange: PropTypes.func.isRequired,
  onNewPasswordChange: PropTypes.func.isRequired,
  onOldPasswordChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  showErrorMessage: PropTypes.bool.isRequired
};