import React from 'react';
import {mount} from 'enzyme';
import * as sinon from 'sinon';
import OneTimePasswordForm from './OneTimePasswordForm';

describe('(Component) OneTimePasswordForm', () => {
  let form;
  describe('renders', () => {
    beforeEach(() => {
      form = _mount({});
    });
    it('without exploding', () => {
      expect(form).toHaveLength(1);
    });

    it('correctly', () => {
      expect(form).toMatchSnapshot();
    });

    it('with error message', () => {
      form = _mount({showErrorMessage: true});
      expect(form.text()).toContain('Cannot update your password. Maybe you mistyped something?');
      expect(form).toMatchSnapshot();
    });
  });
  describe('filled with valid data', () => {
    let spy;
    beforeEach(() => {
      spy = sinon.spy();
      form = _mount({
        onSubmit: spy
      });
    });
    afterEach(() => {
      spy.resetHistory();
    });
    it('button is enabled', () => {
      expect(form.find('#one-time-update').prop('disabled')).toBe(false);
    });
    it('on button click submit is called', () => {
      form.find('#one-time-update').simulate('submit');
      expect(spy.calledOnce).toBe(true);
    });

  });
  describe('button is disabled', () => {
    it('when old password is empty', () => {
      form = _mount({
        hasValidOldPassword: false,
        oldPassword: ''
      });
      expect(form.find('#one-time-update').prop('disabled')).toBe(true);
    });
    it('when new password is empty', () => {
      form = _mount({
        hasValidNewPassword: false,
        newPassword: ''
      });
      expect(form.find('#one-time-update').prop('disabled')).toBe(true);
    });
    it('when confirm new password is empty', () => {
      form = _mount({
        confirmNewPassword: '',
        hasValidConfirmNewPassword: false
      });
      expect(form.find('#one-time-update').prop('disabled')).toBe(true);
    });
    it('when new password and confirm new password are different', () => {
      form = _mount({
        confirmNewPassword: 'anotherPassword',
        hasValidConfirmNewPassword: false,
        hasValidNewPassword: true,
        newPassword: 'newPassword'
      });
      expect(form.find('#one-time-update').prop('disabled')).toBe(true);
    });
  });

  describe('change is detected', () => {
    function setInputValue(selector, value) {
      const wrapper = form.find(selector);
      wrapper.instance().value = value;
      wrapper.simulate('change', {target: {value}});
      form.update();
    }

    let spy;
    beforeEach(() => {
      spy = sinon.spy();
    });
    afterEach(() => {
      spy.resetHistory();
    });
    it('when old password changes, changes are registered', () => {
      form = _mount({onOldPasswordChange: spy});
      setInputValue('#one-time-old-password', 'anotherPassword');
      expect(spy.withArgs('anotherPassword').calledOnce).toBe(true);
    });
    it('when new password changes, changes are registered', () => {
      form = _mount({onNewPasswordChange: spy});
      setInputValue('#one-time-new-password', 'anotherPassword');
      expect(spy.withArgs('anotherPassword').calledOnce).toBe(true);
    });
    it('when confirm new  password changes, changes are registered', () => {
      form = _mount({onConfirmNewPasswordChange: spy});
      setInputValue('#one-time-confirm-password', 'anotherPassword');
      expect(spy.withArgs('anotherPassword').calledOnce).toBe(true);
    });
  });

});

const defaultProps = {
  confirmNewPassword: 'newPassword',
  email: 'email@valid.de',
  hasValidConfirmNewPassword: true,
  hasValidNewPassword: true,
  hasValidOldPassword: true,
  newPassword: 'newPassword',
  oldPassword: 'oldPassword',
  onConfirmNewPasswordChange: () => {},
  onNewPasswordChange: () => {},
  onOldPasswordChange: () => {},
  onSubmit: () => {},
  showErrorMessage: false
};
const _mount = (props) => {
  const finalProps = {...defaultProps, ...props};
  return mount(<OneTimePasswordForm {...finalProps}/>);
};
