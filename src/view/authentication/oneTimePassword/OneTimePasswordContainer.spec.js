import {mount} from 'enzyme';
import React from 'react';
import {OneTimePasswordContainer} from './OneTimePasswordContainer';
import {spy} from 'sinon';

describe('OneTimePasswordContainer', () => {
  let wrapper;
  const updateSpy = spy();
  const state = {
    token: 'TOKEN',
    userName: 'userX',
    email: 'email@valid.de',
    isAdministrator: false,
    isOneTimePassword: true,
    hasFinished: true
  };
  beforeEach(() => {
    wrapper = mount(
      <OneTimePasswordContainer
        from="/" state={state} location={{state: {from: '/'}}} updatePassword={updateSpy}
      />);
  });

  afterEach(() => {
    updateSpy.resetHistory();
  });
  it('without crashing', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('empty old password is invalid', () => {
    wrapper.instance().onOldPasswordChange('');
    expect(wrapper.state('hasValidOldPassword')).toBe(false);
  });

  it('state is updated when old password changes', () => {
    wrapper.instance().onOldPasswordChange('password');
    expect(wrapper.state('oldPassword')).toEqual('password');
  });
  it('empty new password is invalid', () => {
    wrapper.instance().onNewPasswordChange('');
    expect(wrapper.state('hasValidNewPassword')).toBe(false);
  });

  it('state is updated when new password changes', () => {
    wrapper.instance().onNewPasswordChange('password');
    expect(wrapper.state('newPassword')).toEqual('password');
  });
  it('empty confirm password is invalid', () => {
    wrapper.instance().onConfirmNewPasswordChange('');
    expect(wrapper.state('hasValidConfirmNewPassword')).toBe(false);
  });
  it('confirm password non equal to new password is invalid', () => {
    wrapper.instance().onNewPasswordChange('password1');
    wrapper.instance().onConfirmNewPasswordChange('password2');
    expect(wrapper.state('hasValidConfirmNewPassword')).toBe(false);
  });

  it('state is updated when confirm password changes', () => {
    wrapper.instance().onConfirmNewPasswordChange('password');
    wrapper.instance().onNewPasswordChange('password');
    expect(wrapper.state('confirmNewPassword')).toEqual('password');
  });
  it('calls update password', () => {
    wrapper.instance().onOldPasswordChange('oldPassword');
    wrapper.instance().onNewPasswordChange('newPassword');
    wrapper.instance().onConfirmNewPasswordChange('newPassword');
    wrapper.instance().updatePassword();
    expect(updateSpy.withArgs('email@valid.de', 'oldPassword', 'newPassword', {pathname: '/'}).calledOnce).toBe(true);
  });
});