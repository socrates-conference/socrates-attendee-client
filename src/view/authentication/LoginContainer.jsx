// @flow

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import {connect} from 'react-redux';
import type {AuthenticationState} from './authenticationReducer';

import './authentication.css';
import {withRouter} from 'react-router';
import OneTimePasswordContainer from './oneTimePassword/OneTimePasswordContainer';
import LoginFormContainer from './loginForm/LoginFormContainer';


export type Props = {
  state: AuthenticationState;
}

export class LoginContainer extends Component<Props> {

  static propTypes = {
    state: PropTypes.any
  };

  render = () => {
    return (
      <div id="login-container">
        {
          this.props.state.isOneTimePassword
            ?
            <OneTimePasswordContainer/>
            :
            <LoginFormContainer/>
        }
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {state: {...state.authentication}};
};

export default withRouter(connect(mapStateToProps)(LoginContainer));
