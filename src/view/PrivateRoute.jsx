// @flow
import React from 'react';
import {Route} from 'react-router-dom';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {Redirect, withRouter} from 'react-router';

// eslint-disable-next-line no-shadow
const PrivateRoute = ({component: Component, children, state, location, ...rest}) => {
  const shouldShowComponent = (): boolean => {
    return state.token.trim().length > 0 && state.isAdministrator;
  };
  return (
    <Route {...rest} render={(props) => (
      shouldShowComponent()
        ? props.component ? <Component {...props} /> : children
        : <Redirect to={{
          pathname: '/login',
          state: {from: location.pathname}
        }}/>
    )}/>
  );
};

PrivateRoute.propTypes = {
  children: PropTypes.object,
  component: PropTypes.func,
  location: PropTypes.object.isRequired,
  state: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
  return {state: {...state.authentication}};
};

export default withRouter(connect(mapStateToProps, null, null, {pure: false})(PrivateRoute));
