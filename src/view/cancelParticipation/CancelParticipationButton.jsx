// @flow
import React, {Component} from 'react';
import {InputTextRow} from '../profile/InputTextRow';
import AreYouSureModal from '../common/modal/AreYouSureModal';
import {ModalResult} from '../common/modal/Modal';
import {withRouter} from 'react-router';
import {
  cancelParticipation,
  requestCancelParticipation,
  abortCancelParticipation,
  finishCancelParticipation
} from './cancelParticipationCommand';
import {connect} from 'react-redux';
import {CancelParticipationState} from './cancelParticipationState';
import type {CancelParticipationStateType} from './cancelParticipationState';
import InfoModal from '../common/modal/InfoModal';
import ErrorModal from '../common/modal/ErrorModal';

type Props = {
  participantId: number,
  cancelationState: CancelParticipationStateType,
  cancelParticipation: (participantId: number) => void,
  requestCancelParticipation: () => void,
  abortCancelParticipation: () => void,
  finishCancelParticipation: () => void
};

type State = {
  cancelationState: CancelParticipationStateType,
}

const INITIAL_STATE = {
  cancelationState: CancelParticipationState.UNDECIDED
};

export class CancelParticipationButton extends Component<Props, State> {
  state = INITIAL_STATE;

  static defaultProps = {
    cancelationState: CancelParticipationState.UNDECIDED,
    cancelParticipation: (participantId: number) => {},
    requestCancelParticipation: () => {},
    abortCancelParticipation: () => {},
    finishCancelParticipation: () => {}
  };

  _closeAskConfirmationModal = (editInfo: Object, modalResult: string) => {
    const shouldSubmit = modalResult === ModalResult.Success;
    if (shouldSubmit) {
      this.props.cancelParticipation(this.props.participantId);
    } else {
      this.props.abortCancelParticipation();
    }
  };

  _closeInfoModal = () => {
    this.props.finishCancelParticipation();
  };

  _closeErrorModal = () => {
    this.props.abortCancelParticipation();
  };

  render() {
    return (
      <InputTextRow>
        <div className="form-group col">{this.renderDependingOnState()}</div>
      </InputTextRow>)
      ;

  }

  renderDependingOnState() {
    switch (this.props.cancelationState) {
      case CancelParticipationState.UNDECIDED:
        return this.renderCancelButton();
      case CancelParticipationState.SUCCESS:
        return this.renderSuccessModal();
      case CancelParticipationState.ERROR:
        return this.renderErrorMessage();
      case CancelParticipationState.ASK_CONFIRMATION:
        return this.renderModalDialog();
      default:
        return null;
    }
  }

  renderModalDialog() {
    return <AreYouSureModal closeModal={this._closeAskConfirmationModal} editData={{}}
                            info={{
                              commandId: 'cancel_participation',
                              title: 'Do you want to cancel your participation?',
                              text: 'If you cancel your participation, your seat will be freed.'
                            }}
                            show={this.props.cancelationState === CancelParticipationState.ASK_CONFIRMATION}/>;
  }

  renderCancelButton() {
    return <div className="row">
      <div className="col-sm-12">
        <button className="btn btn-danger" onClick={this.props.requestCancelParticipation}>Cancel participation
        </button>
      </div>
    </div>;
  }

  renderSuccessModal() {
    return <InfoModal closeModal={this._closeInfoModal} info={{
      title: 'Participation Cancelled',
      text: 'Your participation has been cancelled successfully.'
    }} show={true}/>;
  }

  renderErrorMessage() {
    return <ErrorModal closeModal={this._closeErrorModal} info={{
      title: 'Cancel Participation Failed',
      text: 'Your participation could not be cancelled.',
      message: 'Please try again later or contact the organizers at info@socrates-conference.de .'
    }} show={true}/>;
  }
}

const mapStateToProps = (state) => {
  return {
    participantId: state.profile.participantId,
    cancelationState: state.cancelParticipation
  };
};

const mapDispatchToProps = {
  cancelParticipation,
  requestCancelParticipation,
  abortCancelParticipation,
  finishCancelParticipation
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CancelParticipationButton));
