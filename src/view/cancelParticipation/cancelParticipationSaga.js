// @flow

import {
  call,
  put
} from 'redux-saga/effects';
import * as api from '../requests/api';
import {
  cancelParticipationEnsued,
  cancelParticipationFailed,
  cancelParticipationRequested,
  cancelParticipationAborted
} from './cancelParticipationEvent';
import {logoutSuccess} from '../authentication/authenticationEvents';
import {removeAuthorizationHeader} from '../requests/clientConfig';
import {removeToken} from '../services/jwtStorage';

export function* cancelParticipationSaga(action: Object): Iterable<any> {
  const {participantId} = action;
  let event = cancelParticipationFailed();
  try {
    const result = yield call(api.cancelParticipation, participantId);
    if(result) {
      event = cancelParticipationEnsued();
    }
  } catch(ignored) {
  }
  yield put(event);
}

export function* requestCancelParticipationSaga(): Iterable<any> {
  yield put(cancelParticipationRequested());
}

export function* abortCancelParticipationSaga(): Iterable<any> {
  yield put(cancelParticipationAborted());
}

export function* finishCancelParticipationSage(): Iterable<any> {
  yield call(removeSession);
  yield put(logoutSuccess());
}

const removeSession = () => {
  removeAuthorizationHeader();
  removeToken();
};