// @flow

export const CancelParticipationState = {
  UNDECIDED: 'UNDECIDED',
  ASK_CONFIRMATION: 'ASK_CONFIRMATION',
  PENDING: 'PENDING',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR'
};

export type CancelParticipationStateType = $Keys<typeof CancelParticipationState>