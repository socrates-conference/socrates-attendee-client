//@flow

export default class CancelParticipationCommand {
  static get CANCEL_PARTICIPATION(): string {
    return 'CancelParticipationCommand/CANCEL_PARTICIPATION';
  }

  static get REQUEST_CANCEL_PARTICIPATION(): string {
    return 'CancelParticipationCommand/REQUEST_CANCEL_PARTICIPATION';
  }

  static get ABORT_CANCEL_PARTICIPATION(): string {
    return 'CancelParticipationCommand/ABORT_CANCEL_PARTICIPATION';
  }

  static get FINISH_CANCEL_PARTICIPATION(): string {
    return 'CancelParticipationCommand/FINISH_CANCEL_PARTICIPATION';
  }
}

export const cancelParticipation =
  (participantId: number) => ({type: CancelParticipationCommand.CANCEL_PARTICIPATION, participantId});


export const requestCancelParticipation =
  () => ({type: CancelParticipationCommand.REQUEST_CANCEL_PARTICIPATION});

export const abortCancelParticipation =
  () => ({type: CancelParticipationCommand.ABORT_CANCEL_PARTICIPATION});


export const finishCancelParticipation =
  () => ({type: CancelParticipationCommand.FINISH_CANCEL_PARTICIPATION});
