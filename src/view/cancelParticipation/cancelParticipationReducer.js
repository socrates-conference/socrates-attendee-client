// @flow

import {CancelParticipationEvent} from './cancelParticipationEvent';
import {CancelParticipationState} from './cancelParticipationState';
import type {CancelParticipationStateType} from './cancelParticipationState';

const INITIAL_STATE = CancelParticipationState.UNDECIDED;

export const cancelParticipationReducer = (state: CancelParticipationStateType = INITIAL_STATE, event: any) => {
  switch (event.type) {
    case CancelParticipationEvent.CANCEL_PARTICIPATION_FAILED:
      return CancelParticipationState.ERROR;
    case CancelParticipationEvent.CANCEL_PARTICIPATION_ENSUED:
      return CancelParticipationState.SUCCESS;
    case CancelParticipationEvent.CANCEL_PARTICIPATION_REQUESTED:
      return CancelParticipationState.ASK_CONFIRMATION;
    case CancelParticipationEvent.CANCEL_PARTICIPATION_ABORTED:
      return CancelParticipationState.UNDECIDED;
    default:
      return state;
  }
};
export default cancelParticipationReducer;
