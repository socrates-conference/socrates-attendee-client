// @flow
import SagaTester from 'redux-saga-tester';
import rootSaga from '../sagas/rootSaga';
import * as api from '../requests/api';
import {stub} from 'sinon';
import {cancelParticipationReducer} from './cancelParticipationReducer';
import {CancelParticipationState} from './cancelParticipationState';
import {CancelParticipationEvent} from './cancelParticipationEvent';
import {
  cancelParticipation,
  requestCancelParticipation,
  abortCancelParticipation,
  finishCancelParticipation
} from './cancelParticipationCommand';
import AuthenticationEvent from '../authentication/authenticationEvents';

const INITIAL_STATE = CancelParticipationState.UNDECIDED;

describe('CancelParticipationSaga', () => {
  let sagaTester: SagaTester;
  const cancelParticipationSpy = stub(api, 'cancelParticipation');
  beforeEach(() => {
    sagaTester = new SagaTester({INITIAL_STATE, reducers: cancelParticipationReducer});
    sagaTester.start(rootSaga);
  });

  afterEach(() => {
    sagaTester.reset();
    cancelParticipationSpy.resetHistory();
  });

  it('cancels participation if participantId is valid', async() => {
    cancelParticipationSpy.resolves(true);
    sagaTester.dispatch(cancelParticipation(4));
    await sagaTester.waitFor(CancelParticipationEvent.CANCEL_PARTICIPATION_ENSUED);
    expect(cancelParticipationSpy.withArgs(4).calledOnce).toBe(true);
  });

  it('fails if participantId is invalid', async() => {
    cancelParticipationSpy.resolves(false);
    sagaTester.dispatch(cancelParticipation(4));
    await sagaTester.waitFor(CancelParticipationEvent.CANCEL_PARTICIPATION_FAILED);
    expect(cancelParticipationSpy.withArgs(4).calledOnce).toBe(true);
  });

  it('fails if error occurs', async() => {
    cancelParticipationSpy.throws();
    sagaTester.dispatch(cancelParticipation(4));
    await sagaTester.waitFor(CancelParticipationEvent.CANCEL_PARTICIPATION_FAILED);
    expect(cancelParticipationSpy.withArgs(4).calledOnce).toBe(true);
  });

  it('creates requested event on cancelation request', async() => {
    sagaTester.dispatch(requestCancelParticipation());
    await sagaTester.waitFor(CancelParticipationEvent.CANCEL_PARTICIPATION_REQUESTED);
  });

  it('creates aborted if cancellation is aborted', async() => {
    sagaTester.dispatch(abortCancelParticipation());
    await sagaTester.waitFor(CancelParticipationEvent.CANCEL_PARTICIPATION_ABORTED);
  });

  it('logs out after cancel participation is finished', async() => {
    sagaTester.dispatch(finishCancelParticipation());
    await sagaTester.waitFor(AuthenticationEvent.LOGOUT_SUCCESS);
  });
});
