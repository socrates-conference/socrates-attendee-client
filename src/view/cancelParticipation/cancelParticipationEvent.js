// @flow

export class CancelParticipationEvent {
  static get CANCEL_PARTICIPATION_ENSUED() {
    return 'CancelParticipationEvent/CANCEL_PARTICIPATION_ENSUED';
  }

  static get CANCEL_PARTICIPATION_FAILED() {
    return 'CancelParticipationEvent/CANCEL_PARTICIPATION_FAILED';
  }
  static get CANCEL_PARTICIPATION_REQUESTED() {
    return 'CancelParticipationEvent/CANCEL_PARTICIPATION_REQUESTED';
  }
  static get CANCEL_PARTICIPATION_ABORTED() {
    return 'CancelParticipationEvent/CANCEL_PARTICIPATION_ABORTED';
  }
}

export const cancelParticipationFailed = () => ({type: CancelParticipationEvent.CANCEL_PARTICIPATION_FAILED});
export const cancelParticipationEnsued = () => ({type: CancelParticipationEvent.CANCEL_PARTICIPATION_ENSUED});
export const cancelParticipationRequested = () => ({type: CancelParticipationEvent.CANCEL_PARTICIPATION_REQUESTED});
export const cancelParticipationAborted = () => ({type: CancelParticipationEvent.CANCEL_PARTICIPATION_ABORTED});

