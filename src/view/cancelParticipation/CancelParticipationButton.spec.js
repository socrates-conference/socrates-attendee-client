import {mount} from 'enzyme';
import React from 'react';
import {CancelParticipationButton} from './CancelParticipationButton';
import {CancelParticipationState} from './cancelParticipationState';

describe('CancelParticipationButton', () => {
  describe('renders', () => {
    it('without crashing', () => {
      expect(
        mount(<CancelParticipationButton participantId={1} cancelationState={CancelParticipationState.UNDECIDED} />)
      ).toHaveLength(1);
    });

    it('correctly for state = UNDECIDED', () => {
      checkSnapshotForState(CancelParticipationState.UNDECIDED);
    });

    it('correctly for state = ASK_CONFIRMATION', () => {
      checkSnapshotForState(CancelParticipationState.ASK_CONFIRMATION);
    });

    it('correctly for state = PENDING', () => {
      checkSnapshotForState(CancelParticipationState.PENDING);
    });

    it('correctly for state = SUCCESS', () => {
      checkSnapshotForState(CancelParticipationState.SUCCESS);
    });

    it('correctly for state = ERROR', () => {
      checkSnapshotForState(CancelParticipationState.ERROR);
    });
  });
});

const checkSnapshotForState = (state) => {
  expect(mount(<CancelParticipationButton participantId={1} cancelationState={state} />)).toMatchSnapshot();
};

