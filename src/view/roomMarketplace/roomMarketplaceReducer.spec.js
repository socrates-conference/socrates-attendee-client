import {INITIAL_STATE, roomMarketplaceReducer} from './roomMarketplaceReducer';
import {potentialRoommatesRead} from './roomMarketplaceEvents';

const potentialRoommate = {
  id: 4711,
  email: 'test2@test.de',
  userName: 'Test2',
  gender: 'Some Gender',
  introduction: 'introduction',
  roomType: 'bedInDouble'
};

describe(' room sharing reducer', () => {
  it('updates changes to state', () => {
    const event = potentialRoommatesRead([potentialRoommate]);
    const result = roomMarketplaceReducer(INITIAL_STATE, event);
    expect(result.potentialRoommates).toEqual([potentialRoommate]);
  });
});
