// @flow

import React from 'react';
import Table from '../common/table/Table';
import type {Action, ColumnDefinition} from '../common/table/TableTypes';
import type {PotentialRoommate} from './roomMarketplaceReducer';

export type Props = {
  rowActions: Array<Action>,
  tableActions: Action[],
  rows: PotentialRoommate[],
}

const columns: Array<ColumnDefinition> = [
  {header: 'Name', field: 'userName', isSortable: true, isFilterable: true,
    visible: true},
  {header: 'Gender', field: 'gender', isSortable: true, isFilterable: true,
    visible: true},
  {header: 'Introduction', field: 'introduction', isSortable: true, isFilterable: true,
    visible: true}
];

export default function RoomMarketplaceListComponent(props: Props) {
  return (
    <div id="room-marketplace-table">
      <Table data={props.rows} columns={columns} rowActions={props.rowActions}/>
    </div>
  );
}
