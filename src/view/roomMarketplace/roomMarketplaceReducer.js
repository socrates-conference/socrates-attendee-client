// @flow

import {RoomMarketplaceEvent} from './roomMarketplaceEvents';

export type PotentialRoommate = {
  id: number,
  email: string,
  userName: string,
  gender: string,
  introduction: string,
  roomType: string
};

type State = {
  potentialRoommates: PotentialRoommate[],
  currentCommand: string,
  errorMessage: string,
  currentPotentialRoommate?: PotentialRoommate
};

export const INITIAL_STATE = {
  potentialRoommates: [],
  currentCommand: '',
  errorMessage: '',
  currentPotentialRoommate: undefined
};

export const roomMarketplaceReducer = (state: State = INITIAL_STATE, event: RoomMarketplaceEvent) => {
  switch(event.type) {
    case RoomMarketplaceEvent.POTENTIAL_ROOMMATES_READ:
      return {...state, potentialRoommates: event.payload};
    case RoomMarketplaceEvent.HANDLED_ACTION:
      return {...state, currentCommand: event.payload.commandId, currentPotentialRoommate: event.payload.data};
    case RoomMarketplaceEvent.ACTION_ABORTED:
      return {...state, currentCommand: '', currentPotentialRoommate: undefined};
    case RoomMarketplaceEvent.INVITATION_ADD_FAILED:
      return {...state, currentCommand: 'error', errorMessage: event.payload, currentPotentialRoommate: undefined};
    default:
      return state;
  }
};
