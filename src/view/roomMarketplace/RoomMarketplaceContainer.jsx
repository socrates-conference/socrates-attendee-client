// @flow

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import RoomMarketplaceModals from './RoomMarketplaceModals';
import type {Action} from '../common/table/TableTypes';
import type {PotentialRoommate} from './roomMarketplaceReducer';
import RoomMarketplaceListComponent from './RoomMarketplaceListComponent';
import {closeModal, sendInvitation, getPotentialRoommates, handleAction} from './roomMarketplaceCommand';

import './room-marketplace.css';
import {routeTo} from '../commands/routingCommand';

export type Props = {
  closeModal: () => void,
  sendInvitation: (Object, string) => void,
  handleAction: (string, Object) => void,
  getPotentialRoommates: (string, string) => void,
  potentialRoommates: PotentialRoommate[],
  currentCommand: string,
  currentPotentialRoommate?: PotentialRoommate,
  email: string,
  userName: string,
  errorMessage: string,
  routeTo: (Object | string) => void,
  hasPendingInvitations: boolean,
  hasRoommate: boolean,
  isInMarketplace: boolean,
  departure: string,
}

export class RoomMarketplaceContainer extends Component<Props> {

  static propTypes = {
    closeModal: PropTypes.func.isRequired,
    currentCommand: PropTypes.string.isRequired,
    currentPotentialRoommate: PropTypes.object,
    departure: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    errorMessage: PropTypes.string,
    getPotentialRoommates: PropTypes.func.isRequired,
    handleAction: PropTypes.func.isRequired,
    hasPendingInvitations:PropTypes.bool.isRequired,
    hasRoommate: PropTypes.bool.isRequired,
    isInMarketplace: PropTypes.bool.isRequired,
    potentialRoommates: PropTypes.array.isRequired,
    routeTo: PropTypes.func.isRequired,
    sendInvitation: PropTypes.func.isRequired,
    userName: PropTypes.string.isRequired
  };

  componentDidMount() {
    if (!this.props.isInMarketplace || this.props.hasRoommate || this.props.hasPendingInvitations ) {
      this.props.routeTo('/profile');
    }
  }


  _invite = (potentialRoommate: PotentialRoommate) => {
    this.props.handleAction('invite', potentialRoommate);
  };

  _sendInvitation = (potentialRoommate: PotentialRoommate, commandId: string) => {
    if (commandId === 'invite') {
      this.props.sendInvitation(potentialRoommate, this.props.email);
    }
  };

  _rowActions: Array<Action> = [
    {
      execute: (item) => this._invite(item),
      canExecute: () => true, buttonClass: 'btn-success',
      icon: 'arrow-alt-circle-right', text: 'invite the person in this row to be your room mate', visible: true
    }
  ];

  _tableActions: Array<Action> = [
    {
      execute: () => this.props.getPotentialRoommates(this.props.email, this.props.departure),
      canExecute: () => true, buttonClass: 'btn-info',
      icon: 'sync', text: 'Refresh', visible: true
    }
  ];

  componentWillMount = () => {
    this.props.getPotentialRoommates(this.props.email, this.props.departure);
  };

  render = () => {
    const getTextFromData = () => {
      return this.props.currentPotentialRoommate
        ? `${this.props.currentPotentialRoommate.userName} - ${this.props.currentPotentialRoommate.gender} - 
        ${this.props.currentPotentialRoommate.introduction}`
        : '';
    };

    return (
      <div id="room-marketplace" className="container">
        {
          this.props.currentCommand !== '' ?
            <RoomMarketplaceModals closeModal={this.props.closeModal} commandId={this.props.currentCommand}
              text={getTextFromData()} errorMessage={this.props.errorMessage}
              potentialRoommate={this.props.currentPotentialRoommate} executeAction={this._sendInvitation}
            /> : null
        }
        <h1>Room Marketplace</h1>
        <div>
          Hi {this.props.userName},
        </div>
        <div>
          Here you can find a roommate. If you find someone suitable, send an invitation.
        </div>
        <RoomMarketplaceListComponent
          rowActions={this._rowActions}
          tableActions={this._tableActions}
          rows={this.props.potentialRoommates}
        />
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {
    ...state.roomMarketplace,
    hasPendingInvitations: state.roomSharing.hasPendingInvitations,
    hasRoommate:  state.roomSharing.hasRoommate,
    isInMarketplace:  state.roomSharing.isInMarketplace,
    departure: state.profile.departure,
    email: state.authentication.email,
    userName: state.authentication.userName
  };
};

const mapDispatchToProps = {
  getPotentialRoommates, closeModal, handleAction, sendInvitation, routeTo
};

export default connect(mapStateToProps, mapDispatchToProps)(RoomMarketplaceContainer);
