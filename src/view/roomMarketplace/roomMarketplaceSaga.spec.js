/* eslint-disable max-nested-callbacks */
// @flow
import axios from 'axios';
import SagaTester from 'redux-saga-tester';
import rootSaga from '../sagas/rootSaga';
import {getPotentialRoommates, handleAction, sendInvitation} from './roomMarketplaceCommand';
import {RoomMarketplaceEvent} from './roomMarketplaceEvents';
import {INITIAL_STATE, roomMarketplaceReducer} from './roomMarketplaceReducer';
import {RoomSharingEvent} from '../roomSharing/roomSharingEvents';

jest.mock('axios');

const potentialRoommate = {
  id: 4711,
  email: 'test2@test.de',
  userName: 'Test2',
  gender: 'Some Gender',
  introduction: 'introduction',
  roomType: 'bedInDouble'
};

const participant = {
  participantId: 0,
  personId: 4711,
  nickname: 'Test',
  email: 'test@test.de',
  firstname: 'Test One',
  lastname: 'With Lastname',
  company: '',
  confirmed: true,
  address1: 'addr1',
  address2: 'addr2',
  province: 'prov',
  postal: 'postal',
  city: 'city',
  country: 'country',
  arrival: '0',
  departure: '5',
  dietary: false,
  dietaryInfo: '',
  family: false,
  familyInfo: '',
  gender: '',
  tshirt: 'None',
  labelname: 'Label name',
  price: undefined,
  social: 'twitter',
  pronoun: 'pronoun',
  sharedRoom: true
};

const potential = {
  participantId: 0,
  personId: 815,
  nickname: 'Test2',
  email: 'test2@test.de',
  firstname: 'Test Two',
  lastname: 'With Lastname',
  company: '',
  confirmed: true,
  address1: 'addr1',
  address2: 'addr2',
  province: 'prov',
  postal: 'postal',
  city: 'city',
  country: 'country',
  arrival: '0',
  departure: '5',
  dietary: false,
  dietaryInfo: '',
  family: false,
  familyInfo: '',
  gender: '',
  tshirt: 'None',
  labelname: 'Label name',
  price: undefined,
  social: 'twitter',
  pronoun: 'pronoun',
  sharedRoom: true
};

const invitation = {
  id: 16,
  invitee1: {id: 408, email: 'test@test.de'},
  invitee2: {id: 4711, email: 'test2@test.de'},
  inviter: {id: 408, email: 'test@test.de'},
  status: 'OPEN'
};

describe('RoomMarketplaceSaga', () => {
  let sagaTester: SagaTester;
  beforeEach(() => {
    sagaTester = new SagaTester({INITIAL_STATE, reducers: roomMarketplaceReducer});
    sagaTester.start(rootSaga);
  });

  afterEach(() => {
    sagaTester.reset();
  });

  describe('request potential roommates', () => {
    let promise;
    beforeEach(() => {
      axios.post.mockImplementation(() => promise);
    });

    afterEach(() => {
      axios.post.mockReset();
    });

    it('dispatches POTENTIAL_ROOMMATES_READ', async () => {
      promise = Promise.resolve({data: [{...potentialRoommate}]});
      sagaTester.dispatch(getPotentialRoommates('test@test.de', 'bedInDouble'));
      await sagaTester.waitFor(RoomMarketplaceEvent.POTENTIAL_ROOMMATES_READ);
    });
  });
  describe('handle marketplace action', () => {
    describe('invite', () => {
      it('dispatches HANDLED_ACTION', async () => {
        sagaTester.dispatch(handleAction('invite', potentialRoommate));
        await sagaTester.waitFor(RoomMarketplaceEvent.HANDLED_ACTION);
      });
    });
  });
  describe('send', () => {
    describe('invitation', () => {
      let findParticipantPromise, addInvitationPromise, findPotentialPromise;
      beforeEach(() => {
        axios.get.mockImplementationOnce(() => findParticipantPromise);
        axios.get.mockImplementationOnce(() => findPotentialPromise);
        axios.post.mockImplementation(() => addInvitationPromise);
      });

      afterEach(() => {
        axios.post.mockReset();
        axios.get.mockReset();
      });

      it('when invitation can be accepted dispatches INVITATION_ADDED', async () => {
        addInvitationPromise = Promise.resolve({data: {...invitation}});
        findParticipantPromise = Promise.resolve({data: [{...participant, id: 4711}]});
        findPotentialPromise = Promise.resolve({data: [{...potential, id: 815}]});
        sagaTester.dispatch(sendInvitation(potentialRoommate, 'test@test.de'));
        await sagaTester.waitFor(RoomSharingEvent.INVITATION_ADDED);
      });
      it('when participant cannot be found dispatches INVITATION_ADD_FAILED', async () => {
        findParticipantPromise = Promise.resolve({data: null});
        findPotentialPromise = Promise.resolve({data: [{...potential, id: 815}]});
        sagaTester.dispatch(sendInvitation(potentialRoommate, 'test@test.de'));
        await sagaTester.waitFor(RoomMarketplaceEvent.INVITATION_ADD_FAILED);
      });
      it('when cannot be added dispatches INVITATION_ADD_FAILED', async () => {
        addInvitationPromise = Promise.reject({message: 'some error'});
        findParticipantPromise = Promise.resolve({data: [{...participant, id: 4711}]});
        findPotentialPromise = Promise.resolve({data: [{...potential, id: 815}]});
        sagaTester.dispatch(sendInvitation(potentialRoommate, 'test@test.de'));
        await sagaTester.waitFor(RoomMarketplaceEvent.INVITATION_ADD_FAILED);
      });
      it('when potential roommate cannot be found dispatches INVITATION_ADD_FAILED', async () => {
        findParticipantPromise = Promise.resolve({data: [{...participant, id: 4711}]});
        findPotentialPromise = Promise.resolve({data: null});
        sagaTester.dispatch(sendInvitation(potentialRoommate, 'test@test.de'));
        await sagaTester.waitFor(RoomMarketplaceEvent.INVITATION_ADD_FAILED);
      });
    });
  });
});
