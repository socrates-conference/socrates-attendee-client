// @flow
import {
  shallow
} from 'enzyme';
import React from 'react';
import expect from '../../test/expect';
import {RoomMarketplaceContainer} from './RoomMarketplaceContainer';

const props = {
  closeModal: () => {},
  sendInvitation: () => {},
  handleAction: () => {},
  getPotentialRoommates: () => {},
  potentialRoommates: [],
  currentCommand: '',
  currentPotentialRoommate: undefined,
  email: 'test@test.de',
  userName: 'Test',
  errorMessage: '',
  routeTo: () => {},
  hasPendingInvitations: false,
  hasRoommate: false,
  isInMarketplace: true,
  departure: '5'
};

describe('Profile:', () => {
  describe('should render', () => {
    it('with default data', () => {
      const wrapper = shallow(<RoomMarketplaceContainer {...props}/>);
      expect(wrapper).toMatchSnapshot();
    });
    it('routes to profile if participant is not in marketplace', () => {
      let routedTo = '';
      const newProps = {...props, isInMarketplace: false, routeTo:(to: Object | string) => {
        routedTo = to;
      }};
      const wrapper = shallow(<RoomMarketplaceContainer {...newProps}/>);
      expect(wrapper).toBeDefined();
      expect(routedTo).toEqual('/profile');
    });
    it('showing error', () => {
      const newProps = {...props, currentCommand: 'error', errorMessage: 'Message'};
      const wrapper = shallow(<RoomMarketplaceContainer {...newProps} />);
      expect(wrapper).toMatchSnapshot();
    });
  });
});
