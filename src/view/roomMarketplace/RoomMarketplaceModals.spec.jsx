// @flow
import {
  shallow
} from 'enzyme';
import React from 'react';
import expect from '../../test/expect';
import RoomMarketplaceModals from './RoomMarketplaceModals';
import {ModalResult} from '../common/modal/Modal';

const props = {
  closeModal: () => {},
  executeAction: () => {},
  commandId: '',
  text: '',
  errorMessage: '',
  potentialRoommate: undefined
};

describe('Profile:', () => {
  describe('should render', () => {
    it('with default data', () => {
      const wrapper = shallow(<RoomMarketplaceModals {...props}/>);
      expect(wrapper).toMatchSnapshot();
    });
    it('and can cancel ', (done) => {
      const newProps = {...props, closeModal: () => done()};
      const wrapper = shallow(<RoomMarketplaceModals {...newProps}/>);
      wrapper.instance()._closeConfirmation({}, ModalResult.Cancel, 'command');
    });
    it('and can execute action', (done) => {
      const newProps = {...props, executeAction: () => done()};
      const wrapper = shallow(<RoomMarketplaceModals {...newProps}/>);
      wrapper.instance()._closeConfirmation({}, ModalResult.Success, 'command');
    });
  });
});
