// @flow
import * as api from '../requests/api';
import {call, put} from 'redux-saga/effects';
import {actionAborted, handledAction, invitationAddFailed, potentialRoommatesRead} from './roomMarketplaceEvents';
import type {InvitationRequest} from '../roomSharing/roomSharingReducer';
import {invitationAdded} from '../roomSharing/roomSharingEvents';
import {routeTo} from '../commands/routingCommand';

export function* getPotentialRoommatesSaga(command: any): Iterable<any> {
  const potentialRoommates = yield call(api.findPotentialRoommates, command.email, command.departure);
  yield put(potentialRoommatesRead(potentialRoommates ? potentialRoommates : []));
}

export function* handleMarketplaceActionSaga(command: any): Iterable<any> {
  yield put(handledAction(command.commandId, command.roommate));
}

export function* sendInvitationSaga(command: any): Iterable<any> {
  try {
    const participant = yield call(api.findParticipant, command.email);
    const potential = yield call(api.findParticipant, command.data.email);
    if(participant && potential) {
      const invitationRequest: InvitationRequest = {
        inviterId: participant.personId,
        inviterEmail: participant.email,
        invitee1Id: participant.personId,
        invitee1Email: participant.email,
        invitee2Id: potential.personId,
        invitee2Email: potential.email
      };
      const invitation = yield call(api.addInvitation, invitationRequest);
      if (invitation) {
        yield put(invitationAdded(invitation));
        yield put(routeTo('/profile'));
      } else {
        yield put(invitationAddFailed('Cannot add invitation'));
      }
    } else {
      yield put(invitationAddFailed('Cannot find person'));
    }
  } catch (e) {
    console.error('Cannot add invitation: ', e);
    yield put(invitationAddFailed(e.message));
  }
}

export function* handleMarketplaceCancelActionSaga(): Iterable<any> {
  yield put(actionAborted());

}
