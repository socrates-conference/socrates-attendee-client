// @flow

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import AreYouSureModal from '../common/modal/AreYouSureModal';
import ErrorModal from '../common/modal/ErrorModal';
import type {PotentialRoommate} from './roomMarketplaceReducer';
import {ModalResult} from '../common/modal/Modal';

export type Props = {
  closeModal: () => void,
  executeAction: (Object, string) => void,
  commandId: string,
  text: string,
  errorMessage: string,
  potentialRoommate?: PotentialRoommate
}

export default class RoomMarketplaceModals extends Component<Props> {

  static propTypes = {
    closeModal: PropTypes.func.isRequired,
    commandId: PropTypes.string.isRequired,
    errorMessage:PropTypes.string.isRequired,
    executeAction: PropTypes.func.isRequired,
    potentialRoommate: PropTypes.object,
    text: PropTypes.string.isRequired
  };

  _closeConfirmation = (data: Object, modalResult: string, commandId: string) => {
    const shouldSubmit = modalResult === ModalResult.Success;
    if(shouldSubmit) {
      this.props.executeAction(data, commandId);
    } else {
      this.props.closeModal();
    }
  };

  render = () => {
    const {text, commandId, closeModal, errorMessage, potentialRoommate} = this.props;

    return commandId !== '' ?
      <div>
        <AreYouSureModal info={{commandId, title: commandId, text}}
          id="areYouSureModal" show={commandId === 'invite'}
          closeModal={this._closeConfirmation} editData={potentialRoommate || {}}
        />
        <ErrorModal info={{title: 'Error', text: 'Operation failed', message: errorMessage}}
          show={commandId === 'error'} closeModal={closeModal}
        />
      </div> : null;
  };
}
