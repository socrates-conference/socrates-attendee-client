// @flow
import {
  shallow
} from 'enzyme';
import React from 'react';
import expect from '../../test/expect';
import RoomMarketplaceListComponent from './RoomMarketplaceListComponent';

const inviteAction = {
  execute: () => {},
  canExecute: () => true, buttonClass: 'btn-success',
  icon: 'arrow-alt-circle-right', text: 'invite the person in this row to be your room mate', visible: true

};
const refreshAction = {
  execute: () => {},
  canExecute: () => true, buttonClass: 'btn-success',
  icon: 'refresh', text: 'refresh', visible: true

};
const props = {
  rowActions: [inviteAction],
  tableActions: [refreshAction],
  rows: []
};

describe('Profile:', () => {
  describe('should render', () => {
    it('with default data', () => {
      const wrapper = shallow(<RoomMarketplaceListComponent {...props}/>);
      expect(wrapper).toMatchSnapshot();
    });
  });
});
