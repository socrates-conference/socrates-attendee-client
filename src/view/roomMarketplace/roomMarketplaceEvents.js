// @flow

import type {PotentialRoommate} from './roomMarketplaceReducer';

export class RoomMarketplaceEvent {
  static get POTENTIAL_ROOMMATES_READ() {return 'RoomMarketplaceEvent/POTENTIAL_ROOMMATES_READ';}
  static get HANDLED_ACTION(): string {return 'ApplicationListEvent/HANDLED_ACTION';}
  static get ACTION_ABORTED(): string {return 'ApplicationListEvent/ACTION_ABORTED';}
  static get INVITATION_ADD_FAILED() {return 'RoomMarketplaceEvent/INVITATION_ADD_FAILED'; }
  type: string;
  payload: any;
}

export const invitationAddFailed = (message: string) => ({
  type: RoomMarketplaceEvent.INVITATION_ADD_FAILED,
  payload: message
});

export const potentialRoommatesRead = (potentialRoommates: PotentialRoommate[]) => ({
  type: RoomMarketplaceEvent.POTENTIAL_ROOMMATES_READ,
  payload: potentialRoommates
});

export const handledAction =
  (commandId: string, data: PotentialRoommate) =>
    ({type: RoomMarketplaceEvent.HANDLED_ACTION,
      payload: {commandId, data}
    });
export const actionAborted = () =>({type: RoomMarketplaceEvent.ACTION_ABORTED});
