// @flow

import type {PotentialRoommate} from './roomMarketplaceReducer';

export default class RoomMarketplaceCommand {
  static get GET_POTENTIAL_ROOMMATES() { return 'RoomMarketplaceCommand/GET_POTENTIAL_ROOMMATES'; }
  static get CLOSE_MODAL(): string { return 'RoomMarketplaceCommand/CLOSE_MODAL'; }
  static get HANDLE_ACTION(): string { return 'RoomMarketplaceCommand/HANDLE_ACTION'; }
  static get SEND_INVITATION(): string { return 'RoomMarketplaceCommand/SEND_INVITATION'; }

}

export const getPotentialRoommates =
  (email: string, departure: string) => ({type: RoomMarketplaceCommand.GET_POTENTIAL_ROOMMATES, email, departure});
export const closeModal = (potentialRoommate: PotentialRoommate, result: string, commandId: string) =>
  ({type: RoomMarketplaceCommand.CLOSE_MODAL, potentialRoommate, result, commandId});

export const handleAction = (commandId: string, roommate: PotentialRoommate) =>
  ({type: RoomMarketplaceCommand.HANDLE_ACTION, commandId, roommate});

export const sendInvitation = (data: PotentialRoommate, email: string) =>
  ({type: RoomMarketplaceCommand.SEND_INVITATION, data, email});
