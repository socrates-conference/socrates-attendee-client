// @flow
export default class RoutingCommand {
  static get ROUTE_TO() { return 'RoutingCommand/ROUTE_TO'; }
}

export const routeTo = (location: Object | string) => ({type: RoutingCommand.ROUTE_TO, location});
