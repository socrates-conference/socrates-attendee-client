// @flow
import {
  call,
  put
} from 'redux-saga/effects';
import * as api from '../requests/api';
import {submitError} from '../profile/profileCommand';
import type {Invitable, Invitation} from './roomSharingReducer';
import {
  invitableAdded, invitableAddFailed,
  invitationAdded, invitationAddFailed,
  invitationAlreadyAccepted,
  invitationAlreadyDeclined, invitationHandled, roomSharingChanged
} from './roomSharingEvents';
import RoomSharingCommand from './roomSharingCommand';

export function* requestKnownRoommateSaga(command: any): Iterable<any> {
  let invitation: ?Invitation;
  try {
    invitation = yield call(api.addInvitation, command.invitation);
  } catch (e) {
    console.error('cannot add invitation: ', e);
  }
  if (invitation) {
    yield put(invitationAdded(invitation));
  } else {
    yield put(invitationAddFailed());
  }
}

export function* requestAutomaticRoommateSaga(command: any): Iterable<any> {
  let invitable: ?Invitable;
  try {
    invitable = yield call(api.addInvitable, command.invitable);
  } catch (e) {
    console.error('cannot add invitable: ', e);
  }
  if (invitable) {
    yield put(invitableAdded());
  } else {
    yield put(invitableAddFailed());
  }
}

export function* handleInvitationSaga(command: any): Iterable<any> {
  let invitation: ?Invitation;
  try {
    invitation = yield call(api.readInvitationByKey, command.key);
  } catch (e) {
    console.error('Cannot get invitation: ', e);
  }
  if (invitation) {
    if (invitation.status === 'DECLINED') {
      yield put(invitationAlreadyDeclined());
    }
    if (invitation.status === 'ACCEPTED') {
      yield put(invitationAlreadyAccepted());
    }
    if (invitation.status === 'PARTIALLY_ACCEPTED:1'
      || invitation.status === 'PARTIALLY_ACCEPTED:2'
      || invitation.status === 'OPEN') {
      invitation = yield call(api.handleInvitation, command.action, command.key, command.invitee);
      if (invitation) {
        yield put(invitationHandled(command.action));
      } else {
        yield put(submitError(RoomSharingCommand.HANDLE_INVITATION));
      }
    }
  } else {
    yield put(submitError(RoomSharingCommand.HANDLE_INVITATION));
  }
}

export function* updateRoomSharingSaga(command: any): Iterable<any> {
  yield put(roomSharingChanged(command.fieldName, command.value));
}

