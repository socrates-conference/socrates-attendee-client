// @flow

import type {InvitableRequest, InvitationRequest} from './roomSharingReducer';

export default class RoomSharingCommand {
  static get UPDATE_ROOM_SHARING() { return 'Command/UPDATE_ROOM_SHARING'; }

  static get HANDLE_INVITATION() { return 'Command/HANDLE_INVITATION'; }

  static get REQUEST_AUTOMATIC_ROOMMATE() { return 'Command/REQUEST_AUTOMATIC_ROOMMATE'; }

  static get FIND_ROOM_SHARING_INFORMATION() { return 'Command/FIND_ROOM_SHARING_INFORMATION'; }

  static get REQUEST_KNOWN_ROOMMATE() { return 'Command/REQUEST_KNOWN_ROOMMATE'; }
}

export const requestKnownRoommate =
  (invitation: InvitationRequest) => ({type: RoomSharingCommand.REQUEST_KNOWN_ROOMMATE, invitation});

export const requestAutomaticRoommate = (invitable: InvitableRequest) =>
  ({type: RoomSharingCommand.REQUEST_AUTOMATIC_ROOMMATE, invitable});

export const submitError = (reason: string) => ({type: 'Error/PARTICIPANT_SUBMIT', reason});

export const handleInvitationResponse = (action: string, key: string, invitee: string) =>
  ({type: RoomSharingCommand.HANDLE_INVITATION, action, key, invitee});

export const updateRoomSharing =
  (fieldName: string, value: any) => ({type: RoomSharingCommand.UPDATE_ROOM_SHARING, fieldName, value});

export const findRoomSharingInformation =
  (personId: number) => ({type: RoomSharingCommand.FIND_ROOM_SHARING_INFORMATION, personId});
