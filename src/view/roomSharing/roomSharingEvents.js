// @flow

import type {Invitation} from './roomSharingReducer';

export class RoomSharingEvent {
  static get INVITATION_ADDED() {return 'RoomSharingEvent/INVITATION_ADDED'; }
  static get INVITATION_ALREADY_DECLINED() {return 'RoomSharingEvent/INVITATION_ALREADY_DECLINED'; }
  static get INVITATION_ALREADY_ACCEPTED() {return 'RoomSharingEvent/INVITATION_ALREADY_ACCEPTED'; }
  static get INVITATION_HANDLED() {return 'RoomSharingEvent/INVITATION_HANDLED'; }
  static get ROOM_SHARING_CHANGED() {return 'RoomSharingEvent/ROOM_SHARING_CHANGED'; }
  static get SENT_INVITATION_FOUND() {return 'RoomSharingEvent/SENT_INVITATION_FOUND'; }
  static get RECEIVED_INVITATIONS_FOUND() {return 'RoomSharingEvent/RECEIVED_INVITATIONS_FOUND'; }
  static get INVITATION_ADD_FAILED() {return 'RoomSharingEvent/INVITATION_ADD_FAILED'; }
  static get INVITABLE_ADDED() {return 'RoomSharingEvent/INVITABLE_ADDED'; }
  static get INVITABLE_ADD_FAILED() {return 'RoomSharingEvent/INVITABLE_ADD_FAILED'; }
  static get IS_IN_MARKETPLACE_CHANGED() {return 'RoomSharingEvent/IS_IN_MARKETPLACE_CHANGED'; }

  type: string;
  payload: any;
}

export const invitationAddFailed = () => ({
  type: RoomSharingEvent.INVITATION_ADD_FAILED,
  payload: 'The invitation could not be sent. There can be several reasons for this. The more common ones are ' +
    'that the other participant does not have the same room type as you or has a single room.'
});
export const invitationAdded = (invitation: Invitation) => ({
  type: RoomSharingEvent.INVITATION_ADDED,
  payload: invitation
});
export const invitableAddFailed = () => ({
  type: RoomSharingEvent.INVITABLE_ADD_FAILED,
  payload: 'There was an error adding you to the room market. Please try again later.'
});
export const invitableAdded = () => ({
  type: RoomSharingEvent.INVITABLE_ADDED,
  payload: ''
});
export const invitationAlreadyDeclined = () => ({type: RoomSharingEvent.INVITATION_ALREADY_DECLINED});
export const invitationAlreadyAccepted = () => ({type: RoomSharingEvent.INVITATION_ALREADY_ACCEPTED});
export const invitationHandled = (action: string) => ({
  type: RoomSharingEvent.INVITATION_HANDLED,
  payload: action
});
export const roomSharingChanged = (fieldName: string, value: any) => ({
  type: RoomSharingEvent.ROOM_SHARING_CHANGED,
  payload: {fieldName, value}
});
export const sentInvitationFound = (invitation?: Invitation) => ({
  type: RoomSharingEvent.SENT_INVITATION_FOUND,
  payload: invitation
});
export const receivedInvitationsFound = (invitations?: Invitation[]) => ({
  type: RoomSharingEvent.RECEIVED_INVITATIONS_FOUND,
  payload: invitations || []
});
export const isInMarketplaceChanged = (value: boolean) => ({
  type: RoomSharingEvent.IS_IN_MARKETPLACE_CHANGED,
  payload: value
});

