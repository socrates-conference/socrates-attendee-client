//@flow
import React, {Component} from 'react';
import {FormContainer} from '../profile/FormContainer';
import {InputRadio} from '../profile/InputRadio';
import {HelpText} from '../profile/HelpText';
import {InputText} from '../profile/InputText';
import {connect} from 'react-redux';
import isValidEmailFormat from '../../utils/isValidEmailFormat';
import {requestAutomaticRoommate, requestKnownRoommate, updateRoomSharing} from './roomSharingCommand';
import type {InvitableRequest, Invitation, InvitationRequest} from './roomSharingReducer';
import {InputTextArea} from '../profile/InputTextArea';
import {withRouter} from 'react-router';
import {Link} from 'react-router-dom';

type Props = {
  userName: string,
  gender: string,
  introduction: string,
  sharedRoom: boolean,
  validated: any,
  sharedRoom: boolean,
  roommateChoice: string,
  roommateEmail: string,
  requestAutomaticRoommate: (invitable: InvitableRequest) => void,
  requestKnownRoommate: (invitation: InvitationRequest) => void,
  updateRoomSharing: (fieldName: string, value: any) => void,
  personId: number,
  email: string,
  findRoomSharingInformation: (personId: number) => void,
  hasPendingInvitations: boolean,
  hasRoommate: boolean,
  isInMarketplace: boolean,
  sentInvitation?: Invitation,
  receivedInvitations: Invitation[],
  selectedRoommate: string,
  errorAddingInvitation: string
}


export class Roommate extends Component<Props> {

  _onRequest = () => {
    if (this.props.roommateChoice === 'manual') {
      this.props.requestKnownRoommate(this._toInvitation());
    }
    if (this.props.roommateChoice === 'automatic') {
      this.props.requestAutomaticRoommate(this._toInvitable());
    }
  };

  _onChange = (event: SyntheticEvent<HTMLInputElement>) => {
    this.props.updateRoomSharing(event.currentTarget.name, event.currentTarget.value);
  };

  _toInvitation(): InvitationRequest {
    return {
      inviterId: this.props.personId,
      inviterEmail: this.props.email,
      invitee1Id: this.props.personId,
      invitee1Email: this.props.email,
      invitee2Email: this.props.roommateEmail
    };
  }
  _toInvitable(): InvitableRequest {
    return {
      email: this.props.email,
      userName: this.props.userName,
      gender: this.props.gender,
      introduction: this.props.introduction
    };
  }

  render = () => {
    const email = this.props.sentInvitation ? this.props.sentInvitation.invitee2.email : '';
    const showError = this.props.errorAddingInvitation &&
      <div>
        <p><b>{this.props.errorAddingInvitation}</b></p>
      </div>;
    const showDefaultView = !this.props.hasRoommate && !this.props.hasPendingInvitations
      && !this.props.isInMarketplace &&
      <div className="form-row">
        <div className="form-group col">
          <div className="row">
            <InputRadio
              name="roommateChoice"
              value="manual"
              checked={this.props.roommateChoice === 'manual'}
              id="manualRoommate"
              onChange={this._onChange}
              labelText="I already know whom I want to share my room with."/>
            <InputRadio
              name="roommateChoice"
              value="automatic"
              checked={this.props.roommateChoice === 'automatic'}
              id="automaticRoommate"
              onChange={this._onChange}
              labelText="I don't have a roommate, please help me find one."/>
          </div>
          <div className={'row  ' + (this.props.roommateChoice === 'manual' ? 'fadeIn' : 'fadeOut')}>
            <InputText
              className="col-lg-9 col-sm-12"
              name="roommateEmail" value={this.props.roommateEmail}
              placeholderText="Your chosen roommate's email address"
              labelText="Enter your chosen roommate's email address:"
              onChange={this._onChange}
              required
            />
            <div className="col-lg-3 col-sm-12 requestButton">
              <div className="col-form-label">&nbsp;</div>
              <button
                className="btn btn-primary"
                disabled={this.props.roommateEmail === '' || !isValidEmailFormat(this.props.roommateEmail)}
                onClick={this._onRequest}>Request as roommate
              </button>
            </div>
          </div>
          <HelpText name="roommateEmailHelp" fadeExpression={() => this.props.roommateChoice === 'manual'}>
            When you pick a roommate, we will notify them of your request to share the room with them. They then
            need to confirm the request to make it official. Please do not take rejections personally:
            There can be many reasons why people may prefer to share with someone else - not the least of which is
            if they&apos;d already chosen at the time of your request.
          </HelpText>
          {showError}
          <div className={'row  ' + (this.props.roommateChoice === 'automatic' ? 'fadeIn' : 'fadeOut')}>
            <InputText
              className="col-12"
              name="gender" value={this.props.gender}
              placeholderText="e.g. female, non-binary, gender-fluid, ..."
              labelText="Please tell us your gender:"
              onChange={this._onChange}
              required
            />
            <InputTextArea
              className="col-12"
              name="introduction" value={this.props.introduction}
              labelText="Introduce yourself:"
              placeholderText="Here you can give information about yourself, which in your opinion will help other participants to choose you as a roommate."
              onChange={this._onChange} rows={4}/>
            <div className="col-12 requestButton">
              <div className="col-form-label">&nbsp;</div>
              <button className="btn btn-primary" onClick={this._onRequest}>Request roommate</button>
            </div>
          </div>
          <HelpText name="genderHelp" fadeExpression={() => this.props.roommateChoice === 'automatic'}>
            For some, sharing a room with people of particular genders can be
            uncomfortable. In order to make it easier to pick a good match, we kindly ask you to disclose
            your own gender to potential roommates. We will not use this information in other contexts than
            picking a roommate, nor will it be visible to anyone who is not sharing a room, or who has already
            found a match. Please be aware that when participating in the Marketplace, your e-mail address may
            be made visible to other participants in order to handle sharing requests and responses.
          </HelpText>
        </div>
      </div>;
    const showSentInvitation = this.props.sentInvitation &&
      <p>You have requested <b>{email}</b> as your roommate. Please wait for confirmation.</p>;
    const receivedInvitations = this.props.receivedInvitations
      && this.props.receivedInvitations.length > 0
      && this.props.receivedInvitations.map(
        (item, index) => {
          return <p key={index}>Inviter: <b>{item.invitee1.email}</b></p>;
        });
    const showReceivedInvitations = this.props.receivedInvitations && this.props.receivedInvitations.length > 0 &&
      <div>
        <p>Some people want to share room with you and you received and email to confirm or decline.</p>
        {receivedInvitations}
      </div>;
    const showRoommate = this.props.hasRoommate &&
      <p>Your roommate is: <b>{this.props.selectedRoommate}</b></p>;
    const showInvitationsView = !this.props.hasRoommate && this.props.hasPendingInvitations &&
      <div className="form-row">
        <div className="form-group col">
          <div className="row">
            <div className="col">
              {showSentInvitation}
              {showReceivedInvitations}
            </div>
          </div>
        </div>
      </div>;
    const showIsInMarketplace = !this.props.hasRoommate && !this.props.hasPendingInvitations &&
      this.props.isInMarketplace &&
      <div>
        <p>You have chosen to join the room marketplace.</p>
        <p>Please wait for a request, or check out the <Link to="/room-marketplace" title="Room Marketplace">
          room marketplace
        </Link>.</p>
      </div>;
    return this.props.sharedRoom &&
      (<FormContainer name="roommate" className="withMargin" headline="Room sharing:">
        {showDefaultView}
        {showInvitationsView}
        {showRoommate}
        {showIsInMarketplace}
      </FormContainer>);
  };
}

const mapStateToProps = state => ({
  ...state.roomSharing,
  personId: state.profile.personId,
  email: state.authentication.email,
  userName: state.authentication.userName
});

const mapDispatchToProps = {
  requestKnownRoommate,
  requestAutomaticRoommate,
  updateRoomSharing
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Roommate));
