// @flow
import axios from 'axios';
import SagaTester from 'redux-saga-tester';
import rootSaga from '../sagas/rootSaga';
import roomSharingReducer, {INITIAL_STATE} from './roomSharingReducer';
import {
  handleInvitationResponse,
  requestAutomaticRoommate,
  requestKnownRoommate,
  updateRoomSharing
} from './roomSharingCommand';
import {RoomSharingEvent} from './roomSharingEvents';

jest.mock('axios');

const invitation = {
  id: 16,
  invitee1: {id: 408, email: 'contact@soler-sanandres.net'},
  invitee2: {id: 7, email: 'alexandre@soler-sanandres.net'},
  inviter: {id: 408, email: 'contact@soler-sanandres.net'},
  status: 'OPEN'
};
const invitationRequest = {
  inviterId: 408,
  inviterEmail: 'contact@soler-sanandres.net',
  invitee1Id: 408,
  invitee1Email: 'contact@soler-sanandres.net',
  invitee2Email: 'alexandre@soler-sanandres.net'
};
const invitable = {
  id: 4711,
  email: 'test2@test.de',
  userName: 'Test2',
  gender: 'Some Gender',
  introduction: 'introduction',
  roomType: 'bedInDouble'
};
describe('ProfileSaga', () => {
  let sagaTester: SagaTester;
  let promise;
  beforeEach(() => {
    sagaTester = new SagaTester({INITIAL_STATE, reducers: roomSharingReducer});
    sagaTester.start(rootSaga);
  });

  afterEach(() => {
    sagaTester.reset();
  });

  it('updates data on value change', async () => {
    sagaTester.dispatch(updateRoomSharing('roommateEmail', 'test@test.de'));
    await sagaTester.waitFor(RoomSharingEvent.ROOM_SHARING_CHANGED);
  });
  describe('request known roommate', () => {
    beforeEach(() => {
      axios.post.mockImplementation(() => promise);
    });

    afterEach(() => {
      axios.post.mockReset();
    });

    it('dispatches INVITATION_ADDED', async () => {
      promise = Promise.resolve({data: {...invitation, status: 'PARTIALLY_ACCEPTED:1'}});
      sagaTester.dispatch(requestKnownRoommate(invitationRequest));
      await sagaTester.waitFor(RoomSharingEvent.INVITATION_ADDED);
    });
    it('dispatches INVITATION_ADD_FAILED', async () => {
      promise = Promise.reject({message: 'error'});
      sagaTester.dispatch(requestKnownRoommate(invitationRequest));
      await sagaTester.waitFor(RoomSharingEvent.INVITATION_ADD_FAILED);
    });
  });
  describe('request automatic roommate ( take part on marketplace)', () => {
    beforeEach(() => {
      axios.post.mockImplementation(() => promise);
    });

    afterEach(() => {
      axios.post.mockReset();
    });

    it('dispatches INVITABLE_ADDED, if can add invitable', async () => {
      promise = Promise.resolve({data: true});
      sagaTester.dispatch(requestAutomaticRoommate(invitable));
      await sagaTester.waitFor(RoomSharingEvent.INVITABLE_ADDED);
    });
    it('dispatches INVITABLE_ADD_FAILED, if cannot add invitable', async () => {
      promise = Promise.reject({message: 'error'});
      sagaTester.dispatch(requestAutomaticRoommate(invitable));
      await sagaTester.waitFor(RoomSharingEvent.INVITABLE_ADD_FAILED);
    });
  });
  describe('invitation response handler on already accepted invitation', () => {
    beforeEach(() => {
      axios.get.mockImplementationOnce(() => promise);
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('dispatches INVITATION_ADDED', async () => {
      promise = Promise.resolve({data: {...invitation, status: 'ACCEPTED'}});
      sagaTester.dispatch(handleInvitationResponse('accept', 'key', 'contact@soler-sanandres.net'));
      await sagaTester.waitFor(RoomSharingEvent.INVITATION_ALREADY_ACCEPTED);
    });
  });
  describe('invitation response handler on already declined invitation', () => {
    beforeEach(() => {
      axios.get.mockImplementationOnce(() => promise);
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('dispatches INVITATION_ADDED', async () => {
      promise = Promise.resolve({data: {...invitation, status: 'DECLINED'}});
      sagaTester.dispatch(handleInvitationResponse('accept', 'key', 'contact@soler-sanandres.net'));
      await sagaTester.waitFor(RoomSharingEvent.INVITATION_ALREADY_DECLINED);
    });
  });
  describe('invitation response handler on invitation', () => {
    let promise2;
    beforeEach(() => {
      axios.get.mockImplementationOnce(() => promise);
      axios.put.mockImplementationOnce(() => promise2);
    });

    afterEach(() => {
      axios.get.mockReset();
      axios.put.mockReset();
    });

    it('dispatches INVITATION_ADDED', async () => {
      promise = Promise.resolve({data: {...invitation}});
      promise2 = Promise.resolve({data: {...invitation, status: 'PARTIALLY_ACCEPTED:1'}});
      sagaTester.dispatch(handleInvitationResponse('accept', 'key', 'contact@soler-sanandres.net'));
      await sagaTester.waitFor(RoomSharingEvent.INVITATION_HANDLED);
    });
  });
});
