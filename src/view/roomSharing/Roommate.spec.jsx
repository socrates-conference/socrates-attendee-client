// @flow
import {
  mount
} from 'enzyme';
import React from 'react';
import expect from '../../test/expect';
import {Roommate} from './Roommate';

const props = {
  userName: '',
  gender: '',
  introduction: '',
  sharedRoom: true,
  validated: false,
  roommateChoice: 'manual',
  roommateEmail: 'test@test.de',
  requestAutomaticRoommate: () => {},
  requestKnownRoommate: () => {},
  updateRoomSharing: () => {},
  personId: 4711,
  email: 'paticipant@email.de',
  findRoomSharingInformation: () => {},
  hasPendingInvitations: false,
  hasRoommate: false,
  isInMarketplace: false,
  sentInvitation: undefined,
  receivedInvitations: [],
  selectedRoommate: '',
  errorAddingInvitation: ''
};
const invitation = {
  id: 4711,
  invitee1: {id: 408, email: 'contact@soler-sanandres.net'},
  invitee2: {id: 7, email: 'alexandre@soler-sanandres.net'},
  inviter: {id: 408, email: 'contact@soler-sanandres.net'},
  status: 'PARTIALLY_ACCEPTED:1'
};

describe('Roommate:', () => {
  describe('should render', () => {
    it('with default data', () => {
      const wrapper = mount(<Roommate {...props}/>);
      expect(wrapper).toMatchSnapshot();
    });
    it('with sent invitation', () => {
      const newProps = {...props, hasPendingInvitations: true, sentInvitation: invitation};
      const wrapper = mount(<Roommate {...newProps}/>);
      expect(wrapper).toMatchSnapshot();
    });
    it('with received invitation', () => {
      const newProps = {...props, hasPendingInvitations: true, receivedInvitations: [invitation]};
      const wrapper = mount(<Roommate {...newProps}/>);
      expect(wrapper).toMatchSnapshot();
    });
    it('with roommate', () => {
      const newProps = {...props, hasRoommate: true, receivedInvitations: [{...invitation, status: 'ACCEPTED'}],
        selectedRoommate: 'someone@test.de'};
      const wrapper = mount(<Roommate {...newProps}/>);
      expect(wrapper).toMatchSnapshot();
    });
  });
});
