import {INITIAL_STATE, roomSharingReducer} from './roomSharingReducer';
import {
  invitationAdded,
  invitationAlreadyAccepted, invitationAlreadyDeclined, invitationHandled,
  receivedInvitationsFound,
  roomSharingChanged,
  sentInvitationFound
} from './roomSharingEvents';

describe(' room sharing reducer', () => {
  it('updates changes to state', () => {
    const event = roomSharingChanged('roommateChoice', 'automatic');
    const result = roomSharingReducer(INITIAL_STATE, event);
    expect(result.roommateChoice).toEqual('automatic');
  });
  describe('on invitation added', () => {
    let event, invitation;
    beforeEach(() => {
      invitation = {
        id: 16,
        invitee1: {id: 408, email: 'contact@soler-sanandres.net'},
        invitee2: {id: 7, email: 'alexandre@soler-sanandres.net'},
        inviter: {id: 408, email: 'contact@soler-sanandres.net'},
        status: 'ACCEPTED'
      };
      event = invitationAdded(invitation);

    });
    it('can be set to undefined', () => {
      const eventWithoutData = invitationAdded();
      const result = roomSharingReducer(INITIAL_STATE, eventWithoutData);
      expect(result.sentInvitation).toBeUndefined();
    });
    it('can be set to invitation object', () => {
      const result = roomSharingReducer(INITIAL_STATE, event);
      expect(result.sentInvitation).toBeDefined();
    });
    it('hasRoommate is false', () => {
      const result = roomSharingReducer(INITIAL_STATE, event);
      expect(result.hasRoommate).toBe(false);
    });
    it('hasPendingInvitations is true', () => {
      const result = roomSharingReducer(INITIAL_STATE, event);
      expect(result.hasPendingInvitations).toBe(true);
    });
  });
  describe('on sent invitation found', () => {
    let event, invitation;
    beforeEach(() => {
      invitation = {
        id: 16,
        invitee1: {id: 408, email: 'contact@soler-sanandres.net'},
        invitee2: {id: 7, email: 'alexandre@soler-sanandres.net'},
        inviter: {id: 408, email: 'contact@soler-sanandres.net'},
        status: 'ACCEPTED'
      };
      event = sentInvitationFound(invitation);

    });
    it('can be set to undefined', () => {
      const eventWithoutData = sentInvitationFound();
      const result = roomSharingReducer(INITIAL_STATE, eventWithoutData);
      expect(result.sentInvitation).toBeUndefined();
    });
    it('can be set to invitation object', () => {
      const result = roomSharingReducer(INITIAL_STATE, event);
      expect(result.sentInvitation).toBeDefined();
    });
    it('if invitation is accepted hasRoommate is true', () => {
      const result = roomSharingReducer(INITIAL_STATE, event);
      expect(result.hasRoommate).toBe(true);
    });
    it('if invitation is accepted selected roommate is set to the email of invitee2', () => {
      const result = roomSharingReducer(INITIAL_STATE, event);
      expect(result.selectedRoommate).toBe('alexandre@soler-sanandres.net');
    });
    it('if invitation is not accepted and no received invitations hasRoommate is false', () => {
      invitation.status = 'OPEN';
      const result = roomSharingReducer(INITIAL_STATE, event);
      expect(result.hasRoommate).toBe(false);
    });
  });
  describe('on received invitations found', () => {
    let event, invitation;
    beforeEach(() => {
      invitation = {
        id: 16,
        invitee1: {id: 408, email: 'contact@soler-sanandres.net'},
        invitee2: {id: 7, email: 'alexandre@soler-sanandres.net'},
        inviter: {id: 408, email: 'contact@soler-sanandres.net'},
        status: 'ACCEPTED'
      };
      event = receivedInvitationsFound(
        [invitation]);

    });
    it('can be set to empty array', () => {
      const eventWithoutData = sentInvitationFound([]);
      const result = roomSharingReducer(INITIAL_STATE, eventWithoutData);
      expect(result.receivedInvitations.length).toBe(0);
    });
    it('can be set to invitation object', () => {
      const result = roomSharingReducer(INITIAL_STATE, event);
      expect(result.receivedInvitations.length).toBe(1);
    });
    it('if received invitation is accepted hasRoommate is true', () => {
      const result = roomSharingReducer(INITIAL_STATE, event);
      expect(result.hasRoommate).toBe(true);
    });
    it('if received invitation is accepted selected roommate is set to the email of invitee1', () => {
      const result = roomSharingReducer(INITIAL_STATE, event);
      expect(result.selectedRoommate).toBe('contact@soler-sanandres.net');
    });
    it('if received invitation is not accepted and no sent invitations hasRoommate is false', () => {
      invitation.status = 'OPEN';
      const result = roomSharingReducer(INITIAL_STATE, event);
      expect(result.hasRoommate).toBe(false);
    });
  });
  it('on invitation already accepted', () => {
    const result = roomSharingReducer(INITIAL_STATE, invitationAlreadyAccepted());
    expect(result.accepted).toBe(false);
    expect(result.declined).toBe(false);
    expect(result.alreadyAccepted).toBe(true);
    expect(result.alreadyDeclined).toBe(false);
  });
  it('on invitation already declined', () => {
    const result = roomSharingReducer(INITIAL_STATE, invitationAlreadyDeclined());
    expect(result.accepted).toBe(false);
    expect(result.declined).toBe(false);
    expect(result.alreadyAccepted).toBe(false);
    expect(result.alreadyDeclined).toBe(true);
  });
  describe('on invitation handled', () => {
    it('and accepted', () => {
      const result = roomSharingReducer(INITIAL_STATE, invitationHandled('accept'));
      expect(result.accepted).toBe(true);
      expect(result.declined).toBe(false);
      expect(result.alreadyAccepted).toBe(false);
      expect(result.alreadyDeclined).toBe(false);
    });
    it('and declined', () => {
      const result = roomSharingReducer(INITIAL_STATE, invitationHandled('decline'));
      expect(result.accepted).toBe(false);
      expect(result.declined).toBe(true);
      expect(result.alreadyAccepted).toBe(false);
      expect(result.alreadyDeclined).toBe(false);
    });
  });
});
