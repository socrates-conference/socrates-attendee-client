// @flow


import {RoomSharingEvent} from './roomSharingEvents';

export type InvitationRequest = {
  inviterId: number,
  inviterEmail: string,
  invitee1Id: number,
  invitee1Email: string,
  invitee2Email: string,
}
export type InvitableRequest = {
  email: string,
  userName: string,
  gender: string,
  introduction: string
}
export type Person = {
  id: number,
  email: string
}

export type InvitationStatus =
  'UNKNOWN'
  | 'OPEN'
  | 'ACCEPTED'
  | 'DECLINED'
  | 'PARTIALLY_ACCEPTED:1'
  | 'PARTIALLY_ACCEPTED:2';

export type Invitation = {
  id: number,
  invitee1: Person,
  invitee2: Person,
  inviter: Person,
  status: InvitationStatus
}
export type Invitable = {
  id: number,
  personId: number,
  email: string,
  userName: string,
  gender: string,
  roomType: string
}


type State = {
  alreadyAccepted: boolean,
  alreadyDeclined: boolean,
  newStatus: InvitationStatus,
  accepted: boolean,
  declined: boolean,
  sentInvitation?: Invitation,
  receivedInvitations: Invitation[],
  roommateChoice: string,
  roommateEmail: string,
  gender: string,
  introduction: string,
  hasPendingInvitations: boolean,
  hasRoommate: boolean,
  isInMarketplace: boolean,
  selectedRoommate: string,
  errorAddingInvitation: string
};

export const INITIAL_STATE = {
  alreadyAccepted: false,
  alreadyDeclined: false,
  newStatus: 'OPEN',
  accepted: false,
  declined: false,
  sentInvitation: undefined,
  receivedInvitations: [],
  roommateChoice: 'manual',
  roommateEmail: '',
  gender: '',
  introduction: '',
  hasPendingInvitations: false,
  hasRoommate: false,
  isInMarketplace: false,
  selectedRoommate: '',
  errorAddingInvitation: ''
};

export const roomSharingReducer = (state: State = INITIAL_STATE, event: RoomSharingEvent) => {
  function hasRoommate(sentInvitation?: Invitation, receivedInvitations?: Invitation[]): boolean {
    const hasAcceptedReceivedInvitation = receivedInvitations
      ? receivedInvitations.filter(x => x.status === 'ACCEPTED').length === 1
      : false;

    const hasAcceptedSentInvitation = sentInvitation && sentInvitation.status === 'ACCEPTED';

    return hasAcceptedSentInvitation || hasAcceptedReceivedInvitation;
  }

  function hasPendingInvitations(sentInvitation?: Invitation, receivedInvitations?: Invitation[]): boolean {
    const hasPendingReceivedInvitation = receivedInvitations
      ? receivedInvitations.filter(x => x.status !== 'ACCEPTED' || x.status !== 'DECLINED').length > 0
      : false;

    const hasPendingSendInvitation = sentInvitation && sentInvitation.status !== 'ACCEPTED'
      && sentInvitation.status !== 'DECLINED';
    return (hasPendingSendInvitation || hasPendingReceivedInvitation);
  }

  function determineRoommate(sentInvitation?: Invitation, receivedInvitations?: Invitation[]): string {
    const acceptedReceivedInvitations = receivedInvitations
      ? receivedInvitations.filter(x => x.status === 'ACCEPTED')
      : [];
    const hasAcceptedReceivedInvitation = acceptedReceivedInvitations.length === 1;

    const hasAcceptedSentInvitation = sentInvitation && sentInvitation.status === 'ACCEPTED';
    if (hasAcceptedSentInvitation) {
      return sentInvitation && sentInvitation.invitee2 ? sentInvitation.invitee2.email : 'not available';
    }
    if (hasAcceptedReceivedInvitation) {
      return acceptedReceivedInvitations[0].invitee1 ? acceptedReceivedInvitations[0].invitee1.email : 'not available';
    }
    return '';
  }

  switch (event.type) {
    case RoomSharingEvent.ROOM_SHARING_CHANGED:
      return {
        ...state, [event.payload.fieldName]: event.payload.value
      };
    case RoomSharingEvent.INVITATION_HANDLED:
      return {
        ...state, alreadyAccepted: false, alreadyDeclined: false,
        accepted: event.payload === 'accept',
        declined: event.payload === 'decline',
        errorAddingInvitation: ''
      };
    case RoomSharingEvent.INVITATION_ALREADY_ACCEPTED:
      return {
        ...state, alreadyAccepted: true, alreadyDeclined: false, accepted: false, declined: false,
        errorAddingInvitation: ''
      };
    case RoomSharingEvent.INVITATION_ALREADY_DECLINED:
      return {
        ...state, alreadyAccepted: false, alreadyDeclined: true, accepted: false, declined: false,
        errorAddingInvitation: ''
      };
    case RoomSharingEvent.INVITABLE_ADDED:
      return {
        ...state, hasRoommate: false, isInMarketplace: true,
        hasPendingInvitations: false, selectedRoommate: '', errorAddingInvitation: ''
      };
    case RoomSharingEvent.INVITABLE_ADD_FAILED:
      return {
        ...state, hasRoommate: false, isInMarketplace: false,
        hasPendingInvitations: false, selectedRoommate: '', errorAddingInvitation: event.payload
      };
    case RoomSharingEvent.INVITATION_ADDED:
      return {
        ...state, sentInvitation: event.payload, hasRoommate: false,
        hasPendingInvitations: true, selectedRoommate: '', errorAddingInvitation: ''
      };
    case RoomSharingEvent.INVITATION_ADD_FAILED:
      return {
        ...state, sentInvitation: undefined, hasRoommate: false,
        hasPendingInvitations: false, selectedRoommate: '', errorAddingInvitation: event.payload
      };
    case RoomSharingEvent.SENT_INVITATION_FOUND:
      return {
        ...state, sentInvitation: event.payload, hasRoommate: hasRoommate(event.payload, state.receivedInvitations),
        hasPendingInvitations: hasPendingInvitations(event.payload, state.receivedInvitations),
        selectedRoommate: determineRoommate(event.payload, state.receivedInvitations), errorAddingInvitation: ''
      };
    case RoomSharingEvent.RECEIVED_INVITATIONS_FOUND:
      return {
        ...state, receivedInvitations: event.payload, hasRoommate: hasRoommate(state.sentInvitation, event.payload),
        hasPendingInvitations: hasPendingInvitations(state.sentInvitation, event.payload),
        selectedRoommate: determineRoommate(state.sentInvitation, event.payload), errorAddingInvitation: ''
      };
    case RoomSharingEvent.IS_IN_MARKETPLACE_CHANGED:
      return {
        ...state, isInMarketplace: event.payload
      };
    default:
      return state;
  }
};
export default roomSharingReducer;
