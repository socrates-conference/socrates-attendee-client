// @flow
import {
  shallow
} from 'enzyme';
import React from 'react';
import expect from '../../test/expect';
import {Conference} from './Conference';

describe('Profile: Conference', () => {
  const conferenceData = {
    arrival: '0',
    departure: '5',
    dietary: true,
    dietaryInfo: 'Dietary Info',
    family: true,
    familyInfo: 'Family Info',
    price:{
      nights: 4,
      room: 'TestRoom',
      roomName: 'Test room',
      roomPricePerNight: 50,
      totalRoomPrice: 200,
      fees: [],
      sponsored: 50,
      totalFeesPrice: 50,
      totalPrice: 200
    },
    validated: true,
    onChange: () => {},
    onToggle: () => {}
  };

  const emptyData = {
    arrival: '',
    departure: '',
    dietary: false,
    dietaryInfo: '',
    family: false,
    familyInfo: '',
    price: undefined,
    validated: false,
    onChange: () => {},
    onToggle: () => {}
  };

  describe('when user data was collected', () => {
    it('should render correctly', () => {
      const wrapper = shallow(<Conference {...conferenceData} />);
      expect(wrapper).toMatchSnapshot('no data');
    });
  });

  describe('when user data was never collected', () => {
    const wrapper = shallow(<Conference {...emptyData} />);

    it('should render correctly', () => {
      expect(wrapper).toMatchSnapshot('with data');
    });
  });
});
