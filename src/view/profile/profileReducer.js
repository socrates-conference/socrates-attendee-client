// @flow

import {ProfileEvent} from './profileEvents';
import {RoomSharingEvent} from '../roomSharing/roomSharingEvents';

const ROOM_NAMES = {
  'single': 'Single room',
  'bedInDouble': 'Double room / shared',
  'juniorExclusively': 'Jr. Double room / not shared',
  'juniorShared': 'Jr. Double room / shared'
};

export type PriceCalculation = {
  nights: number,
  room: string,
  roomName?: string,
  roomPricePerNight: number,
  totalRoomPrice: number,
  fees: string[],
  sponsored: number,
  totalFeesPrice: number,
  totalPrice: number
};

export type Participant = {
  participantId: ?number,
  personId: number,
  nickname: string,
  email: string,
  firstname: string,
  lastname: string,
  company: string,
  confirmed: boolean,
  address1: string,
  address2: string,
  province: string,
  postal: string,
  city: string,
  country: string,
  arrival: string,
  departure: string,
  dietary: boolean,
  dietaryInfo: string,
  family: boolean,
  familyInfo: string,
  gender: string,
  tshirt: string,
  labelname: string,
  price?: PriceCalculation,
  social: string,
  pronoun: string,
  sharedRoom: boolean,
};

type State = Participant & {
  submitted: boolean,
  confirmed: boolean,
  hasData: boolean
};

export const INITIAL_STATE = {
  address1: '',
  address2: '',
  arrival: '',
  city: '',
  company: '',
  confirmed: false,
  country: '',
  departure: '',
  dietary: false,
  dietaryInfo: '',
  email: '',
  error: false,
  family: false,
  familyInfo: '',
  firstname: '',
  gender: '',
  hasData: false,
  participantId: 0,
  personId: 0,
  introduction: '',
  lastname: '',
  nickname: '',
  labelname: '',
  postal: '',
  pronoun: '',
  province: '',
  roommateChoice: 'manual',
  roommateEmail: '',
  roommateRequested: false,
  sharedRoom: false,
  social: '',
  submitted: false,
  tshirt: '',
  validated: {},
  isDataPrivacyConfirmedChecked: false
};

export const profileReducer = (state: State = INITIAL_STATE, event: ProfileEvent | RoomSharingEvent) => {
  switch (event.type) {
    case ProfileEvent.PRICE_CALCULATED:
      return {...state, price: priceWithRoomName(event.payload)};
    case ProfileEvent.PARTICIPANT_FOUND:
      return {
        ...state, hasData: true, submitted: false, error: false, ...event.payload,
        participantId: event.payload.id,
        price: priceWithRoomName(event.payload.price)
      };
    case ProfileEvent.PARTICIPANT_NOT_FOUND:
      return {...state, hasData: false, submitted: false, error: false};
    case ProfileEvent.PARTICIPANT_CONFIRMED:
    case ProfileEvent.PARTICIPANT_UPDATED:
      return {
        ...state, hasData: true, submitted: true, error: false, ...event.payload,
        price: priceWithRoomName(event.payload.price)
      };
    case ProfileEvent.PROFILE_CHANGED:
      return {...state, ...event.payload};
    case 'Error/PARTICIPANT_SUBMIT':
      return {...state, error: true, submitted: false};
    case RoomSharingEvent.INVITATION_ADDED:
      return {
        ...state, roommateRequested: true, roommateEmail: event.payload.invitee2.email
      };
    default:
      return state;
  }
};
export default profileReducer;

const priceWithRoomName = (price: PriceCalculation): PriceCalculation => {
  price.roomName = ROOM_NAMES[price.room];
  return price;
};
