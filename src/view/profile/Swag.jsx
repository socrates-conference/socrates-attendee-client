// @flow
import React from 'react';
import {FormContainer} from './FormContainer';
import {InputSelect} from './InputSelect';
import {HelpText} from './HelpText';

export type SwagProps = {
  tshirt: string,
  validated: any,
  onChange: (event: SyntheticEvent<any>) => void
}
export const Swag = ({tshirt, validated, onChange}: SwagProps) => {
  return (<FormContainer name="swag" className="withMargin" headline="Conference Swag:">
    <div className="form-row">
      <div className="col">
        <div className="row">
          <InputSelect name="tshirt"
            required
            labelText="Your t-shirt size:"
            onChange={onChange}
            value={tshirt}
            placeholderText="Please select an option"
            helpName="tshirtHelp"
            isValid={validated['tshirt']}
            options={[{value: '10', label: 'Unisex S'},
              {value: '11', label: 'Unisex M'},
              {value: '12', label: 'Unisex L'},
              {value: '13', label: 'Unisex XL'},
              {value: '14', label: 'Unisex XXL'},
              {value: '15', label: 'Unisex XXXL'},
              {value: '99', label: 'No t-shirt, please'}]}
          />
        </div>
        <HelpText name="tshirtHelp">We provide a unisex cut (sizes S to XXXL). If you feel like you don&apos;t
          like t-shirts, or these sizes won&apos;t fit anyway,
          that&apos;s okay - please just tell us you don&apos;t want one.</HelpText>
      </div>
    </div>
  </FormContainer>);
};