// @flow
import React from 'react';

type Props = {
  name: string,
  fadeExpression?: () => boolean,
  children?: any
}

export function HelpText({name, children, fadeExpression}: Props) {
  const className = fadeExpression ? 'row ' + (fadeExpression() ? 'fadeIn' : 'fadeOut') : 'row';
  return (<div className={className}>
    <div className="col">
      <small id={name} className="form-text text-muted">{children}</small>
    </div>
  </div>);
}