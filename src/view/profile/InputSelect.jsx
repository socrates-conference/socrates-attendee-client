//@flow
import React from 'react';

export type SelectOption = {
  label: string,
  value: string
}
type Props = {
  className?: string,
  name: string,
  placeholderText: string,
  isValid?: boolean,
  labelText: string,
  required: boolean,
  onChange: (event: SyntheticEvent<HTMLInputElement>) => void,
  value: string,
  helpName: string,
  options: SelectOption[]
}

export function InputSelect(props: Props) {
  const {className, name, value, options, onChange, helpName, isValid, labelText, placeholderText, required} = props;
  const labelClassName = required ? 'col-form-label required' : 'col-form-label';
  const validClassName = isValid === undefined ? '' : isValid ? 'is-valid' : 'is-invalid';
  return (<div className={className || 'col'}>
    <label htmlFor={name} className={labelClassName}>{labelText}</label>
    <select className={`form-control ${validClassName}`} id={name}
      onChange={onChange}
      name={name}
      required
      value={value}
      aria-describedby={helpName || name + 'Help'}>
      <option disabled value="">{placeholderText}</option>
      {options.map(({label, value: val}) => (<option key={val} value={val}>{label}</option>))}
    </select>
  </div>);
}