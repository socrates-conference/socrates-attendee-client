// @flow
import React from 'react';

type Props = {
  className?: string,
  labelText: string,
  name: string,
  helpName?: string,
  isValid?: boolean,
  onChange: (event: SyntheticEvent<HTMLInputElement>) => void,
  placeholderText?: string,
  required?: boolean,
  value: string,
  rows?: number
}

export function InputTextArea(props: Props) {
  const {className, name, helpName, isValid, labelText, placeholderText, onChange, required, value, rows} = props;
  const validClassName = isValid === undefined ? '' : isValid ? 'is-valid' : 'is-invalid';
  return (<div className={className ? className : 'col'}>
    <label htmlFor={name} className="col-form-label">{labelText}</label>
    <textarea name={name} value={value}
      id={name}
      required={required}
      placeholder={placeholderText}
      className={`form-control ${validClassName}`}
      onChange={onChange}
      aria-describedby={helpName ? helpName : name + 'Help'}
      rows={rows ? rows : 2}
    />
  </div>);
}
