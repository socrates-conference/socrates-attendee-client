// @flow
import React from 'react';
import {InputCheckbox} from './InputCheckbox';
import {InputTextArea} from './InputTextArea';

type Props = {
  className?: string,
  labelText: string,
  name: string,
  helpName?: string,
  onChange: (event: SyntheticEvent<HTMLInputElement>) => void,
  placeholderText?: string,
  required?: boolean,
  value: string,
  fadeExpression: () => boolean,
  textName: string,
  textLabelText: string,
  toggleValue: boolean,
  onToggle: (event: SyntheticEvent<HTMLInputElement>) => void
}

export function InputToggleTextArea(props: Props) {
  const {
    className, fadeExpression, name, textName, helpName, labelText,
    textLabelText, placeholderText, onChange, onToggle, value, toggleValue
  } = props;
  return (<div>
    <div className={className}>
      <InputCheckbox name={name}
        value={toggleValue}
        id={name}
        onToggle={onToggle}
        labelText={labelText}/>
    </div>
    <div className={fadeExpression ? 'row  ' + (fadeExpression() ? 'fadeIn' : 'fadeOut') : 'row'}>
      <InputTextArea name={textName} value={value}
        labelText={textLabelText}
        placeholderText={placeholderText}
        helpName={helpName ? helpName : name + 'Help'}
        onChange={onChange}/>
    </div>
  </div>);
}