import {profileReducer, INITIAL_STATE} from './profileReducer';
import {profileChanged} from './profileEvents';

describe(' profile reducer', () => {
  it('should override values in state by values in event on profile change', () => {
    const event = profileChanged({firstname: 'Test'});
    const result = profileReducer(INITIAL_STATE, event);
    expect(result.firstname).toEqual('Test');
  });
});