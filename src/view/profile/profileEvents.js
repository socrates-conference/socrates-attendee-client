// @flow

import type {
  Participant,
  PriceCalculation
} from './profileReducer';

export class ProfileEvent {

  static get PRICE_CALCULATED() {return 'ProfileEvent/PRICE_CALCULATED';}

  static get PARTICIPANT_FOUND() {return 'ProfileEvent/PARTICIPANT_FOUND';}

  static get PARTICIPANT_NOT_FOUND() {return 'ProfileEvent/PARTICIPANT_NOT_FOUND';}

  static get PARTICIPANT_CONFIRMED() {return 'ProfileEvent/PARTICIPANT_CONFIRMED';}

  static get PARTICIPANT_UPDATED() {return 'ProfileEvent/PARTICIPANT_UPDATED'; }

  static get PROFILE_CHANGED() {return 'ProfileEvent/PROFILE_CHANGED'; }

  type: string;
  payload: any;
}

export const priceCalculated = (price: PriceCalculation) => ({
  type: ProfileEvent.PRICE_CALCULATED,
  payload: price
});

export const participantFound = (participant: Participant) => ({
  type: ProfileEvent.PARTICIPANT_FOUND,
  payload: participant
});

export const participantNotFound = (email: string) => ({
  type: ProfileEvent.PARTICIPANT_NOT_FOUND,
  payload: email
});

export const participantConfirmed = (participant: Participant) => ({
  type: ProfileEvent.PARTICIPANT_CONFIRMED,
  payload: participant
});

export const participantUpdated = (participant: Participant) => ({
  type: ProfileEvent.PARTICIPANT_UPDATED,
  payload: participant
});

export const profileChanged = (value: Object) => ({
  type: ProfileEvent.PROFILE_CHANGED,
  payload: value
});

