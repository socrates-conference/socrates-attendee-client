// @flow
import {
  shallow
} from 'enzyme';
import React from 'react';
import expect from '../../test/expect';
import {DataPrivacyCheckbox} from './DataPrivacyCheckbox';

describe('Profile: DataPrivacyCheckBox', () => {
  const withData = {
    isDataPrivacyConfirmedChecked: true,
    toggleDataPrivacyConfirmation: () => {}
  };

  const emptyData = {
    isDataPrivacyConfirmedChecked: false,
    toggleDataPrivacyConfirmation: () => {}
  };

  describe('when user data was collected', () => {
    it('should render correctly', () => {
      const wrapper = shallow(<DataPrivacyCheckbox {...withData} />);
      expect(wrapper).toMatchSnapshot('no data');
    });
  });

  describe('when user data was never collected', () => {
    const wrapper = shallow(<DataPrivacyCheckbox {...emptyData} />);

    it('should render correctly', () => {
      expect(wrapper).toMatchSnapshot('with data');
    });
  });
});
