//@flow
import React from 'react';
import {FormContainer} from './FormContainer';
import {InputSelect} from './InputSelect';
import {InputToggleTextArea} from './InputToggleTextArea';
import {HelpText} from './HelpText';
import type {PriceCalculation} from './profileReducer';

type Props = {
  arrival: string,
  departure: string,
  dietary: boolean,
  dietaryInfo: string,
  family: boolean,
  familyInfo: string,
  price?: ?PriceCalculation,
  validated: any,
  onChange: (event: SyntheticEvent<any>) => void,
  onToggle: (event: SyntheticEvent<any>) => void
}
export const Conference = ({
  arrival, departure, dietary, dietaryInfo, family, familyInfo, price, validated, onChange, onToggle
}: Props) => {
  return (<FormContainer name="conference" className="withMargin" headline="Your stay at the hotel:">
    <div className="form-row">
      <div className="form-group col">
        <div className="row">
          <InputSelect name="arrival" value={arrival}
            options={[{value: '0', label: 'Thursday afternoon (with dinner)'},
              {value: '1', label: 'Thursday night (without dinner)'}]}
            className="col-lg-6 col-sm-12"
            required
            labelText="Day of arrival:"
            placeholderText="Please select a day"
            onChange={onChange}
            isValid={validated['arrival']}
            helpName="datesHelp"
          />
          <InputSelect name="departure" value={departure}
            options={[{value: '0', label: 'Saturday afternoon (without dinner)'},
              {value: '1', label: 'Saturday night (with dinner)'},
              {value: '2', label: 'Sunday morning'},
              {value: '3', label: 'Sunday afternoon (with lunch)'},
              {value: '4', label: 'Sunday night (with dinner)'},
              {value: '5', label: 'Monday morning'}]}
            className="col-lg-6 col-sm-12"
            required
            labelText="Day of departure:"
            placeholderText="Please select a day"
            onChange={onChange}
            isValid={validated['departure']}
            helpName="datesHelp"
          />
        </div>
        {price &&
        <div className="row">
          <div className="col">
            <div className="container" id="price">
              <h5>Your stay will likely cost:</h5>
              <table id="priceTable">
                <thead>
                  <tr>
                    <th>Position</th>
                    <th className="price">Price</th>
                    <th />
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{price.nights} nights in a {price.roomName ? price.roomName : price.room} ({toCurrency(
                      price.roomPricePerNight)} &euro;/night)
                    </td>
                    <td className="price">{toCurrency(price.totalRoomPrice)} &euro;</td>
                    <td/>
                  </tr>
                  <tr>
                    <td>Conference fee (food, drinks, session rooms)</td>
                    <td className="price">{toCurrency(price.totalFeesPrice)} &euro;</td>
                    <td/>
                  </tr>
                  <tr>
                    <td>Fees covered by sponsoring</td>
                    <td className="price cost">-{toCurrency(price.sponsored)} &euro;</td>
                    <td className="price cost">&lArr; WARNING: This amount may still change!
                    </td>
                  </tr>
                  <tr>
                    <td colSpan="3">
                      <small>(Sponsored fees are calculated by your individual conference fee times the
                        percentage of total conference fees covered by sponsoring money. This means, the sum of all
                        sponsoring money that isn&apos;t needed for organisational spending (such as materials,
                        conference swag, etc.) will be used to make the conference cheaper for everyone. It also means
                        that the calculated sum changes, when people change their length of stay, or when new sponsors
                        sign up, or if unforeseen additional organisational costs occur -- and <i>that</i> means we
                        can&apos;t tell you exactly how much you will have to pay, until the conference starts.)
                      </small>
                    </td>
                  </tr>
                  <tr>
                    <td colSpan="3">
                      <hr/>
                    </td>
                  </tr>
                  <tr>
                    <td>Total (without sponsoring)</td>
                    <td className="price">{toCurrency(price.totalPrice)} &euro;</td>
                    <td/>
                  </tr>
                  <tr>
                    <td><strong>Total (with sponsoring)</strong></td>
                    <td className="price income">
                      <strong>{toCurrency(price.totalPrice - price.sponsored)} &euro;</strong></td>
                    <td/>
                  </tr>
                  <tr>
                    <td colSpan="3">
                      <small>Please note that this calculation covers <b>only</b> your own conference cost, and nothing
                        else. If you choose to bring your family members, or a pet, or buy drinks at the bar, etc., you
                        will have to pay extra.
                      </small>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>}
        <InputToggleTextArea name="dietary" toggleValue={dietary}
          className="row withTopMargin"
          labelText="I have special dietary needs."
          fadeExpression={() => dietary}
          textName="dietaryInfo" value={dietaryInfo}
          textLabelText="Please explain what we can do to accommodate them:"
          helpName="dietaryHelp"
          onToggle={onToggle}
          onChange={onChange}/>
        <HelpText name="dietaryHelp">By default, all meals have at least two options, one &quot;with meat and
          everything&quot;, and one vegan. If you have allergies or intolerances, please let us know in advance,
          and the hotel kitchen will try to prepare something adequate.
        </HelpText>
        <InputToggleTextArea name="family" toggleValue={family}
          fadeExpression={() => family}
          className="row withTopMargin"
          labelText="I would like to bring my partner/family."
          textName="familyInfo" value={familyInfo}
          textLabelText="Please tell us about them:"
          helpName="familyHelp"
          onToggle={onToggle}
          onChange={onChange}
        />
        <HelpText name="familyHelp" fadeExpression={() => family}>We only need to know the names and
          very basic info about who you are going to bring, as in &quot;I&apos;d like to bring my daughter Emma,
          she&apos;s 4 and a vegetarian.&quot;<br/>Partners and family members do not require a conference
          ticket, unless they want to participate fully, in which case please make sure they applied for the
          lottery.<br/>Hotel Park has a limited number of family rooms that will be given to people bringing
          their loved ones. When these rooms are taken, we will try to host you all in the hotel next door.
        </HelpText>
      </div>
    </div>
  </FormContainer>);
};

const toCurrency = (amount: number) => {
  const rounded = Math.round(amount * 100) * 0.01;
  const text = `${rounded}`;
  if (text.indexOf('.') > -1) {
    const parts = text.split('.');
    if (parts[1].length < 2) {
      parts[1] += '0';
    } else if (parts[1].length > 2) {
      parts[1] = parts[1].substr(0, 2);
    }
    return parts.join('.');
  }
  return text + '.00';
};