// @flow
import React from 'react';
import {InputTextRow} from './InputTextRow';

type Props = {
  disabled: boolean,
  submitted: boolean,
  error: boolean,
  onClick: () => void
};

export const SubmitButton = ({submitted, disabled, error, onClick}: Props) => {
  return (<InputTextRow>
    <div className="form-group col">
      {submitted ?
        (<div className="row">
          <div className="col">
            <div
              id="application-message"
              className="alert alert-success"
              role="alert">
              Data was submitted successfully.
            </div>
          </div>
        </div>) :
        (<div className="row">
          <div className="col-lg-10 col-sm-12">{disabled ?
            (<small className="form-text text-muted">You must confirm your registration and
              accept the Code of Conduct and the privacy information to submit your data.</small>) :
            <p>&nbsp;</p>}</div>
          <div className="col-lg-2 col-sm-12">
            <button disabled={disabled} className="btn btn-primary" onClick={onClick}>Send data...</button>
          </div>
        </div>)}
      {error &&
      (<div className="row">
        <div className="col">
          <div
            id="application-message"
            className="alert alert-danger"
            role="alert">
            There was an error submitting your data. Please try again later.
          </div>
        </div>
      </div>)}
    </div>
  </InputTextRow>);
};