// @flow
import {
  shallow
} from 'enzyme';
import React from 'react';
import expect from '../../test/expect';
import {Swag} from './Swag';

describe('Profile: Nametag', () => {
  const withData = {
    tshirt: '12',
    validated: true,
    onChange: () => {}
  };

  const emptyData = {
    tshirt: '',
    validated: false,
    onChange: () => {}
  };

  describe('when user data was collected', () => {
    it('should render correctly', () => {
      const wrapper = shallow(<Swag {...withData} />);
      expect(wrapper).toMatchSnapshot('no data');
    });
  });

  describe('when user data was never collected', () => {
    const wrapper = shallow(<Swag {...emptyData} />);

    it('should render correctly', () => {
      expect(wrapper).toMatchSnapshot('with data');
    });
  });
});
