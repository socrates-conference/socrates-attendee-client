// @flow

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import './profile.css';
import {
  confirmParticipant,
  findParticipant,
  updateParticipant,
  changeProfile
} from './profileCommand';
import type {
  Participant,
  PriceCalculation
} from './profileReducer';
import {SubmitButton} from './SubmitButton';
import {Nametag} from './Nametag';
import {Swag} from './Swag';
import Roommate from '../roomSharing/Roommate';
import {Conference} from './Conference';
import {Billing} from './Billing';
import {InputCheckbox} from './InputCheckbox';
import {InputTextRow} from './InputTextRow';
import {FormContainer} from './FormContainer';
import {HelpText} from './HelpText';
import config from '../../config';
import ExternalLink from '../common/ExternalLink';
import {DataPrivacyCheckbox} from './DataPrivacyCheckbox';
import CancelParticipationButton from '../cancelParticipation/CancelParticipationButton';

type Props = {
  participantId: ?number,
  personId: number,
  userName: string,
  address1: string,
  address2: string,
  arrival: string,
  city: string,
  company: string,
  confirmed: boolean,
  confirmParticipant: (participant: Participant) => void,
  country: string,
  departure: string,
  dietary: boolean,
  dietaryInfo: string,
  email: string,
  error: boolean,
  family: boolean,
  familyInfo: string,
  findParticipant: (email: string) => void,
  firstname: string,
  gender: string,
  introduction: string,
  lastname: string,
  labelname: string,
  nickname: string,
  postal: string,
  price?: PriceCalculation,
  pronoun: string,
  province: string,
  sharedRoom: boolean,
  social: string,
  tshirt: string,
  updateParticipant: (participant: Participant) => void,
  changeProfile: (value: any) => void,
  validated: any,
  isDataPrivacyConfirmedChecked: boolean,
  hasData: boolean,
  submitted: boolean
}

type State = {}

type EventHandler = (key: string, value: any) => void;

const REQUIRED_FIELDS = [
  'firstname',
  'lastname',
  'address1',
  'postal',
  'city',
  'country',
  'arrival',
  'departure',
  'tshirt',
  'labelname'
];
const isRequired = (name: string) => REQUIRED_FIELDS.some(key => key === name);

export class Profile extends Component<Props, State> {

  static defaultProps = {
    findParticipant: () => {},
    confirmParticipant: () => {},
    updateParticipant: () => {},
    requestAutomaticRoommate: () => {},
    requestKnownRoommate: () => {},
    changeProfile: () => {}
  };

  componentDidMount() {
    this.props.findParticipant(this.props.email);
  }

  _eventHandler = (handler: EventHandler) =>
    ({currentTarget: {name, value}}: SyntheticEvent<HTMLInputElement>) =>
      handler(name, value);

  _onChange = (key: string, value: any) => {
    this.props.changeProfile({[key]: value});
  };

  _onToggle = (key: string) => {
    this.props.changeProfile({[key]: !this.props[key], [`${key}Info`]: '', submitted: false});
  };

  _onFormSend = () => {
    this._validate();
  };

  _sendAfterValidate = () => {
    if (!this.props.hasData) {
      this.props.changeProfile({hasData: true});
      this.props.confirmParticipant(this.props);
    } else {
      this.props.updateParticipant(this.props);
    }
  };

  _validate = () => {
    const validated = Object.keys(this.props)
      .reduce((acc: any, key) => {
        const value = this.props[key];
        acc[key] = Boolean(!isRequired(key) || value);
        return acc;
      }, {...this.props.validated});

    const isFormValid = Object.keys(validated).reduce((acc, key) => acc && validated[key], true);
    if (!isFormValid) {
      this.props.changeProfile({validated});
    } else {
      this.props.changeProfile({validated, submitted: true});
      this._sendAfterValidate();
    }
  };

  _onDataPrivacyChange = () => {
    this.props.changeProfile({isDataPrivacyConfirmedChecked: !this.props.isDataPrivacyConfirmedChecked});
  };

  render() {
    const confirmLabel = 'I hereby confirm my registration for SoCraTes 2019. ' +
      'I have read and accept the terms of the conference Code of Conduct.';
    return (<div id="profile" className="container">
      <div className="row greeting"><h4>Hi, {this.props.userName}!</h4></div>
      {!this.props.hasData && <div className="row">
        <div className="col-lg-12">
          <p>So far, we haven&apos;t collected any personal information from you, except your nickname,
            email address and diversity status.</p>
          <p>In order to organize your stay at the hotel, we have to fill in those blanks now. </p>
          <p>You can help by completing this form:</p>
        </div>
      </div>}
      {this.props.hasData && <div className="row">
        <div className="col-lg-12">
          <p>You are registered {this.props.confirmed && 'and confirmed '} with the data below. Feel free to change it
            at any time. Please note that the following changes may not yield the result you expect:</p>
          <ul>
            <li>Changes of name tag contents just prior to the conference may not be in time for assembly of the actual
              name tags. If you intend to change your information within two weeks of the conference date, make sure to
              print your own label to insert into the name tag holder.
            </li>
            <li>Changes in t-shirt size can only be considered before the order has been placed.</li>
          </ul>
        </div>
      </div>}

      <Billing
        firstname={this.props.firstname}
        lastname={this.props.lastname}
        company={this.props.company}
        address1={this.props.address1}
        address2={this.props.address2}
        province={this.props.province}
        postal={this.props.postal}
        city={this.props.city}
        country={this.props.country}
        validated={this.props.validated}
        onChange={this._eventHandler(this._onChange)}
      />
      <Conference
        arrival={this.props.arrival}
        departure={this.props.departure}
        dietary={this.props.dietary}
        dietaryInfo={this.props.dietaryInfo}
        family={this.props.family}
        familyInfo={this.props.familyInfo}
        validated={this.props.validated}
        price={this.props.price}
        onChange={this._eventHandler(this._onChange)}
        onToggle={this._eventHandler(this._onToggle)}
      />
      <Roommate sharedRoom={this.props.sharedRoom}/>
      <Swag
        tshirt={this.props.tshirt}
        validated={this.props.validated}
        onChange={this._eventHandler(this._onChange)}
      />
      <Nametag
        labelname={this.props.labelname}
        pronoun={this.props.pronoun}
        social={this.props.social}
        validated={this.props.validated}
        onChange={this._eventHandler(this._onChange)}
      />
      <FormContainer name="confirmation" className="withMargin" headline={!this.props.hasData
        ? 'Let\'s make it official:' : ''}>
        {!this.props.hasData &&
        <InputTextRow>
          <div className="form-group col">
            <div className="row">
              <InputCheckbox
                name="confirmed"
                value={this.props.confirmed}
                required
                onToggle={this._eventHandler(this._onToggle)}
                labelText={confirmLabel}
              />
            </div>
            <HelpText className="row" name="confirmedHelp">Your registration with SoCraTes will not become valid
              until you actively accept this. If you haven&apos;t read the&nbsp;
            <ExternalLink url={`${config.siteUrl}/values`} target="_blank" title="Code of Conduct">
                Code of Conduct
            </ExternalLink>,
              please do so.
            </HelpText>
            <DataPrivacyCheckbox
              isDataPrivacyConfirmedChecked={this.props.isDataPrivacyConfirmedChecked}
              toggleDataPrivacyConfirmation={this._onDataPrivacyChange}
            />
          </div>
        </InputTextRow>}
        <SubmitButton
          disabled={!this.props.hasData && (!this.props.confirmed || !this.props.isDataPrivacyConfirmedChecked)}
          submitted={this.props.submitted}
          error={this.props.error}
          onClick={this._onFormSend}/>
      </FormContainer>
      {this.props.hasData && (<CancelParticipationButton/>)}
    </div>);
  }
}

const mapStateToProps = state => ({
  ...state.profile,
  email: state.authentication.email,
  userName: state.authentication.userName
});

const mapDispatchToProps = {
  findParticipant,
  confirmParticipant,
  updateParticipant,
  changeProfile
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Profile));

