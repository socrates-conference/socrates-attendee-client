// @flow
import React from 'react';

type Props = {
  additionalClassNames?: string,
  children?: any
}

export function InputTextRow({additionalClassNames, children}: Props) {
  return (<div className={additionalClassNames ? 'form-row' + additionalClassNames : 'form-row'}>
    {children}
  </div>);
}