// @flow
import React from 'react';

type Props = {
  name: string,
  className?: string,
  headline: string,
  children: any
}

export function FormContainer({name, className, headline, children}: Props) {
  return (<div className="form container" id={name + '-form'}>
    <div className="form-row">
      <h4 className={className}>
        {headline}
      </h4>
    </div>
    {children}
  </div>);
}