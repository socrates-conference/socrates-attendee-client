// @flow

import type {Participant} from './profileReducer';

export default class ProfileCommand {
  static get REQUEST_AUTOMATIC_ROOMMATE() { return 'Command/REQUEST_AUTOMATIC_ROOMMATE'; }

  static get REQUEST_KNOWN_ROOMMATE() { return 'Command/REQUEST_KNOWN_ROOMMATE'; }

  static get FIND_PARTICIPANT() { return 'Command/FIND_PARTICIPANT'; }

  static get CONFIRM_PARTICIPANT() { return 'Command/CONFIRM_PARTICIPANT'; }

  static get UPDATE_PARTICIPANT() { return 'Command/UPDATE_PARTICIPANT'; }

  static get CHANGE_PROFILE() { return 'Command/CHANGE_PROFILE'; }
}

export const findParticipant = (email: string) => ({type: ProfileCommand.FIND_PARTICIPANT, email});

export const confirmParticipant = (participant: Participant) => ({
  type: ProfileCommand.CONFIRM_PARTICIPANT,
  participant
});

export const updateParticipant = (participant: Participant) => ({type: ProfileCommand.UPDATE_PARTICIPANT, participant});

export const changeProfile = (value: any) => ({type: ProfileCommand.CHANGE_PROFILE, value});

export const requestKnownRoommate = (email: string) => ({type: ProfileCommand.REQUEST_KNOWN_ROOMMATE, email});

export const requestAutomaticRoommate = (email: string, gender: string, introduction: string) =>
  ({type: ProfileCommand.REQUEST_AUTOMATIC_ROOMMATE, email, gender, introduction});

export const submitError = (reason: string) => ({type: 'Error/PARTICIPANT_SUBMIT', reason});
