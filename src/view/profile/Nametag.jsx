// @flow

import {FormContainer} from './FormContainer';
import React from 'react';
import {InputText} from './InputText';
import {InputTextRow} from './InputTextRow';
import {HelpText} from './HelpText';

type Props = {
  labelname: string,
  pronoun: string,
  social: string,
  validated: any,
  onChange: (event: SyntheticEvent<any>) => void
};
export const Nametag = ({labelname, pronoun, social, validated, onChange}: Props) => {
  return (<FormContainer name="nametag" headline="Name tag content:">
    <InputTextRow>
      <div className="col">
        <div className="row">
          <InputText name="labelname" value={labelname}
            placeholderText="Nickname"
            labelText="Nickname"
            required
            isValid={validated['labelname']}
            onChange={onChange}/>
        </div>
        <HelpText name="nametagHelp">Name tags will be given to you when you arrive. You may choose to display
          your legal name or an alias, whichever you prefer. This should be the name you want people to call
          you at the conference.
        </HelpText>
      </div>
    </InputTextRow>
    <InputTextRow additionalClassNames=" withTopMargin">
      <div className="col">
        <div className="row">
          <InputText name="pronoun" value={pronoun}
            labelText="Preferred pronoun"
            placeholderText="e.g. he, she, they, e, ..."
            isValid={validated['pronoun']}
            onChange={onChange}/>
        </div>
        <HelpText name="pronounHelp">Fill this in if you prefer to be addressed by a specific pronoun, such as
          &quot;she&quot; or &quot;they&quot; (optional).</HelpText>
      </div>
    </InputTextRow>
    <InputTextRow additionalClassNames=" withTopMargin">
      <div className="col">
        <div className="row">
          <InputText name="social" value={social}
            labelText="Social media handle"
            placeholderText="e.g. Twitter/facebook name"
            isValid={validated['social']}
            onChange={onChange}/>
        </div>
        <HelpText name="socialHelp">Twitter or other social media handle (optional).</HelpText>
      </div>
    </InputTextRow>
  </FormContainer>);
};