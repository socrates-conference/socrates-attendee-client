//@flow
import React from 'react';
import {FormContainer} from './FormContainer';
import {InputTextRow} from './InputTextRow';
import {InputText} from './InputText';
import {HelpText} from './HelpText';

type Props = {
  firstname: string,
  lastname: string,
  company: string,
  address1: string,
  address2: string,
  province: string,
  postal: string,
  city: string,
  country: string,
  validated: any,
  onChange: (event: SyntheticEvent<any>) => void
}
export const Billing = ({
  firstname, lastname,
  company, address1, address2, province, postal, city, country,
  validated, onChange
}: Props) => {
  return (<FormContainer name="billing" className="withMargin" headline="Please provide a valid billing address:">
    <InputTextRow>
      <div className="form-group col">
        <div className="row">
          <InputText name="firstname" value={firstname}
            className="col-lg-6 col-sm-12"
            required
            onChange={onChange}
            labelText="First name:"
            placeholderText="Your first name"
            isValid={validated['firstname']}
          />
          <InputText name="lastname" value={lastname}
            className="col-lg-6 col-sm-12"
            required
            onChange={onChange}
            labelText="Last name:"
            placeholderText="Your last name"
            isValid={validated['lastname']}
          />
        </div>
        <HelpText name="namesHelp">This needs to be your (legal) first and last name.</HelpText>
      </div>
    </InputTextRow>
    <InputTextRow>
      <div className="form-group col">
        <div className="row">
          <InputText name="company" value={company}
            labelText="Company:"
            placeholderText="Your company name"
            isValid={validated['company']}
            onChange={onChange}
          />
        </div>
        <HelpText name="companyHelp">(If the hotel bill goes to your company)</HelpText>
      </div>
    </InputTextRow>
    <InputTextRow>
      <div className="form-group col">
        <div className="row">
          <InputText name="address1" value={address1}
            required
            labelText="Address line 1:"
            placeholderText="e.g. street and number"
            isValid={validated['address1']}
            onChange={onChange}
          />
        </div>
        <div className="row">
          <InputText name="address2" value={address2}
            labelText="Address line 2:"
            placeholderText="e.g. apartment or po.box number"
            isValid={validated['address2']}
            onChange={onChange}
          />
        </div>
        <div className="row withTopMargin">
          <InputText name="province" value={province}
            labelText="Province or State:"
            placeholderText="Province or State"
            isValid={validated['province']}
            onChange={onChange}
          />
        </div>
        <div className="row">
          <div className="col">
            <div className="row">
              <InputText name="postal" value={postal}
                className="col-lg-6 col-sm-12"
                required
                labelText="Postal Code:"
                placeholderText="Postal Code"
                isValid={validated['postal']}
                onChange={onChange}
              />
              <InputText name="city" value={city}
                className="col-lg-6 col-sm-12"
                required
                labelText="City:"
                placeholderText="City"
                isValid={validated['city']}
                onChange={onChange}
              />
            </div>
          </div>
        </div>
        <div className="row withTopMargin">
          <InputText name="country" value={country}
            required
            labelText="Country:"
            placeholderText="Country"
            isValid={validated['country']}
            onChange={onChange}
          />
        </div>
        <HelpText name="addressHelp">Since different countries use different address formats, we tried to make
          this form as generic as possible. Please fill in the required fields (which we believe will be required
          in all country formats), and use the others as needed for the tax validity of your bill and receipt).
        </HelpText>
      </div>
    </InputTextRow>
  </FormContainer>);
};