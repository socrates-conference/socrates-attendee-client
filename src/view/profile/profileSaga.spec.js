// @flow
import axios from 'axios';
import SagaTester from 'redux-saga-tester';
import rootSaga from '../sagas/rootSaga';
import {profileReducer, INITIAL_STATE} from './profileReducer';
import {ProfileEvent} from './profileEvents';
import {changeProfile, confirmParticipant, findParticipant, updateParticipant} from './profileCommand';
import * as api from '../requests/api';
import {stub} from 'sinon';
import {RoomSharingEvent} from '../roomSharing/roomSharingEvents';

jest.mock('axios');

const participant = {
  participantId: 0,
  personId: 4711,
  nickname: 'Test',
  email: 'test@test.de',
  firstname: 'Test One',
  lastname: 'With Lastname',
  company: '',
  confirmed: true,
  address1: 'addr1',
  address2: 'addr2',
  province: 'prov',
  postal: 'postal',
  city: 'city',
  country: 'country',
  arrival: '0',
  departure: '5',
  dietary: false,
  dietaryInfo: '',
  family: false,
  familyInfo: '',
  gender: '',
  tshirt: 'None',
  labelname: 'Label name',
  price: {
    nights: 4,
    room: 'single',
    roomName: '',
    roomPricePerNight: 50,
    totalRoomPrice: 200,
    fees: [],
    sponsored: 0,
    totalFeesPrice: 0,
    totalPrice: 200
  },
  social: 'twitter',
  pronoun: 'pronoun',
  sharedRoom: true
};

describe('ProfileSaga', () => {
  let sagaTester: SagaTester;
  const calculatePricesSpy = stub(api, 'calculatePrice');

  const updateState = async state => {
    sagaTester.dispatch(changeProfile(state));
    await sagaTester.waitFor(ProfileEvent.PROFILE_CHANGED);
  };

  beforeEach(() => {
    sagaTester = new SagaTester({initialState: {profile: INITIAL_STATE,
      authentication: {email: 'a@b.de'}}, reducers: {profile: profileReducer,
      authentication: (state) => state ? state : {}}});
    sagaTester.start(rootSaga);
    calculatePricesSpy.resolves({});
  });

  afterEach(() => {
    sagaTester.reset();
    calculatePricesSpy.resetHistory();
  });

  it('updates profile on value change', async() => {
    sagaTester.dispatch(changeProfile({firstname: 'Test'}));
    await sagaTester.waitFor(ProfileEvent.PROFILE_CHANGED);
  });

  describe('Prices calculation', () => {
    it('does not update the prices if neither arrival nor departure change', async() => {
      await updateState({arrival: '1', departure: '1'});
      expect(sagaTester.numCalled(ProfileEvent.PRICE_CALCULATED)).toBe(1);
      await updateState({firstname: 'Test'});
      expect(sagaTester.numCalled(ProfileEvent.PRICE_CALCULATED)).toBe(1);
    });

    it('does not update the prices if departure is missing and arrival changes', async() => {
      await updateState({arrival: '1'});
      await updateState({arrival: '2'});
      expect(sagaTester.wasCalled(ProfileEvent.PRICE_CALCULATED)).toBeFalsy();
    });

    it('does not update the prices if arrival is missing and departure changes', async() => {
      await updateState({departure: '1'});
      await updateState({departure: '2'});
      expect(sagaTester.wasCalled(ProfileEvent.PRICE_CALCULATED)).toBeFalsy();
    });

    it('does not update the prices if prices could not be fetched', async() => {
      calculatePricesSpy.resolves(null);
      await updateState({arrival: '1', departure: '1'});
      expect(sagaTester.wasCalled(ProfileEvent.PRICE_CALCULATED)).toBeFalsy();
    });

    it('updates prices, if arrival and deperature change', async() => {
      await updateState({arrival: '1', departure: '1'});
      await sagaTester.waitFor(ProfileEvent.PRICE_CALCULATED);
    });

    it('updates prices, if arrival is set for the first time and departure is set', async() => {
      await updateState({departure: '1'});
      await updateState({firstname: 'Test', arrival: '2'});
      await sagaTester.waitFor(ProfileEvent.PRICE_CALCULATED);
    });

    it('updates prices, if departure is set for the first time and arrival is set', async() => {
      await updateState({arrival: '1'});
      await updateState({firstname: 'Test', departure: '2'});
      await sagaTester.waitFor(ProfileEvent.PRICE_CALCULATED);
      await updateState({firstname: 'Test'});
    });
  });

  describe('finding the participant data', () => {
    describe('if participant cannot be found', () => {
      beforeEach(() => {
        axios.get.mockImplementation(() => Promise.resolve({data: null}));
      });

      afterEach(() => {
        axios.get.mockReset();
      });
      it('dispatches PARTICIPANT_NOT_FOUND', async() => {
        sagaTester.dispatch(findParticipant('test@test.de'));
        await sagaTester.waitFor(ProfileEvent.PARTICIPANT_NOT_FOUND);
      });
    });
    describe('if participant can be found', () => {
      beforeEach(() => {
        axios.get.mockImplementationOnce(() => Promise.resolve({data: participant}));
        axios.get.mockImplementationOnce(() => Promise.resolve({data: null}));
        axios.get.mockImplementationOnce(() => Promise.resolve({data: []}));
        axios.post.mockImplementationOnce(() => Promise.resolve({data: false}));
      });

      afterEach(() => {
        axios.get.mockReset();
      });
      describe('if participant can be found', () => {
        it('dispatches SENT_INVITATION_FOUND', async() => {
          sagaTester.dispatch(findParticipant('test@test.de'));
          await sagaTester.waitFor(RoomSharingEvent.SENT_INVITATION_FOUND);
        });
        it('dispatches RECEIVED_INVITATIONS_FOUND', async() => {
          sagaTester.dispatch(findParticipant('test@test.de'));
          await sagaTester.waitFor(RoomSharingEvent.RECEIVED_INVITATIONS_FOUND);
        });
        it('dispatches IS_IN_MARKETPLACE_CHANGED', async() => {
          sagaTester.dispatch(findParticipant('test@test.de'));
          await sagaTester.waitFor(RoomSharingEvent.IS_IN_MARKETPLACE_CHANGED);
        });
        it('dispatches PARTICIPANT_FOUND', async() => {
          sagaTester.dispatch(findParticipant('test@test.de'));
          await sagaTester.waitFor(ProfileEvent.PARTICIPANT_FOUND);
        });
      });
    });
  });

  describe('confirming participant attendance', () => {
    describe('if can save confirmation', () => {
      beforeEach(() => {
        axios.put.mockImplementation(() => Promise.resolve({data: {...participant, id: 4711}}));
      });

      afterEach(() => {
        axios.put.mockReset();
      });
      it('dispatches PARTICIPANT_CONFIRMED', async() => {
        sagaTester.dispatch(confirmParticipant(participant));
        await sagaTester.waitFor(ProfileEvent.PARTICIPANT_CONFIRMED);
      });
    });
    describe('if cannot save confirmation', () => {
      beforeEach(() => {
        axios.put.mockImplementation(() => Promise.reject({message: 'error'}));
      });

      afterEach(() => {
        axios.put.mockReset();
      });
      it('dispatches Error/PARTICIPANT_SUBMIT', async() => {
        sagaTester.dispatch(confirmParticipant(participant));
        await sagaTester.waitFor('Error/PARTICIPANT_SUBMIT');
      });
    });
  });
  describe('updating participant profile', () => {
    describe('if can save profile', () => {
      beforeEach(() => {
        axios.post.mockImplementation(() => Promise.resolve({data: {...participant, id: 4711}}));
      });

      afterEach(() => {
        axios.post.mockReset();
      });
      it('dispatches PARTICIPANT_UPDATED', async() => {
        sagaTester.dispatch(updateParticipant(participant));
        await sagaTester.waitFor(ProfileEvent.PARTICIPANT_UPDATED);
      });
    });
    describe('if cannot save profile', () => {
      beforeEach(() => {
        axios.post.mockImplementation(() => Promise.reject({message: 'error'}));
      });

      afterEach(() => {
        axios.post.mockReset();
      });
      it('dispatches Error/PARTICIPANT_SUBMIT', async() => {
        sagaTester.dispatch(updateParticipant(participant));
        await sagaTester.waitFor('Error/PARTICIPANT_SUBMIT');
      });
    });
  });
});
