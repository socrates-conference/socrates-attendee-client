// @flow
import React from 'react';

type Props = {
  checked: boolean,
  name: string,
  labelText: string,
  onChange: (event: SyntheticEvent<HTMLInputElement>) => void,
  value: string
}

export function InputRadio(props: Props) {
  const {name, checked, labelText, onChange, value} = props;
  return (<div className="col">
    <input
      type="radio"
      checked={checked}
      name={name}
      className="form-check form-check-inline"
      value={value}
      id={name}
      onChange={onChange}
      aria-describedby={name + 'Help'}
    />
    <label htmlFor={name} className="col-form-label">{labelText}</label>
  </div>);
}