// @flow
import {
  call,
  put,
  select
} from 'redux-saga/effects';
import * as api from '../requests/api';
import {
  participantConfirmed,
  participantFound,
  participantNotFound,
  participantUpdated,
  priceCalculated,
  profileChanged
} from './profileEvents';
import type {
  Participant,
  PriceCalculation
} from './profileReducer';
import ProfileCommand, {submitError} from './profileCommand';
import {isInMarketplaceChanged, receivedInvitationsFound, sentInvitationFound} from '../roomSharing/roomSharingEvents';

type FindParticipantData = {
  email: string
}

type ConfirmParticipantData = {
  participant: Participant
}

type UpdateParticipantData = {
  participant: Participant
}

export function* findParticipantSaga({email}: FindParticipantData): Iterable<any> {
  let participant: ?Participant;
  try {
    participant = yield call(api.findParticipant, email);
  } catch (e) {
    console.error('Error while finding participant:', e);
  }
  if (participant) {
    const sentInvitation = yield call(api.findSentInvitation, participant.personId);
    yield put(sentInvitationFound(sentInvitation));
    const receivedInvitations = yield call(api.findReceivedInvitations, participant.personId);
    yield put(receivedInvitationsFound(receivedInvitations));
    const isInMarketplace = yield call(api.isInMarketplace, participant.email);
    yield put(isInMarketplaceChanged(Boolean(isInMarketplace)));
    yield put(participantFound(participant));
  } else {
    yield put(participantNotFound(email));
  }
}

export function* confirmParticipantSaga({participant}: ConfirmParticipantData): Iterable<any> {
  let confirmedParticipant: ?Participant;
  try {
    confirmedParticipant = yield call(api.confirmParticipant, participant);
  } catch (e) {
    console.error('Confirmation failed:', e);
  }
  if (confirmedParticipant) {
    yield put(participantConfirmed(confirmedParticipant));
  } else {
    yield put(submitError(ProfileCommand.CONFIRM_PARTICIPANT));
  }
}

export function* updateParticipantSaga({participant}: UpdateParticipantData): Iterable<any> {
  let updatedParticipant: ?Participant;
  try {
    updatedParticipant = yield call(api.updateParticipant, participant);
  } catch (e) {
    console.error('Update failed:', e);
  }
  if (updatedParticipant) {
    yield put(participantUpdated(updatedParticipant));
  } else {
    yield put(submitError(ProfileCommand.UPDATE_PARTICIPANT));
  }
}

export function* changeProfileSaga(command: any): Iterable<any> {
  yield put(profileChanged(command.value));
  yield updatePrices(command.value);
}

function* updatePrices(payload) {
  const profile = yield select(state => state.profile);
  const priceDataUnchanged = !payload.arrival && !payload.departure;
  const priceDataMissing = !profile.arrival || !profile.departure;
  if(priceDataUnchanged || priceDataMissing) {
    return;
  }
  const personId = profile.personId;
  const arrival = profile.arrival;
  const departure = profile.departure;
  const email = yield select(state => state.authentication.email);
  const calculation: ?PriceCalculation = yield call(api.calculatePrice, personId, arrival, departure, email);
  if (calculation) {
    yield put(priceCalculated(calculation));
  }
}
