// @flow
import React from 'react';

type Props = {
  className?: string,
  labelText: string,
  name: string,
  helpName?: string,
  isValid?: boolean,
  onChange: (event: SyntheticEvent<HTMLInputElement>) => void,
  placeholderText?: string,
  required?: boolean,
  value: string
}

export function InputText({className, name, helpName, isValid, labelText, onChange, placeholderText, required,
  value}: Props) {
  const labelClassName = required ? 'col-form-label required' : 'col-form-label';
  const validClassName = isValid === undefined ? '' : isValid ? ' is-valid' : ' is-invalid';
  return (<div className={className ? className : 'col'}>
    <label htmlFor={name} className={labelClassName}>{labelText}</label>
    <input type="text"
      className={`form-control validName${validClassName}`}
      id={name}
      name={name}
      required={required}
      value={value}
      onChange={onChange}
      placeholder={placeholderText}
      aria-describedby={helpName ? helpName : name + 'Help'}
    />
  </div>);
}