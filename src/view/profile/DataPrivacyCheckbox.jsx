// @flow
import React from 'react';
import config from '../../config';
import ExternalLink from '../common/ExternalLink';

type Props = {
  isDataPrivacyConfirmedChecked: boolean,
  toggleDataPrivacyConfirmation: () => void
}

export function DataPrivacyCheckbox(props: Props) {
  const {isDataPrivacyConfirmedChecked} = props;
  const checkboxValidationClass = (isValid) => `form-control ${isValid ? 'is-valid' : 'is-invalid'}`;
  const checkboxClass = checkboxValidationClass(isDataPrivacyConfirmedChecked);
  return (
    <div className="row col mt-2">
      <div className="input-group">
        <div className="input-group-prepend">
          <div className="input-group-text">
            <input
              id="profile-dataPrivacy"
              type="checkbox" value="dataPrivacyConfirmed"
              checked={isDataPrivacyConfirmedChecked} onChange={props.toggleDataPrivacyConfirmation}
            />
          </div>
        </div>
        <label className={checkboxClass}>
          <div>
            I agree that the information above will be collected and processed.<br/><br/>
            <small>This data is necessary to make the hotel reservation and to carry out organizational measures around
              the conference. The data remains in your profile until you remove or change them yourself. Registration
              and application data will be deleted automatically after the current year conference is over. The profile
              information will persist to simplify the registration process in the coming years. Nevertheless, you can
              request the deletion of this data after the conference (see note below). If a deletion should be prevented
              by legal obligation to retain data, the data will be blocked until expiry of this obligation.<br/><br/>
            Note: You can revoke your consent at any time for the future via e-mail to
            registration AT socrates MINUS conference DOT de, thereby we will delete your data. <br/><br/>
            Detailed information on handling user data can be found in our&nbsp;
            <ExternalLink
              url={`${config.siteUrl}/privacy-policy`} target="_blank" title="Privacy policy">
              Privacy policy
            </ExternalLink>.</small>
          </div>
        </label>
      </div>
    </div>
  );
}