// @flow
import {
  shallow
} from 'enzyme';
import React from 'react';
import expect from '../../test/expect';
import {Nametag} from './Nametag';

describe('Profile: Nametag', () => {
  const withData = {
    labelname: 'label name',
    pronoun: 'pronoun',
    social: 'social',
    validated: true,
    onChange: () => {}
  };

  const emptyData = {
    labelname: '',
    pronoun: '',
    social: '',
    validated: false,
    onChange: () => {}
  };

  describe('when user data was collected', () => {
    it('should render correctly', () => {
      const wrapper = shallow(<Nametag {...withData} />);
      expect(wrapper).toMatchSnapshot('no data');
    });
  });

  describe('when user data was never collected', () => {
    const wrapper = shallow(<Nametag {...emptyData} />);

    it('should render correctly', () => {
      expect(wrapper).toMatchSnapshot('with data');
    });
  });
});
