// @flow
import {
  shallow
} from 'enzyme';
import React from 'react';
import {Profile} from './Profile';
import expect from '../../test/expect';

describe('Profile:', () => {
  const participant = {
    personId: 1,
    nickname: 'tester',
    email: 'test@test.de',
    firstname: 'tester',
    lastname: 'test',
    company: 'testco',
    address1: 'test 123',
    address2: 'apt. test',
    province: 'test',
    postal: '12345',
    city: 'testcity',
    country: 'testcountry',
    gender: 'female',
    introduction: '',
    arrival: 'Thursday afternoon',
    departure: 'Sunday evening',
    dietary: false,
    dietaryInfo: '',
    family: false,
    familyInfo: '',
    sharedRoom: true,
    tshirt: 'Unisex S',
    labelname: 'tester',
    social: '@tester',
    pronoun: 'she',
    roommateChoice: 'manual',
    roommateEmail: '',
    roommateRequested: false,
    participantId: 0,
    validated: {},
    isDataPrivacyConfirmedChecked: false,
    hasData: true
  };

  const initial = {
    address1: '',
    address2: '',
    arrival: '',
    city: '',
    company: '',
    country: '',
    departure: '',
    dietary: false,
    dietaryInfo: '',
    email: '',
    family: false,
    familyInfo: '',
    firstname: '',
    gender: '',
    hasData: false,
    participantId: 0,
    introduction: '',
    labelname: '',
    lastname: '',
    nickname: '',
    personId: 0,
    postal: '',
    pronoun: '',
    province: '',
    roommateChoice: 'manual',
    roommateEmail: '',
    roommateRequested: false,
    sharedRoom: true,
    social: '',
    tshirt: '',
    validated: {},
    isDataPrivacyConfirmedChecked: false
  };

  describe('when user data was collected', () => {
    it('should render correctly', () => {
      const wrapper = shallow(<Profile {...participant} confirmed={false}
        error={false} submitted={false} userName="test"/>);
      expect(wrapper).toMatchSnapshot('no data');
    });
  });

  describe('when user data was never collected', () => {
    const wrapper = shallow(<Profile {...initial} confirmed={false}
      error={false} submitted={false} userName="test"/>);

    it('should render correctly', () => {
      expect(wrapper).toMatchSnapshot('with data');
    });
  });
});
