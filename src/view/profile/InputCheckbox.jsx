// @flow
import React from 'react';

type Props = {
  name: string,
  labelText: string,
  onToggle: (event: SyntheticEvent<HTMLInputElement>) => void,
  required?: boolean,
  value: boolean
}

export function InputCheckbox(props: Props) {
  const {name, required, labelText, onToggle, value} = props;
  const labelClass = required ? 'col-form-label required' : 'col-form-label';
  return (<div className="col">
    <input
      type="checkbox"
      name={name}
      className="form-control"
      value={value}
      checked={value}
      id={name}
      required={required}
      onChange={onToggle}
      aria-describedby={name + 'Help'}
    />
    <label htmlFor={name} className={labelClass}>{labelText}</label>
  </div>);
}