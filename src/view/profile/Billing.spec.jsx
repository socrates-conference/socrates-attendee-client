// @flow
import {
  shallow
} from 'enzyme';
import React from 'react';
import expect from '../../test/expect';
import {Billing} from './Billing';

describe('Profile: Billing', () => {
  const participant = {
    firstname: 'First Name',
    lastname: 'Last Name',
    company: 'Company',
    address1: 'Address1',
    address2: 'Address2',
    province: 'Province',
    postal: 'Postal',
    city: 'City',
    country: 'Country',
    validated: true,
    onChange: () => {}
  };

  const initial = {
    firstname: '',
    lastname: '',
    company: '',
    address1: '',
    address2: '',
    province: '',
    postal: '',
    city: '',
    country: '',
    validated: false,
    onChange: () => {}
  };

  describe('when user data was collected', () => {
    it('should render correctly', () => {
      const wrapper = shallow(<Billing {...participant} />);
      expect(wrapper).toMatchSnapshot('no data');
    });
  });

  describe('when user data was never collected', () => {
    const wrapper = shallow(<Billing {...initial} />);

    it('should render correctly', () => {
      expect(wrapper).toMatchSnapshot('with data');
    });
  });
});
