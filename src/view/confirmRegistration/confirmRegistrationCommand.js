//@flow

export default class ConfirmRegistrationCommand {
  static get UPDATE_KEY(): string {
    return 'ConfirmRegistrationCommand/UPDATE_KEY';
  }
}

export const updateConfirmationKey =
  (key: string, email: string) => ({type: ConfirmRegistrationCommand.UPDATE_KEY, key, email});

