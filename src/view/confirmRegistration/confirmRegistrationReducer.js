// @flow

import {ConfirmRegistrationEvent} from './confirmRegistrationEvents';

export type ConfirmRegistrationState = {
  hasFailedKeyCheck: boolean
};

const INITIAL_STATE: ConfirmRegistrationState = {
  hasFailedKeyCheck: false
};

const confirmRegistrationReducer = (state: ConfirmRegistrationState = INITIAL_STATE, event: any) => {
  switch (event.type) {
    case ConfirmRegistrationEvent.CONFIRMATION_KEY_FAILED:
      return {...state, hasFailedKeyCheck: true};
    default:
      return state;
  }
};
export default confirmRegistrationReducer;
