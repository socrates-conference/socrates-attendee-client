// @flow
import SagaTester from 'redux-saga-tester';
import rootSaga from '../sagas/rootSaga';
import * as api from '../requests/api';
import type {ConfirmRegistrationState} from './confirmRegistrationReducer';
import confirmRegistrationReducer from './confirmRegistrationReducer';
import {ConfirmRegistrationEvent} from './confirmRegistrationEvents';
import {updateConfirmationKey} from './confirmRegistrationCommand';
import {stub} from 'sinon';
import RoutingCommand from '../commands/routingCommand';

const INITIAL_STATE: ConfirmRegistrationState = {
  confirmationKey: '',
  hasFailedKeyCheck: false
};

describe('ConfirmRegistrationSaga', () => {
  let sagaTester: SagaTester;
  const readEmailForConfirmationKeySpy = stub(api, 'readEmailForConfirmationKey');
  const deleteConfirmationKeySpy = stub(api, 'deleteConfirmationKey');
  beforeEach(() => {
    sagaTester = new SagaTester({INITIAL_STATE, reducers: confirmRegistrationReducer});
    sagaTester.start(rootSaga);
  });

  afterEach(() => {
    sagaTester.reset();
    readEmailForConfirmationKeySpy.resetHistory();
    deleteConfirmationKeySpy.resetHistory();
  });
  it('deletes confirmation key if email is correct', async() => {
    readEmailForConfirmationKeySpy.resolves('email@valid.de');
    sagaTester.dispatch(updateConfirmationKey('4711', 'email@valid.de'));
    await sagaTester.waitFor(RoutingCommand.ROUTE_TO);
    expect(deleteConfirmationKeySpy.withArgs('4711').calledOnce).toBe(true);
  });
  it('Emit failure if email is false', async() => {
    readEmailForConfirmationKeySpy.resolves('anotherEmail@valid.de');
    sagaTester.dispatch(updateConfirmationKey('4711', 'email@valid.de'));
    await sagaTester.waitFor(ConfirmRegistrationEvent.CONFIRMATION_KEY_FAILED);
    expect(readEmailForConfirmationKeySpy.withArgs('4711').calledOnce).toBe(true);
    expect(deleteConfirmationKeySpy.calledOnce).toBe(false);
  });
});
