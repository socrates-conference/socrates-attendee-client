// @flow

import React from 'react';
import {withRouter} from 'react-router';
import {updateConfirmationKey} from './confirmRegistrationCommand';
import {connect} from 'react-redux';

type Props = {
  match: Object,
  email: string,
  hasFailedKeyCheck: string,
  updateConfirmationKey: (key: string, email: string) => void

}

export class ConfirmRegistration extends React.Component<Props> {

  componentDidMount = () => {
    this.props.updateConfirmationKey(this.props.match.params.confirmationKey, this.props.email);
  };

  render = () => {
    if (this.props.hasFailedKeyCheck) {
      return (
        <div className="container mt-5">
          <h2>Confirmation key check failed</h2>
          <div>
            Did you mean to login? If so please visit <a href="https://socrates-conference.de/attendee/login">this page</a>.
            Otherwise, please contact the orga team at registration@socrates-conference.de
          </div>
        </div>
      );
    }
    return null;
  };
}

const mapStateToProps = (state) => {
  return {
    email: state.authentication.email,
    hasFailedKeyCheck: state.confirmRegistration.hasFailedKeyCheck
  };
};

const mapDispatchToProps = {
  updateConfirmationKey
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ConfirmRegistration));
