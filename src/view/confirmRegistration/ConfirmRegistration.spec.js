import {mount} from 'enzyme';
import React from 'react';
import {ConfirmRegistration} from './ConfirmRegistration';

describe('ConfirmRegistration', () => {
  let wrapper;
  describe('renders', () => {
    beforeEach(() => {
      wrapper = mount(
        <ConfirmRegistration
          match={{params: {confirmationKey: 4711}}}
          email="email@valid.de"
          updateConfirmationKey={() => {}}
          hasFailedKeyCheck={true}
        />);
    });

    it('without crashing', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });
});
