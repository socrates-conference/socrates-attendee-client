// @flow

import {
  call,
  put
} from 'redux-saga/effects';
import {confirmationKeyFailed} from './confirmRegistrationEvents';
import * as api from '../requests/api';
import {routeTo} from '../commands/routingCommand';

export function* updateConfirmationKeySaga(action: Object): Iterable<any> {
  const {key, email} = action;
  const apiEmail: ?string = yield call(api.readEmailForConfirmationKey, key);
  if(apiEmail && apiEmail === email) {
    yield call(api.deleteConfirmationKey, key);
    yield put(routeTo({pathname: '/profile'}));
  } else {
    yield put(confirmationKeyFailed());
  }

}

