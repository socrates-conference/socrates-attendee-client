// @flow

export class ConfirmRegistrationEvent {
  static get CONFIRMATION_KEY_FAILED() {return 'ConfirmRegistrationEvent/CONFIRMATION_KEY_FAILED';}
}

export const confirmationKeyFailed = () => ({type: ConfirmRegistrationEvent.CONFIRMATION_KEY_FAILED});

