// @flow

import React from 'react';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import {handleInvitationResponse} from '../roomSharing/roomSharingCommand';

type Props = {
  match: Object,
  handleInvitationResponse: (action: string, key: string, email: string) => void,
  alreadyAccepted: boolean,
  alreadyDeclined: boolean,
  accepted: boolean,
  declined: boolean,
  email: string
}

export class HandleInvitationComponent extends React.Component<Props> {

  componentDidMount = () => {
    if (this.props.email) {
      this.props.handleInvitationResponse(this.props.match.params.action,
        this.props.match.params.key, this.props.email);
    }
  };

  render = () => {
    if (this.props.accepted) {
      return <div className="container mt-5"><h2>Invitation confirmed</h2>
        <div>Room sharing invitation accepted. Your roommate has been informed.</div>
      </div>;
    }
    if (this.props.declined) {
      return <div className="container mt-5"><h2>Invitation declined</h2>
        <div>Room sharing invitation declined. The other person has been informed.</div>
      </div>;
    }
    if (this.props.alreadyAccepted) {
      return <div className="container mt-5"><h2>Nothing to do</h2>
        <div>No changes possible. Invitation was already accepted.</div>
      </div>;
    }
    if (this.props.alreadyDeclined) {
      return <div className="container mt-5"><h2>Invitation already declined</h2>
        <div>No changes possible. Invitation was already declined.</div>
      </div>;
    }
    return null;
  };
}

const mapStateToProps = (state) => {
  return {
    alreadyAccepted: state.roomSharing.alreadyAccepted,
    alreadyDeclined: state.roomSharing.alreadyDeclined,
    accepted: state.roomSharing.accepted,
    declined: state.roomSharing.declined,
    email: state.authentication.email
  };
};

const mapDispatchToProps = {
  handleInvitationResponse
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HandleInvitationComponent));
