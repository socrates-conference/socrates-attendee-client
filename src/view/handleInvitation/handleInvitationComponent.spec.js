// @flow
import {
  mount
} from 'enzyme';
import React from 'react';
import expect from '../../test/expect';
import {HandleInvitationComponent} from './handleInvitationComponent';

const props = {
  match: {params: {action: 'none', key: 'key'}},
  handleInvitationResponse: () => {},
  alreadyAccepted: false,
  alreadyDeclined: false,
  accepted: false,
  declined: false,
  email: 'test@test.de'
};

describe('Profile:', () => {
  describe('should render', () => {
    it('when unknown action accepted', () => {
      const wrapper = mount(<HandleInvitationComponent {...props}/>);
      expect(wrapper).toMatchSnapshot();
    });
    it('when already accepted invitation', () => {
      const newProps = {...props, alreadyAccepted: true};
      const wrapper = mount(<HandleInvitationComponent {...newProps}/>);
      expect(wrapper).toMatchSnapshot();
    });
    it('when already declined invitation', () => {
      const newProps = {...props, alreadyDeclined: true};
      const wrapper = mount(<HandleInvitationComponent {...newProps}/>);
      expect(wrapper).toMatchSnapshot();
    });
    it('when accepting invitation', () => {
      const newProps = {...props, accepted: true};
      const wrapper = mount(<HandleInvitationComponent {...newProps}/>);
      expect(wrapper).toMatchSnapshot();
    });
    it('when declining invitation', () => {
      const newProps = {...props, declined: true};
      const wrapper = mount(<HandleInvitationComponent {...newProps}/>);
      expect(wrapper).toMatchSnapshot();
    });
  });
});
