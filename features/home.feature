# language:en
Feature:
  As an applicant, I want the attendee area to be secure, so that nobody else will see my personal data.
  Background:
    Given the application is running

  Scenario:
  Login page should show be loaded
    Given the "profile" page is open
    Then it shows the ".card-header" "<h2>Login</h2>"

