/* eslint-disable babel/no-invalid-this */
import {defineSupportCode} from 'cucumber';
import {push} from 'react-router-redux';
import {expectContentToContain} from '../support/expectations';

// eslint-disable-next-line no-unused-vars
function setInputValue(wrapper, selector, value) {
  const nameNode = wrapper.find(selector);
  nameNode.instance().value = value;
  nameNode.simulate('change', {target: {value: value}});
}

defineSupportCode(function ({Given, When, Then}) {
  Given(/^the "(.+)" page is open$/, function (title) {
    this.sagaTester.dispatch(push(`/${title}`));
  });

  When(/^I open the "(.+)" page$/, function (title) {
    this.sagaTester.dispatch(push(`/${title}`));
  });

  Then(/^it shows the "(.+)" "(.+)"$/, function (selector, content) {
    expectContentToContain(selector, content);
  });
});
