/* eslint-disable no-unused-vars */
// noinspection ES6UnusedImports
import * as setup from '../../src/test/setup';
/* eslint-enable no-unused-vars */

import {defineSupportCode} from 'cucumber';
import reducers from '../../src/view/reducers/rootReducer';
import {routerReducer} from 'react-router-redux';
import {HistoryFactory} from '../../src/view/store/store';

function SoCraTesWorld() {
  this.history = HistoryFactory.createTestHistory('/attendee');
  this.allReducers = {...reducers, router: routerReducer};
}

defineSupportCode(({setWorldConstructor}) => {
  setWorldConstructor(SoCraTesWorld);
});