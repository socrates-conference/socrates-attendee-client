/* eslint-disable babel/no-invalid-this */
import {defineSupportCode} from 'cucumber';
import setUpJsDom from '../../src/test/setupJsDom';
import createTestLogger from '../../src/test/logger';
import rootSaga from '../../src/view/sagas/rootSaga';
import {routerMiddleware} from 'react-router-redux';
import SagaTester from 'redux-saga-tester';
import {stub} from 'sinon';
import * as api from '../../src/view/requests/api';

defineSupportCode(function ({Before, After}) {
  Before(function () {
    this.sagaTester = new SagaTester({
      reducers: this.allReducers,
      middlewares: [routerMiddleware(this.history), createTestLogger()]
    });
    this.sagaTester.start(rootSaga);
    this.root = setUpJsDom();
    this.loginStub = stub(api, 'login');
  });

  After(function () {
    if (this.root) {
      this.root = undefined;
    }
    api.login.restore();
  });
});