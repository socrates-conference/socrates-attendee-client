// @flow
/* eslint-disable */
import type {Config} from '../../src/ConfigType';

const config: Config = {
  environment: 'dev',
  jwtSecret: '%jwt_secret%',
  siteUrl: 'http://18.197.143.144',
  serverBackend: '/server/api/v1',
};
export default config;
